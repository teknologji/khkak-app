import React from "react";
import { NavigationContainer } from "@react-navigation/native";

import DrawerNavigator from "./src/navigation/DrawerNavigator";

import { Provider } from 'react-redux';
import store from './src/store/index';
import { RootSiblingParent } from 'react-native-root-siblings'
import { Provider as PaperProvider } from 'react-native-paper';

const App = () => {

  return (

    <Provider store={store}>
      <PaperProvider>

      <RootSiblingParent>
        <NavigationContainer>
          <DrawerNavigator />
        </NavigationContainer>
      </RootSiblingParent>
      </PaperProvider>

    </Provider>

  );
}
export default App;