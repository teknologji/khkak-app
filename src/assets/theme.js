import { DefaultTheme } from 'react-native-paper';

export const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#e53232',
    secondary: '#222222',
    error: '#f13a59',
  
},
};

export const cstmStyle = {
  surface: {
    width: '100%',
    elevation: 5,
    padding: 20
  },
  dataTbl: {
    justifyContent: 'center',
    backgroundColor: '#fff',
    padding: 8,
    marginTop: 20
  },
  upldAFile: {
    width: '100%',
    height: 50,
    borderRadius: 22,
    borderStyle: 'dashed',
    borderColor: '#999',
    borderWidth: 1,
    backgroundColor: 'white',
    marginBottom: 15,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row'
  },
  dsntBx:{
    width: 65, 
            height: 25, 
            position: "absolute",
            left: 13, top: 13,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 4,
            backgroundColor: '#cd5700',
  },
  textWhite: { color: '#fff'},
  textRight: {textAlign: 'right'},
  textLightGrey: {color: '#ccc'},
  textGrey:{color: '#666', fontWeight: 'bold'},
  textBlack:{color: '#000'},
  textDanger: { color: '#dc3545'},    
  textGreen: { color: '#7BA41F'},
  textYellow: { color: '#d39e00'},
  textBlue: { color: '#0062cc'},
  textOrange: {color: '#cd5700'}, //D81500

  orderRowCellRight: {flex:0.5},
  orderRowCellLeft:{flex:0.5},
  bgGreen: { backgroundColor: '#7BA41F'},
  bgDefault: {backgroundColor: '#edf2f6'},
  bgWarning: { backgroundColor: '#ffc107'},
  bgDanger: { backgroundColor: '#dc3545'},
  bgOrange: { backgroundColor: '#cd5700'},
  bgGrey: { backgroundColor: '#999'},
  


 /*  boxStyle: {
    height: 300, 
    width: '47%', 
    borderWidth: 0, 
    backgroundColor: 'white',
    padding: 10, 
    marginBottom: 10, 
    marginLeft: 4,
    marginRight: 8, 
    marginTop: 20,
    borderRadius: 15,
    shadowColor: "#000",
shadowOffset: {
  width: 0,
  height: 2,
},
shadowOpacity: 0.23,
shadowRadius: 2.62,
elevation: 4,
  }, */

  txtStriked: {
    textDecorationLine: 'line-through',
  },
  prgrfTitle: {
    borderColor: '#ccc', 
    borderBottomWidth: 1, 
    width: '100%', 
    textAlign: 'center',
    paddingVertical: 10,
    color: '#cd5700'
  },
  fltrRow: {flexDirection: 'row', width: '50%', justifyContent: 'space-between'}
}