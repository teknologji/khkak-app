import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import KhaledStack from "./StackNavigator";
import { connect } from 'react-redux';
import { FontAwesome5/* , SimpleLineIcons, MaterialCommunityIcons */ } from '@expo/vector-icons';
import MyTxt from "../components/MyTxt";
import { theme } from '../assets/theme';
import LoginScreen from "../screens/auth/LoginScreen";
import Contact from "../screens/general/Contact";

const Tab = createBottomTabNavigator();


const BottomTabNavigator = (props) => {
  const lng = props.selectedLanguage;
  return (
    <Tab.Navigator 
    initialRouteName="Home"
    screenOptions={({ route }) => ({
      tabBarIcon: ({ focused, color, size }) => {
        let iconName;

        if (route.name === 'Home') {
          iconName = focused
            ? 'home'
            : 'home';
        } else if (route.name === 'Contact') {
          iconName = focused ? 'user-plus' : 'user-plus';
        } else if (route.name === 'Login') {
          iconName = focused ? 'unlock' : 'unlock';
        }

        // You can return any component that you like here!
        return <FontAwesome5 name={iconName} size={16} color={color} />;
      },
    })}
    tabBarOptions={{
      activeTintColor: '#ffa500',
      inactiveTintColor: 'white',
      showLabel:true,
      labelPosition: "below-icon",
      style: {
        backgroundColor: theme.colors.primary,
        fontSize: 22
      },
    }}
    
        >
      
      
  <Tab.Screen name="Home" component={KhaledStack} 
      options={{ tabBarLabel: () => (<MyTxt txt={lng.homeTxt} style={{color: '#fff'}} />) }} /> 

  <Tab.Screen name="Contact" component={Contact} 
      options={{ tabBarLabel: () => (<MyTxt txt={lng.contactTxt} style={{color: '#fff'}} />) }} /> 
      
  <Tab.Screen name="Login" component={LoginScreen} 
      options={{ tabBarLabel: () => (<MyTxt txt={lng.loginTxt} style={{color: '#fff'}} />) }} /> 
 

</Tab.Navigator>
  );
};

//export default BottomTabNavigator;

const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
    defaultSets: state.DfltSts,
    userInfo: state.User.info,
  }
}
export default connect(mapStateToProps, null)(BottomTabNavigator);