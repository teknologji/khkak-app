import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import { View } from 'react-native';
import { theme } from "../assets/theme";
import { Appbar, Avatar, Badge } from 'react-native-paper';
import HomeScreen from "../screens/HomeScreen";
import RegisterScreen from "../screens/auth/RegisterScreen";
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { connect } from 'react-redux';
import LoginScreen from "../screens/auth/LoginScreen";
import ResetPassword from "../screens/auth/ResetPassword";


import AllProducts from "../screens/product/All";

import ShowProduct from "../screens/product/Show";
import ContactScreen from "../screens/general/Contact";
import AboutScreen from "../screens/general/About";
import FaqScreen from "../screens/general/Faq";
import Profile from "../screens/auth/Profile";

import ViewCart from "../screens/order/ViewCart";
import Activation from "../screens/auth/Activation";

import Search from "../screens/general/Search";
import Cpage from "../screens/general/Cpage";

import CreateShipmentAdrs from "../screens/shipment/Create";
import EditShipmentAdrs from "../screens/shipment/Edit";

import ShipmentAdrsIndex from "../screens/shipment/Index";
import Pay from "../screens/order/Pay";
import MyOrders from "../screens/order/MyOrders";

const Stack = createStackNavigator();


const Header = ({ scene, previous, navigation, cartItems }) => {

  //console.log(cartItems)
  const { options } = scene.descriptor;
  const title =
    options.headerTitle !== undefined
      ? options.headerTitle
      : options.title !== undefined
        ? options.title
        : scene.route.name;

  return (
    <Appbar.Header
      /* statusBarHeight={Platform.OS === "android" ? 30 : 30} */

      style={{ backgroundColor: theme.colors.primary }}
       /* theme={{ colors: { primary: theme.colors.surface } }} */>

      {scene.route.name != 'Home' ? (
        <Appbar.BackAction
          onPress={navigation.goBack}
        /* color={theme.colors.primary} */
        />
      ) : (

          <Avatar.Image

            style={{ backgroundColor: 'transparent' }}
            size={60}
            source={require('../assets/logo96.png')}
          /* source={{
            uri:
              'https://pbs.twimg.com/profile_images/952545910990495744/b59hSXUd_400x400.jpg',
          }} */
          />

        )}


      <Appbar.Content
        title={title}
      />

      <View
        style={[
          { flexDirection: 'column', paddingBottom: 16, width: 45, marginRight:10 },
        ]}
        pointerEvents="box-none"
      >
        <Badge style={{ alignSelf: 'center', height: 18, backgroundColor: 'white' }}>
          {cartItems ? cartItems.cart.length : 0}
        </Badge>
        <MaterialCommunityIcons
          name="cart"
          color='white'
          size={21}
          onPress={() => { navigation.navigate('ViewCart'); }}
        />

      </View>

      <Appbar.Action icon="search-web" color="white" size={28} onPress={() => { navigation.navigate('Search'); }} />

      <Appbar.Action icon="menu" color="white" size={28} onPress={() => { navigation.toggleDrawer(); }} />
    </Appbar.Header>
  );
};


// Define multiple groups of screens in objects like this
const commonScreens = {
  Home: HomeScreen,
  Activation: Activation,
  Contact: ContactScreen,
  About: AboutScreen,
  Faq: FaqScreen,
  Search: Search,
  Cpage: Cpage,
  
  AllProducts: AllProducts,
  ShowProduct: ShowProduct,
  ViewCart: ViewCart,
  homePage: HomeScreen,
};

const authScreens = {
  Login: LoginScreen,
  ResetPassword: ResetPassword,
  Register: RegisterScreen,
};

const userScreens = {
  Profile: Profile,
  CreateShipmentAdrs: CreateShipmentAdrs,
  EditShipmentAdrs: EditShipmentAdrs,
  ShipmentAdrsIndex: ShipmentAdrsIndex,
  Pay: Pay,
  MyOrders: MyOrders
};

// Then use them in your components by looping over the object and creating screen configs
// You could extract this logic to a utility function and reuse it to simplify your code
const KhaledStack = ({ userInfo, selectedLanguage, cartItems }) => {

  return (
    <Stack.Navigator
      headerMode="screen"
      screenOptions={{
        header: ({ scene, previous, navigation }) => (
          <Header scene={scene} previous={previous} navigation={navigation} cartItems={cartItems} />
        ),
      }}>
      {Object.entries({
        // Use the screens normally 
        ...commonScreens,
        // Use some screens conditionally based on some condition
        ...(userInfo.info.accessToken ? userScreens : authScreens),
      }).map(([name, component, hdrTitle], i) => (
        <Stack.Screen name={name} component={component} key={i} />
      ))}
    </Stack.Navigator>
  )
}

const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
    userInfo: state.User,
    cartItems: state.cartItems,
  }
}
export default connect(mapStateToProps, null)(KhaledStack);
