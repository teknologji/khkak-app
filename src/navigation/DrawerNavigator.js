import React from "react";
import { connect, useDispatch } from 'react-redux';

//import Toast from 'react-native-root-toast'
import { ar } from '../lang/ar';
import { en } from '../lang/en';
import { tr } from '../lang/tr';
import { al } from '../lang/al';

import { createDrawerNavigator, DrawerContentScrollView } from "@react-navigation/drawer";
import {
  Drawer as Drawerk,
  Avatar,
  Title,
  Caption,
  Paragraph,
  Text,
  TouchableRipple,
  Switch,
  Badge
} from 'react-native-paper';

//import { ContactStackNavigator } from "./StackNavigator";
//import TabNavigator from "./TabNavigator";
import { View, Image, StyleSheet } from "react-native";
import { MaterialCommunityIcons } from '@expo/vector-icons';

import MyTxt from "../components/MyTxt";
import { Fragment } from "react";

import KhaledStack from './StackNavigator';

function CustomDrawerContent(props) {
  //console.log(props.usr)

  let lng = props.lng;
  let userInfo = props.usr;
  const dispatch = useDispatch();

  function hndlLng(val) {
    dispatch({ type: 'language_data', payload: val })
  }

  return (
    <DrawerContentScrollView {...props}>

      <View
        style={
          styles.drawerContent
        }
      >
        <View style={styles.userInfoSection}>

          <Drawerk.Section>
            <View style={[styles.row, { justifyContent: 'flex-start' }]}>
              <Avatar.Image
                source={{
                  uri:
                    'https://pbs.twimg.com/profile_images/952545910990495744/b59hSXUd_400x400.jpg',
                }}
                size={40}
              />

              <MyTxt style={styles.title} txt={userInfo.accessToken ? lng.welcomeTxt + ' ' + userInfo.full_name : lng.welcomeTxt + ' ' + lng.ourVisitorTxt} />

            </View>
            <Caption style={styles.caption}>
              @<MyTxt txt={lng.websiteNameTxt} />
            </Caption>

          </Drawerk.Section>




        </View>


        <Drawerk.Section style={styles.drawerSection}>


          {userInfo.accessToken != '' ?

            <Drawerk.Section>
              <Drawerk.Item
                icon={({ color, size }) => (
                  <MaterialCommunityIcons
                    name="account-outline"
                    color={color}
                    size={36}
                  />
                )}
                label={<MyTxt txt={lng.profileTxt} />}
                onPress={() => { props.navigation.navigate('Profile'); }}
              />
              <Drawerk.Item
                icon={({ color, size }) => (
                  <MaterialCommunityIcons
                    name="account-outline"
                    color={color}
                    size={36}
                  />
                )}
                label={<MyTxt txt={lng.shipmentAdrsesTxt} />}
                onPress={() => { props.navigation.navigate('ShipmentAdrsIndex'); }}
              />

              <Drawerk.Item
                icon={({ color, size }) => (
                  <MaterialCommunityIcons
                    name="account-outline"
                    color={color}
                    size={36}
                  />
                )}
                label={<MyTxt txt={lng.myOrdersTxt} />}
                onPress={() => { props.navigation.navigate('MyOrders'); }}
              />

            </Drawerk.Section>

            : null}

          {/* <Drawerk.Item
                        icon={({ color, size }) => (
                          <MaterialCommunityIcons
                            name="email"
                            color={color}
                            size={36}
                          />
                        )}
                        label={<MyTxt txt={'notification'} />}
                        onPress={() => props.navigation.navigate('Test')}
                      /> */}

          <Drawerk.Item
            icon={({ color, size }) => (
              <MaterialCommunityIcons
                name="library"
                color={color}
                size={36}
              />
            )}
            label={<MyTxt txt={lng.homeTxt} />}
            onPress={() => { props.navigation.navigate('homePage'); }}
          />
          <Drawerk.Item
            icon={({ color, size }) => (
              <MaterialCommunityIcons
                name="certificate"
                color={color}
                size={36}
              />
            )}
            label={<MyTxt txt={lng.aboutUsTxt} />}
            onPress={() => props.navigation.navigate('About')}
          />

          <Drawerk.Item
            icon={({ color, size }) => (
              <MaterialCommunityIcons
                name="teach"
                color={color}
                size={36}
              />
            )}
            label={<MyTxt txt={lng.allProductsTxt} />}
            onPress={() => props.navigation.navigate('AllProducts', { id: 'all', title: lng.allProductsTxt })}
          />

          <Drawerk.Item
            icon={({ color, size }) => (
              <MaterialCommunityIcons
                name="contact-phone"
                color={color}
                size={36}
              />
            )}
            label={<MyTxt txt={lng.contactTxt} />}
            onPress={() => props.navigation.navigate('Contact')}
          />



          <Drawerk.Item
            icon={({ color, size }) => (
              <MaterialCommunityIcons
                name="frequently-asked-questions"
                color={color}
                size={36}
              />
            )}
            label={<MyTxt txt={lng.faqsTxt} />}
            onPress={() => props.navigation.navigate('Faq')}
          />

          <Drawerk.Item
            icon={({ color, size }) => (
              <MaterialCommunityIcons
                name="certificate"
                color={color}
                size={36}
              />
            )}
            label={<MyTxt txt={lng.privacyTxt} />}
            onPress={() => props.navigation.navigate('Cpage', { id: 2 })}
          />

          <Drawerk.Item
            icon={({ color, size }) => (
              <MaterialCommunityIcons
                name="certificate"
                color={color}
                size={36}
              />
            )}
            label={<MyTxt txt={lng.changeReturnTxt} />}
            onPress={() => props.navigation.navigate('Cpage', { id: 3 })}
          />

          <Drawerk.Item
            icon={({ color, size }) => (
              <MaterialCommunityIcons
                name="certificate"
                color={color}
                size={36}
              />
            )}
            label={<MyTxt txt={lng.conditionsTxt} />}
            onPress={() => props.navigation.navigate('Cpage', { id: 5 })}
          />

        </Drawerk.Section>





        <Drawerk.Section>
          {userInfo.accessToken != '' ?

            <Drawerk.Item
              icon={({ color, size }) => (
                <MaterialCommunityIcons
                  name="library-books"
                  color={color}
                  size={36}
                />
              )}
              label={<MyTxt txt={lng.logoutTxt} />}
              onPress={() => dispatch({ type: 'LOGOUT', payload: null })}
            />
            :
            <Fragment>

              <Drawerk.Item
                icon={({ color, size }) => (
                  <MaterialCommunityIcons
                    name="lock"
                    color={color}
                    size={36}
                  />
                )}
                label={<MyTxt txt={lng.loginTxt} />}
                onPress={() => props.navigation.navigate('Login')}
              />

              <Drawerk.Item
                icon={({ color, size }) => (
                  <MaterialCommunityIcons
                    name="account-card-details"
                    color={color}
                    size={36}
                  />
                )}
                label={<MyTxt txt={lng.registerTxt} />}
                onPress={() => props.navigation.navigate('Register')}
              />
            </Fragment>

          }


        </Drawerk.Section>


        <Drawerk.Section title={lng.chsLngTxt}>

          <View style={styles.preference}>

            <TouchableRipple onPress={() => hndlLng(ar)}>
              <Image source={require('../assets/imgs/saudi32.png')} />
            </TouchableRipple>

            <TouchableRipple onPress={() => hndlLng(en)}>
              <Image source={require('../assets/imgs/United-kingdom-icon32.png')} />
            </TouchableRipple>


          </View>

          {/* <TouchableRipple onPress={() => {}}>
            <View style={styles.preference}>
              <Text>RTL</Text>
              <View pointerEvents="none">
                <Switch value={true} />
              </View>
            </View>
          </TouchableRipple> */}
        </Drawerk.Section>

      </View>





    </DrawerContentScrollView>
  );
}


const Drawer = createDrawerNavigator();

const DrawerNavigator = (reducers) => {
  //console.log(reducers.userInfo);
  return (
    <Drawer.Navigator
      /* drawerPosition="ltr" */
      drawerContent={(props) => <CustomDrawerContent
        {...props}
        lng={reducers.selectedLanguage}
        usr={reducers.userInfo} />}>

      <Drawer.Screen name="Home" component={KhaledStack} />

    </Drawer.Navigator>
  );
}




const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
    marginTop: 20,
  },
  userInfoSection: {
    paddingLeft: 20,
  },
  title: {
    marginTop: 15,
    marginBottom: 10,
    fontSize: 18,
    marginLeft: 10
  },
  caption: {
    fontSize: 16,
  },
  row: {
    marginTop: 5,
    marginBottom: 5,
    flexDirection: 'row',
    alignItems: 'center',
  },
  section: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 15,
  },
  paragraph: {
    fontWeight: 'bold',
    marginRight: 3,
    color: '#6a5acd',
  },
  drawerSection: {
    marginTop: 15,
  },
  preference: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
});


/* const mapDispatchToProps = (dispatch) => {
  return {
      //selectLanguage: (val) => dispatch({ type: 'language_data', payload: val }),
      selectLanguage: (val) =>  dispatch({ type: 'language_data', payload: val }),
      changeCurrency: (curr) => dispatch({ type: 'CHANGE_CURRENCY', payload: curr }),    
      //logout: () => dispatch({ type: 'LOGOUT', payload: null }),

  }
}; */

const mapStateToProps = (state) => {
  return {

    userInfo: state.User.info,
    selectedLanguage: state.LanguageReducer.slctdLng,
    defaultSets: state.DfltSts,
  }
}

export default connect(mapStateToProps, null)(DrawerNavigator);