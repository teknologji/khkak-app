import React, { Component } from "react";
import {
    View,
    Text,
    ScrollView,
    FlatList
} from "react-native";
import MyTxt from '../../components/MyTxt';
import { connect } from 'react-redux'
import ViewCartProduct from "../../components/ViewCartProduct";
import Toast from 'react-native-root-toast';
import Background from "../../components/Background";
import { Surface } from 'react-native-paper';
import { cstmStyle } from "../../assets/theme";
import Button from "../../components/Button";
import ShipmentAdrsBox from "../../components/ShipmentAdrsBox";
import getData from '../../helpers/getData';
import deleteData from '../../helpers/deleteData';
import AwesomeAlert from 'react-native-awesome-alerts';
import HdrTitle from "../../components/HdrTitle";

class ViewCart extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            shippingCharges: 0,
            userShpmntAdrs: [],
            shipmentAdrsId: 0,
            showAlert: false,
            rowToBeDlt: 0
        };


    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.userInfo.id !== this.props.userInfo.id) {
            this.fetchUsrShpmntAdrs();
        }

    }

    componentDidMount() {
        this.fetchUsrShpmntAdrs();

        this.focusListener = this.props.navigation.addListener('focus', () => {
            this.fetchUsrShpmntAdrs();
        });
        this.props.navigation.setOptions({ headerTitle: <HdrTitle txt={this.props.selectedLanguage.shoppingCartTxt} /> });

    }



    fetchUsrShpmntAdrs() {
        //
        let id = this.props.userInfo.id ? this.props.userInfo.id : 0;
        getData(this.props.defaultSets.apiUrl + 'user-shpmnt-adrs/' + id, this.props.userInfo.accessToken)

            .then((response) => {
                this.setState({ userShpmntAdrs: response.data.rows });
                //console.log(response.data.rows)
            })
            .catch(err => {
                console.log(err);
            });
    }


    getShippingCharges = (shipmentAdrsId) => {

        this.setState({ shipmentAdrsId });
        //const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        getData(this.props.defaultSets.apiUrl + 'shipmentadrs/' + shipmentAdrsId, this.props.userInfo.accessToken)
            .then(response => {

                this.setState({ shippingCharges: response.data.city.shippingCharges });
                //toast.dismiss(ldToast);    

                //console.log(response.data.city.shippingCharges)
            })
            .catch(err => {
                console.log(err);
            });

    }




    deleteRow() {
        let id = this.state.rowToBeDlt;
        //console.log('dlt rowww', id)

        deleteData(this.props.defaultSets.apiUrl + 'shipmentadrs/' + id, this.props.userInfo.accessToken)
            .then(res => {
                const filteredData = this.state.userShpmntAdrs.filter(item => item.id !== id);
                this.setState({ userShpmntAdrs: filteredData });

            })
            .catch(err => {
                console.log(err);
            });

    }
    showAlert = (id) => {
        this.setState({
            rowToBeDlt: id,
            showAlert: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlert: false
        });
    };

    render() {

        let lng = this.props.selectedLanguage;
        let cartItems = this.props.cartItems;
        const { userShpmntAdrs, shippingCharges, shipmentAdrsId, showAlert, loading } = this.state;
        return (

            <Background>

                <ScrollView style={{ flex: 1, width: '100%' }}>

                    <View>
                        {cartItems.length > 0 ?
                            cartItems.map((item, i) => (


                                <ViewCartProduct
                                    key={i}
                                    item={item}
                                    navigation={this.props.navigation}
                                    updateQuantity={this.props.updateQuantity}
                                    removeItem={this.props.removeItem}
                                />




                            )
                            )

                            : <View style={{ flex: 1, alignItems: 'center' }}>

                                <MyTxt txt={lng.noItemsTxt} />
                            </View>
                        }
                    </View>

                    {/* <FlatList
          data={cartItems}
          renderItem={({ item }) => <ViewCartProduct 
          item={item} 
          removeItem={this.props.removeItem}
          updateQuantity = {this.props.updateQuantity}
          onPress={this.props.addItemToCart}
/>}
keyExtractor={item => 'k'+Math.random()}
refreshing={loading}
//onRefresh={this.handleRefresh}
/> */}
                    {/*  <Text>{item.quantity}</Text>
                                    <ViewCartProduct
                                        key={i}
                                        item={item}
                                        navigation={this.props.navigation}
                                        updateQuantity={this.props.updateQuantity}
                                    /> */}
                    <Surface style={[cstmStyle.surface]}>


                        <View style={[{ flexDirection: 'row', padding: 5, justifyContent: 'space-between' }]}>
                            <MyTxt txt={lng.subTotalTxt + ":"} />

                            <Text>
                                {this.props.totalPrice + ' ' + lng.sarTxt}
                            </Text>
                        </View>

                        <View style={[{ flexDirection: 'row', padding: 5, justifyContent: 'space-between' }]}>
                            <MyTxt txt={lng.shippingChargesTxt + ":"} />

                            <Text>
                                {shippingCharges + ' ' + lng.sarTxt}
                            </Text>
                        </View>

                        <View style={[{ flexDirection: 'row', padding: 5, justifyContent: 'space-between' }]}>
                            <MyTxt txt={lng.finalTotalTxt + ":"} />

                            <Text>
                                {(shippingCharges + this.props.totalPrice) + ' ' + lng.sarTxt}
                            </Text>
                        </View>

                        <View style={[{ flexDirection: 'row', padding: 5, justifyContent: 'space-between' }]}>

                            <MyTxt txt={lng.qtyTxt + ":"} />
                            <Text>
                                {this.props.totalQuantity}
                            </Text>

                        </View>



                    </Surface>

                    {this.props.userInfo.id != 0 ?
                    <Surface style={[cstmStyle.surface, { marginTop: 20, flexDirection: 'row', justifyContent: 'space-between' }]}>
                        <MyTxt fntWght='bold' txt={lng.shipmentAdrsesTxt} />

                        <Button mode="contained"
                            onPress={() => this.props.navigation.navigate('CreateShipmentAdrs', { screenToBack: 'ViewCart' })}>
                            {lng.addShipmentAdrsTxt}
                        </Button>
                    </Surface>: null }

                    {userShpmntAdrs.length > 0 ?
                        userShpmntAdrs.map((item, i) => {
                            return (
                                <ShipmentAdrsBox
                                    showAlert={() => this.showAlert(item.id)}
                                    shipmentAdrsId={shipmentAdrsId}
                                    screenToBack="ViewCart"
                                    key={item.id}
                                    item={item}
                                    getShippingCharges={this.getShippingCharges}
                                    navigation={this.props.navigation}
                                />
                            )
                        })

                        : null
                    }

                    {cartItems.length > 0 &&

                        <Surface style={[cstmStyle.surface, { marginVertical: 30 }]}>
                            {this.props.userInfo.id == 0 ?

                                <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>


                                    <Button mode="contained"
                                        onPress={() => this.props.navigation.navigate('Login', { screenToBack: 'ViewCart' })}>
                                        {lng.loginTxt}
                                    </Button>

                                    <Button mode="contained"
                                        onPress={() => this.props.navigation.navigate('Register')}>
                                        {lng.registerTxt}
                                    </Button>
                                </View>
                                :

                                userShpmntAdrs.length > 0 ?
                                    (shipmentAdrsId == 0 ?
                                        <Button mode="contained">
                                            {lng.chooseShipmentAdrsTxt}
                                        </Button> :
                                        <Button mode="contained"
                                            onPress={() => this.props.navigation.navigate('Pay', { shippingCharges: shippingCharges, shipmentAdrsId: shipmentAdrsId })}>
                                            {lng.payTxt}
                                        </Button>)
                                    :
                                    (<Button mode="contained"
                                        onPress={() => this.props.navigation.navigate('CreateShipmentAdrs', { screenToBack: 'ViewCart' })}>
                                        {lng.addShipmentAdrsTxt}
                                    </Button>)


                            }

                        </Surface>


                    }


                    <AwesomeAlert
                        show={showAlert}
                        showProgress={false}
                        title={lng.confirmMsgTxt}
                        message={lng.rUSureTxt}
                        closeOnTouchOutside={true}
                        closeOnHardwareBackPress={false}
                        showCancelButton={true}
                        showConfirmButton={true}
                        cancelText={lng.noTxt}
                        confirmText={lng.yesTxt}
                        confirmButtonColor="#DD6B55"
                        onCancelPressed={() => {
                            this.hideAlert();
                        }}
                        onConfirmPressed={() => {

                            this.deleteRow();
                            this.hideAlert();
                        }}
                    />
                </ScrollView>
            </Background>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        cartItems: state.cartItems.cart,
        totalQuantity: state.cartItems.totalQuantity,
        totalPrice: state.cartItems.totalPrice,
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info

    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        removeItem: (product) => dispatch({ type: 'REMOVE_FROM_CART', payload: product }),
        //updateCartTotal: (newSubTotal) => dispatch({ type: 'UPDATE_CART_TOTAL', newSubTotalValue: newSubTotal }),
        updateQuantity: (productId, quantity) => {
            dispatch({ type: 'ChANGE_PRODUCT_QUANTITY', productId: productId, newQuantity: quantity });
        }
        //addItemToCart: (product) => dispatch({ type: 'ADD_TO_CART', payload: product })

    }
}


export default connect(mapStateToProps, mapDispatchToProps)(ViewCart);

