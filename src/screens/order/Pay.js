import React, { Component } from "react";
import {
    View,
    Text,
    ScrollView,
    TouchableOpacity
} from "react-native";
import MyTxt from '../../components/MyTxt';
import { connect } from 'react-redux'
import Toast from 'react-native-root-toast';
import Background from "../../components/Background";
import { Surface } from 'react-native-paper';
import { cstmStyle } from "../../assets/theme";
import Button from "../../components/Button";
import HdrTitle from "../../components/HdrTitle";
import postData from '../../helpers/postData';

class Pay extends Component {

    constructor(props) {
        super(props);
        this.state = {
            paymentType: 2,
            loading: false,
            shippingCharges: this.props.route.params.shippingCharges,
            shipmentAdrsId: this.props.route.params.shipmentAdrsId,
        };

    }

    componentDidMount() {
        this.props.navigation.setOptions({ headerTitle: <HdrTitle txt={this.props.selectedLanguage.payTxt} /> });

    }


    handlePayment() {

        let ldToast = Toast.show(this.props.selectedLanguage.loadingTxt, {
            duration: 10000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#ffc107' //dc3545 //7aa513 
        });

        postData(this.props.defaultSets.apiUrl + 'orders', {
            paymentType: this.state.paymentType,
            userId: this.props.userInfo.id,
            shipmentAdrsId: this.state.shipmentAdrsId,
            items: this.props.cartItems,
            finalTotal: (this.props.totalPrice + this.state.shippingCharges),
            shipmentCost: this.state.shippingCharges,
            subTotal: this.props.totalPrice,
            langCode: this.props.selectedLanguage.langCode,
            transaction_currency: 'SAR'
        }, this.props.userInfo.accessToken)
            .then((response) => {

                Toast.hide(ldToast);

                if (response.data.success == false) {
                    this.setState({ isDisabled: false })
 
                    return Toast.show(response.data.msg, {
                        duration: 10000, //Toast.durations.LONG,
                        position: 0, //Toast.positions.TOP
                        shadow: true,
                        animation: true,
                        hideOnPress: true,
                        delay: 0,
                        backgroundColor: '#dc3545' //dc3545 //7aa513
                    });
                }

                Toast.show(response.data.msg, {
                    duration: 10000, //Toast.durations.LONG,
                    position: 0, //Toast.positions.TOP
                    shadow: true,
                    animation: true,
                    hideOnPress: true,
                    delay: 0,
                    backgroundColor: '#7aa513' //dc3545 //7aa513
                });

                this.props.emptifyCheckout();

                //return this.props.navigation.navigate('MyOrders');

                setTimeout( () => {
                    this.props.navigation.navigate('MyOrders');
                }, 1500);
                
            })
            .catch((error) => {
                //return console.log(error.response);
                this.setState({ isDisabled: false })

                if (error.response !== undefined && error.response.status === 422) {
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);
                    });
                    Toast.show(this.props.selectedLanguage.loadingTxt, {
                        duration: 10000, //Toast.durations.LONG,
                        position: 0, //Toast.positions.TOP
                        shadow: true,
                        animation: true,
                        hideOnPress: true,
                        delay: 0,
                        backgroundColor: '#dc3545' //dc3545 //7aa513
                    });
                }
            });

    }

    render() {

        let lng = this.props.selectedLanguage;
        let cartItems = this.props.cartItems;
        const { shippingCharges, paymentType } = this.state;
        return (

            <Background>

                <ScrollView style={{ flex: 1, width: '100%' }}>


                    <Surface style={[cstmStyle.surface]}>


                        <View style={[{
                            flexDirection: 'row', padding: 5,
                            justifyContent: 'space-between'
                        }]}>
                            <MyTxt txt={lng.productsCountTxt + ":"} />

                            <Text>
                                {cartItems && cartItems.length}
                            </Text>
                        </View>

                        <View style={[{ flexDirection: 'row', padding: 5, justifyContent: 'space-between' }]}>
                            <MyTxt txt={lng.shippingChargesTxt + ":"} />

                            <Text>
                                {shippingCharges + ' ' + lng.sarTxt}
                            </Text>
                        </View>

                        <View style={[{ flexDirection: 'row', padding: 5, justifyContent: 'space-between' }]}>
                            <MyTxt txt={lng.finalTotalTxt + ":"} />

                            <Text>
                                {(shippingCharges + this.props.totalPrice) + ' ' + lng.sarTxt}
                            </Text>
                        </View>



                    </Surface>


                    <Surface style={[cstmStyle.surface, { marginTop: 20, flexDirection: 'row', justifyContent: 'space-around' }]}>
                        
                        <TouchableOpacity 
                        
                        onPress={() => this.setState({ paymentType: 2 })}>
                            <MyTxt style={{color: paymentType == 2 ? 'black': '#ccc'}} fntWght='bold' txt={lng.usingRceiptsTxt} />
                        </TouchableOpacity>
                        <Text>|</Text>
                        <TouchableOpacity onPress={() => this.setState({ paymentType: 3 })}>
                            <MyTxt style={{color: paymentType == 3 ? 'black': '#ccc'}} fntWght='bold' txt={lng.paymentUponReceiptTxt} />
                        </TouchableOpacity>

                    </Surface>

                    {paymentType == 2 &&
                        <Surface style={[cstmStyle.surface, { marginTop: 20 }]}>
                            <MyTxt txt={lng.usingRceiptsMsgTxt} />

                            <View style={[{ flexDirection: 'row', marginVertical: 25, justifyContent: 'space-between' }]}>
                            <MyTxt fntWght='bold' txt={lng.ahlyBankTxt} />
                       
                        <Text>|</Text>

                        <Text>10963774000108</Text>

                        </View>

                        <View style={[{ flexDirection: 'row', marginVertical: 25, justifyContent: 'space-between' }]}>
                            <MyTxt fntWght='bold' txt={lng.rajhiBankTxt} />
                       
                        <Text>|</Text>

                        <Text>380608010127165</Text>

                        </View>

                        

                            <Button mode="contained"
                            onPress={() => this.handlePayment()}>
                            {lng.completeTxt}
                        </Button>
                        </Surface>}



                    {paymentType == 3 &&
                        <Surface style={[cstmStyle.surface, { marginTop: 20 }]}>
                            <MyTxt style={{color: 'red', fontSize: 18, marginBottom: 20}} txt={lng.paymentUponReceiptMsgTxt} />

                            <Button mode="contained"
                            onPress={() => this.handlePayment()}>
                            {lng.completeTxt}
                        </Button>

                        </Surface>}



                </ScrollView>
            </Background>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        cartItems: state.cartItems.cart,
        totalQuantity: state.cartItems.totalQuantity,
        totalPrice: state.cartItems.totalPrice,
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info

    }
}



const mapDispatchToProps = (dispatch) => {
    return {
        emptifyCheckout: () => dispatch({ type: 'EMPTIFY_CART', payload: null }),
    
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Pay);

