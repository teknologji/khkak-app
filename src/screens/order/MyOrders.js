import React, { Component, Fragment } from 'react';
import { View, Text, RefreshControl, ScrollView, TouchableOpacity, Image } from "react-native";
import Background from '../../components/Background';
import { connect } from 'react-redux';
import getData from '../../helpers/getData';
import putData from '../../helpers/putData';
import { cstmStyle } from '../../assets/theme';
import { DataTable, Surface, Divider } from 'react-native-paper';
import Toast from 'react-native-root-toast';

import { FontAwesome5 } from '@expo/vector-icons';

import AwesomeAlert from 'react-native-awesome-alerts';
import HdrTitle from '../../components/HdrTitle';
import MyTxt from '../../components/MyTxt';

// end file upload.
import Moment from 'moment';


class MyOrders extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rows: '',
      showAlert: false,
      orderId: 0,
      loading: false
    };
  }


  componentDidMount() {

    this.fetchRows();

    this.focusListener = this.props.navigation.addListener('focus', () => {
      this.fetchRows();
    });
  }


  fetchRows() {
    getData(this.props.defaultSets.apiUrl + 'orders', this.props.userInfo.accessToken)
      //.then(response => response.json())
      .then(response => {
        this.setState({ rows: response.data.rows, loading: false });
        //console.log(response.data.rows)
      })
      .catch(function (error) {
        console.log(error);
      })
    this.props.navigation.setOptions({ headerTitle: <HdrTitle txt={this.props.selectedLanguage.myOrdersTxt} /> });

  }

  cancelOrder() {

    let ldToast = Toast.show(this.props.selectedLanguage.loadingTxt, {
      duration: 5000, //Toast.durations.LONG,
      position: 0, //Toast.positions.TOP
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      backgroundColor: '#ffc107' //dc3545 //7aa513
    });

    putData(this.props.defaultSets.apiUrl + 'orders/' + this.state.orderId, {
      id: this.state.orderId,
      orderStatus: 4, // cancelled
      langCode: this.props.selectedLanguage.langCode
    }, this.props.userInfo.accessToken)
      .then((response) => {
        Toast.hide(ldToast);

        this.fetchRows();

      })
      .catch(err => {
        console.log(err);
      });

  }

  returnPaymentStatus(paymentStatus) {
    switch (paymentStatus) {
      case 1: return this.props.selectedLanguage.verifiedTxt;
      case 2: return this.props.selectedLanguage.failedTxt;
      case 3: return this.props.selectedLanguage.waitingVerificationTxt;
      default: return this.props.selectedLanguage.loadingTxt;
    }
  }

  showAlert = (id) => {
    this.setState({
      showAlert: true, orderId: id
    });
  };

  hideAlert = () => {
    this.setState({
      showAlert: false
    });
  };


  returnOrderStatus(orderStatus) {
    //console.log(orderStatus)
    switch (orderStatus) {
      case 3: return this.props.selectedLanguage.doneTxt;
      case 4: return this.props.selectedLanguage.cancelledTxt;
      case 8: return this.props.selectedLanguage.pendingTxt;
      case 7: return this.props.selectedLanguage.returnedTxt;
      case 6: return this.props.selectedLanguage.sentToCargoTxt;
      default: return ''
    }
  }

  render() {

    const lng = this.props.selectedLanguage;
    const { rows, showAlert, loading, orderId } = this.state;

    return (

      <Background>


        <ScrollView style={{ flex: 1, width: '100%' }}
          refreshControl={
            <RefreshControl refreshing={loading} onRefresh={() => this.fetchRows()} />
          }
        >



          {rows ?

            rows.map((item, index) => {

              return (
                <Fragment key={item.id}>


                  <Surface
                    style={[cstmStyle.surface, { marginBottom: 10, backgroundColor: orderId == item.id ? '#efefef' : 'white' }]}>

                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', marginBottom: 30 }}>

                      <MyTxt txt={lng.orderIdTxt + ' : ' + item.orderId} fntWght='bold' />

                      <Text>|</Text>
                      <MyTxt txt={item.finalTotal + ' ' + lng.sarTxt} />


                    </View>

                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', marginBottom: 30 }}>


                      <View style={{ flexDirection: 'row' }}>
                        <MyTxt txt={lng.orderStatusTxt} fntWght='bold' style={{ fontSize: 14 }} />
                        <MyTxt style={{ fontSize: 14 }} txt={' : ' + this.returnOrderStatus(item.orderStatus)} />
                      </View>

                      <Text>|</Text>
                      <View style={{ flexDirection: 'row' }}>

                        <MyTxt txt={lng.paymentStatusTxt} fntWght='bold' style={{ fontSize: 14 }} />
                        <MyTxt style={{ fontSize: 12 }} txt={' : ' + this.returnPaymentStatus(item.paymentStatus)} />

                      </View>

                    </View>



                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', marginBottom: 30 }}>

                      <View style={{ flex: 0.5 }}>
                        <MyTxt txt={lng.dateTxt + ' : ' + Moment(item.created_at).format("YYYY-MM-DD")} />
                      </View>



                      <View style={{ flex: 0.5, flexDirection: 'row', justifyContent: 'flex-end' }}>
                        <TouchableOpacity

                          style={{ width: 30, marginHorizontal: 15 }}
                          onPress={() => this.setState({ orderId: item.id })}>
                          <FontAwesome5 name="eye" size={20} style={cstmStyle.textGreen} />

                        </TouchableOpacity>

                        <TouchableOpacity
                          style={{ width: 20 }}
                          onPress={() => this.showAlert(item.id)}>
                          <FontAwesome5 name="trash" size={20} style={cstmStyle.textDanger} />
                        </TouchableOpacity>
                      </View>

                    </View>


                    {orderId == item.id ?

                      item.order_items && item.order_items.map((row, k) => {
                        return (
                          <View key={'od' + row.id} style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', marginBottom: 30 }}>

                            <MyTxt style={{ width: 200 }} txt={eval("row.product.title" + lng.langCode)} />

                            <MyTxt txt={row.priceAfterDiscount + ' ' + lng.sarTxt} />
                            <Text>|</Text>
                            <MyTxt txt={lng.qtyTxt + ' : ' + row.quantity} />

                          </View>

                        );
                      }
                      )
                      : null}
                    <Divider />
                  </Surface>
                </Fragment>
              )
            }) :
            <MyTxt txt={lng.loadingTxt} />
          }


          <AwesomeAlert
            show={showAlert}
            showProgress={false}
            title={lng.confirmMsgTxt}
            message={lng.rUSureTxt}
            closeOnTouchOutside={true}
            closeOnHardwareBackPress={false}
            showCancelButton={true}
            showConfirmButton={true}
            cancelText={lng.noTxt}
            confirmText={lng.yesTxt}
            confirmButtonColor="#DD6B55"
            onCancelPressed={() => {
              this.hideAlert();
            }}
            onConfirmPressed={() => {
              this.cancelOrder();
              this.hideAlert();
            }}
          />
        </ScrollView>
      </Background>

    )
  }
}



const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
    defaultSets: state.DfltSts,
    userInfo: state.User.info,
  }
}
export default connect(mapStateToProps, null)(MyOrders);
