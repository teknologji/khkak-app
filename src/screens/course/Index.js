import React, { Component, Fragment } from 'react';
import { View, RefreshControl, ScrollView, TouchableOpacity, Image } from "react-native";
import Background from '../../components/Background';
import Button from '../../components/Button';
import Header from '../../components/Header';
import { connect } from 'react-redux';
import getData from '../../helpers/getData';
import deleteData from '../../helpers/deleteData';
import { cstmStyle } from '../../assets/theme';
import { DataTable, Searchbar } from 'react-native-paper';

import { FontAwesome5 } from '@expo/vector-icons';

import AwesomeAlert from 'react-native-awesome-alerts';
import HdrTitle from '../../components/HdrTitle';
import MyTxt from '../../components/MyTxt';

// end file upload.
//import Moment from 'moment';


class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rows: '',
      showAlert: false,
      rowToBeDlt: 0,
      trnsToBeDlt: 0,
      toDlt: '',
      loading: false,
      arrayholder: [],
      transOpen: 0
    };
  }


  componentDidMount() {

    this.fetchRows();

    this.focusListener = this.props.navigation.addListener('focus', () => {
      this.fetchRows();
    });

  }



  fetchRows() {
    getData(this.props.defaultSets.apiUrl + 'courses', this.props.userInfo.accessToken)
      //.then(response => response.json())
      .then(response => {
        this.setState({ arrayholder: response.data.rows, rows: response.data.rows, loading: false });
        //console.log(response.data.rows)
      })
      .catch(function (error) {
        console.log(error);
      })
    this.props.navigation.setOptions({ headerTitle: <HdrTitle txt={this.props.selectedLanguage.coursesTxt} /> });

  }

  /*  componentDidMount(){
     const { navigation } = this.props;
     this.focusListener = navigation.addListener('didFocus', () => {
       this.fetchOrders();
 
     });
   }
 
   componentWillUnmount() {
     // Remove the event listener
     this.focusListener.remove();
   } */
  deleteTrans() {

    //console.log('dlt trns', this.state.trnsToBeDlt)
    deleteData(this.props.defaultSets.apiUrl + 'courses-trans/' + this.state.trnsToBeDlt, this.props.userInfo.accessToken)
      .then(res => {
        this.fetchRows();
      })
      .catch(err => {
        console.log(err);
      });

  }

  deleteRow() {
    let id = this.state.rowToBeDlt;
    //console.log('dlt rowww', id)

    deleteData(this.props.defaultSets.apiUrl + 'courses/' + id, this.props.userInfo.accessToken)
      .then(res => {
        const filteredData = this.state.rows.filter(item => item.id !== id);
        this.setState({ rows: filteredData });

      })
      .catch(err => {
        console.log(err);
      });

  }
  showAlert = () => {
    this.setState({
      showAlert: true
    });
  };

  hideAlert = () => {
    this.setState({
      showAlert: false
    });
  };


  onChangeSearch(query) {
    //passing the inserted text in textinput
    let oRows = this.state.arrayholder;
    let newRows = [];

    for (let i = 0; i < oRows.length; i++) {

      for (let k = 0; k < oRows[i].elment_trans.length; k++) {
        if (oRows[i].elment_trans[k].title.toString().indexOf(query) > -1) {
          newRows.push(oRows[i]);
        }
      }


    }

    this.setState({
      //setting the filtered newData on datasource
      rows: newRows
    });

  }

  render() {

    const lng = this.props.selectedLanguage;
    const { loading, showAlert, rows, transOpen, toDlt } = this.state;

    return (

      <Background>

        <Button mode="contained" style={cstmStyle.bgOrange}
          onPress={() => this.props.navigation.navigate('CreateCourse')}
          icon="plus"
          compact={false}
          contentStyle={{ height: 30 }}
        >
          <MyTxt txt={lng.crtNwCrsTxt} />
        </Button>
        <Searchbar
          placeholder={lng.srchByNmTxt}
          onChangeText={(val) => this.onChangeSearch(val)}
          style={{ marginTop: 20 }}
        />

        <ScrollView style={{ flex: 1, width: '100%' }}
          refreshControl={
            <RefreshControl refreshing={loading} onRefresh={() => this.fetchRows()} />
          }
        >


          <DataTable style={cstmStyle.dataTbl}>
            <DataTable.Header>
              <DataTable.Title numberOfLines={2}>
                <MyTxt txt={lng.titleTxt} />
              </DataTable.Title>
              <DataTable.Title><MyTxt txt={lng.priceTxt} /></DataTable.Title>
              <DataTable.Title><MyTxt txt={lng.actionTxt} /></DataTable.Title>
            </DataTable.Header>


            {rows ?

              rows.map((item, index) => {

                const elmntTrns = item && item.elment_trans.find(row => row.languageCode == lng.langCode);

                return (
                  <Fragment key={'row' + index}>
                    <DataTable.Row style={{ backgroundColor: transOpen == item.id ? '#efefef' : 'transparent' }}>
                      <DataTable.Cell onPress={() => this.setState({ transOpen: item.id })}>
                        <MyTxt txt={elmntTrns ? elmntTrns.title : lng.noTrnsFndTxt} />
                      </DataTable.Cell>
                      <DataTable.Cell>{item.price}</DataTable.Cell>

                      <TouchableOpacity
                        style={{ width: 40, paddingTop: 15 }}
                        onPress={() => this.props.navigation.navigate('EditCourse', { id: item.id, courseTitle: elmntTrns ? elmntTrns.title : lng.noTrnsFndTxt })}>
                        <FontAwesome5 name="pencil-alt" size={20} style={cstmStyle.textGrey} />
                      </TouchableOpacity>

                      <TouchableOpacity
                        style={{ width: 40, paddingTop: 15 }}
                        onPress={() => { this.setState({ toDlt: 'elmnt', rowToBeDlt: item.id }, () => this.showAlert()) }}>
                        <FontAwesome5 name="trash" size={20} style={cstmStyle.textDanger} />
                      </TouchableOpacity>


                      <TouchableOpacity
                        style={{ width: 40, paddingTop: 15 }}
                        onPress={() => this.setState({ transOpen: item.id })}>
                        <FontAwesome5 name="globe" size={20} style={cstmStyle.textGreen} />
                      </TouchableOpacity>

                    </DataTable.Row>
                    {transOpen == item.id &&

                      <DataTable style={[cstmStyle.dataTbl, { marginBottom: 30, backgroundColor: '#efefef' }]}>
                        <DataTable.Header>
                          <DataTable.Title><MyTxt txt={lng.titleTxt} /></DataTable.Title>
                          <DataTable.Title><MyTxt txt={lng.languageCodeTxt} /></DataTable.Title>
                          <DataTable.Title>
                            <MyTxt txt={lng.actionTxt} />
                            <TouchableOpacity
                              style={{ width: 40, paddingTop: 15 }}
                              onPress={() => this.props.navigation.navigate('CreateCourseTrans', { id: item.id, courseTitle: elmntTrns ? elmntTrns.title : lng.noTrnsFndTxt })}>
                              <FontAwesome5 name="plus" size={20} style={cstmStyle.textGreen} />
                            </TouchableOpacity>
                          </DataTable.Title>
                        </DataTable.Header>

                        {item.elment_trans.map((trns, i) => {

                          return (elmntTrns ?
                            <DataTable.Row key={'trns' + i}>
                              <DataTable.Cell>{trns.title}</DataTable.Cell>
                              <DataTable.Cell>{trns.languageCode}</DataTable.Cell>



                              <TouchableOpacity
                                style={{ width: 40, paddingTop: 15 }}
                                onPress={() => this.props.navigation.navigate('EditCourseTrans', { id: trns.id, courseTitle: elmntTrns ? elmntTrns.title : lng.noTrnsFndTxt })}>
                                <FontAwesome5 name="pencil-alt" size={20} style={cstmStyle.textGrey} />
                              </TouchableOpacity>

                              <TouchableOpacity
                                style={{ width: 40, paddingTop: 15 }}
                                onPress={() => { this.setState({ toDlt: 'trns', trnsToBeDlt: trns.id }, () => this.showAlert()) }}>
                                <FontAwesome5 name="trash" size={20} style={cstmStyle.textDanger} />
                              </TouchableOpacity>



                            </DataTable.Row> : null
                          )
                        })}
                      </DataTable>
                    }
                  </Fragment>

                )
              }) :
              <DataTable.Row>
                <DataTable.Cell>{lng.loadingTxt}</DataTable.Cell>
              </DataTable.Row>
            }
            <DataTable.Pagination
              page={1}
              numberOfPages={3}
              onPageChange={page => {
                console.log(page);
              }}
              label="1-2 of 6"
            />
          </DataTable>

          <AwesomeAlert
            show={showAlert}
            showProgress={false}
            title={lng.confirmMsgTxt}
            message={lng.rUSureTxt}
            closeOnTouchOutside={true}
            closeOnHardwareBackPress={false}
            showCancelButton={true}
            showConfirmButton={true}
            cancelText={lng.noTxt}
            confirmText={lng.yesTxt}
            confirmButtonColor="#DD6B55"
            onCancelPressed={() => {
              this.hideAlert();
            }}
            onConfirmPressed={() => {
              if (toDlt == 'elmnt') {
                this.deleteRow();
              } else if (toDlt == 'trns') {
                this.deleteTrans();
              }
              this.hideAlert();
            }}
          />



        </ScrollView>
      </Background>

    )
  }
}



const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
    defaultSets: state.DfltSts,
    userInfo: state.User.info,
  }
}
export default connect(mapStateToProps, null)(Index);
