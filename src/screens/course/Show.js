import React, { Component } from 'react';
import { View, Text, StyleSheet, Share, ImageBackground } from "react-native";
import Background from '../../components/Background';

import { connect } from 'react-redux';
import Toast from 'react-native-root-toast';
import getData from '../../helpers/getData';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Carousel from 'react-native-snap-carousel';
import Swiper from 'react-native-swiper'


import MyTxt from '../../components/MyTxt';

import HdrTitle from '../../components/HdrTitle';
// end file upload.
//import Moment from 'moment';

import { DataTable, Surface } from 'react-native-paper';
import { cstmStyle } from '../../assets/theme';

class Show extends Component {
  constructor(props) {
    super(props);
    this.state = {
      row: {},
      elment_trans: [],
      isDisabled: false,
    };

  }

  componentDidMount() {
    this.props.navigation.setOptions({ headerTitle: <HdrTitle txt={this.props.route.params.courseTitle} /> });

    this.fetchData(this.props.route.params.id);
    //console.log('khaled', this.props.route.params.id)                
  }


  fetchData(elmntId) {
    getData(this.props.defaultSets.apiUrl + 'courses/'+elmntId, this.props.userInfo.accessToken)

      .then(response => {

        //console.log('cc', response.data.files);
        
        let data = response.data;
        let elmntTrns = data.elment_trans && data.elment_trans.find(item => item.languageCode == this.props.selectedLanguage.langCode);
  
        
        this.setState({
          row: data,
          elmntTrns,
          
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  }





  async onShare (title, url) {
  
    try {
      
      const result = await Share.share({
        message: (Platform.OS == 'ios')? title : title + ' \n ' + url,
        url: url
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  }

  render() {


    const { isDisabled, row, elmntTrns } = this.state;
    const lng = this.props.selectedLanguage;
    return (

      <Background>

{row.files && 

<Swiper 
      style={styles.swiperContainer} 
      showsButtons={false}
      loop={true}
      dotColor="#ccc"
      activeDotColor="#6a5acd"
      autoplay={true}
      key={row.files.length}
      >     
           
      {row.files.map( (item, i) =>  {
          return(
           
            <ImageBackground 
            style={{width: '100%', height: 200}}
            resizeMode= 'contain'            
            source={{ uri: item.imgUrl}}
            key={item.id}>     
          </ImageBackground> 
         
         );      
        
        })
       }


      </Swiper>

      }

{elmntTrns &&
<>
<Surface style={styles.surface}>

  <Text style={cstmStyle.prgrfTitle}> 
  <MyTxt txt={lng.cntntTxt} fntWght='bold'  />

  </Text>

<Text style={{padding: 20}}>
<MyTxt txt={elmntTrns.description} /> 
</Text>

  </Surface>
            
            <Surface style={cstmStyle.surface}>


<DataTable style={styles.dataTbl}>

    <DataTable.Row>
      <DataTable.Title>
      <MyTxt txt={lng.priceTxt} />
      </DataTable.Title>
      <DataTable.Cell>{row.price}</DataTable.Cell>
    </DataTable.Row>

    <DataTable.Row>
      <DataTable.Title>
      <MyTxt txt={lng.shareCourseTxt} />
      </DataTable.Title>
      <DataTable.Cell onPress={() => this.onShare(elmntTrns.title, row.priceAfterDiscount)}>
      <FontAwesome 
                name="share-alt" 
                
                style={{
                  position: "absolute", 
                  right:30, top: 20,
                  fontSize: 20,
                  color: '#cd5700'  
                  }}
                />
        </DataTable.Cell>
    </DataTable.Row>

    
    {/* <DataTable.Pagination
      page={1}
      numberOfPages={1}
      onPageChange={page => {
        console.log(page);
      }}
      label=""
    /> */}
  </DataTable>


</Surface>
</>
}

        {/* {row.files && row.files.map((file, i) => {
          return (
            <View key={file.id}>

              <Thumbnail imgUrl={file.imgUrl} />
              <TouchableOpacity onPress={() => this.removeImg(file.id)}>
                <FontAwesome name="window-close" size={28} color="#900" />
              </TouchableOpacity>
            </View>
          )
        })
        } */}



      </Background>

    )
  }
}

const styles = StyleSheet.create({
  dataTbl: {
    justifyContent: 'center',
    backgroundColor: '#fff',
    padding: 8,
    marginTop: 10
  },
 
  swiperContainer: {
    height: 200,
    marginBottom: 10
  },

});


const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
    defaultSets: state.DfltSts,
    userInfo: state.User.info,
  }
}
export default connect(mapStateToProps, null)(Show);
