import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, Image } from "react-native";
import Button from '../../components/Button';
import TextInput from '../../components/TextInput';
import { connect } from 'react-redux';
import Toast from 'react-native-root-toast';
import postData from '../../helpers/postData';
import getData from '../../helpers/getData';
import { cstmStyle } from '../../assets/theme';
import { Surface } from 'react-native-paper';

import DropDownPicker from 'react-native-dropdown-picker';
import FontAwesome from 'react-native-vector-icons/FontAwesome';


//file upload.

import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import MyTxt from '../../components/MyTxt';
import Background from '../../components/Background';
import HdrTitle from '../../components/HdrTitle';
// end file upload.
//import Moment from 'moment';


class Create extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      description: '',
      languageCode: 'AR',
      price: '',
      errorMessage: '',
      isDisabled: false,
      aFile: null,
      languages: []
    };

  }

  componentDidMount() {
    this.props.navigation.setOptions({ headerTitle: <HdrTitle txt={this.props.selectedLanguage.crtNwCrsTxt} /> });
    this.fetchLanguagues();

  }

  
  fetchLanguagues() {
    getData(this.props.defaultSets.apiUrl + 'languages', this.props.userInfo.accessToken)
      //.then(response => response.json())
      .then(response => {

        let languages = [];

        response.data.rows.map((item, i) => {
          languages.push({ 
            label: item.title,
            value: item.languageCode
            //, selected: true 
          });
        });

        this.setState({ languages }); 
      })
      .catch(function (error) {
        console.log(error);
      })
    this.props.navigation.setOptions({ headerTitle: <HdrTitle txt={this.props.selectedLanguage.coursesTxt} /> });

  }

  /* onChangeValue(e) {
      //console.log(e.target.value)
      this.setState({
          [e.target.name]: e.target.value, isDisabled: false
      });
  } */


  hndlChsPhoto = () => {
    const options = {
      noData: true,
    }
    ImagePicker.launchImageLibrary(options, response => {
      if (response.uri) {
        this.setState({ photo: response })
      }
    })
  }

  getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
  }



  _takePhoto = async () => {
    let result = await ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      //aspect: [4, 3],
      quality: 1
    });



    if (result.uri) {
      this.setState({ aFile: result });
    }
  };

  _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      //aspect: [4, 3],
      quality: 1
    });



    if (result.uri) {
      this.setState({ aFile: result });
    }
  };



  async upldAFile(rowId) {


    let attachedFile = this.state.aFile;
    if (attachedFile != null) {


      const data = new FormData();

      data.append('rowId', rowId);
      data.append('type', 'course');

      let filePath = attachedFile.uri;
      let fileExtention = filePath.substring(filePath.lastIndexOf(".") + 1, filePath.length);
      data.append('fileExtention', fileExtention);

      data.append('files[]', { // o Halid, these 3 paramters are neccessary for the object to be sent 
        uri: filePath,
        type: "image/jpeg", // it's very important to android
        name: 'khaled'
      }
      );

      fetch(this.props.defaultSets.apiUrl + "upload-files", { // give something like https://xx.yy.zz/upload/whatever
        method: 'POST',
        body: data,
      })
        .then(response => response.json())
        .then(response => {
          // console.log(response)        
        }
        ).catch(
          error => console.log('uploadImagek error:', error.response.data)
        );
    }



  }



  onChangeValue = (key, val) => {

    this.setState({ [key]: val })
  }
  handleSubmit = () => {

    if (this.state.aFile == null) { // check user files.

      /* return Toast.show(this.props.selectedLanguage.plsUpldUsfFlTxt, {
        duration: 10000, //Toast.durations.LONG,
        position: 0, //Toast.positions.TOP
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 0,
        backgroundColor: '#dc3545' //dc3545 //7aa513
      }) */
    }

    this.setState({ isDisabled: true });

    let ldToast = Toast.show(this.props.selectedLanguage.loadingTxt, {
      duration: 10000, //Toast.durations.LONG,
      position: 0, //Toast.positions.TOP
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      backgroundColor: '#7aa513' //dc3545 //7aa513
    })


    //return console.log(this.state.countryInfo.phoneCode+this.state.phone)
    postData(this.props.defaultSets.apiUrl + 'courses', {

      title: this.state.title,
      description: this.state.description,
      price: this.state.price,
      languageCode: this.state.languageCode,
      langCode: this.props.selectedLanguage.langCode,
      fromMobApp: 1,
    }, this.props.userInfo.accessToken)
      .then((response) => {
        Toast.hide(ldToast);

        if (response.data.success == true) {

          //upload user files.
          this.upldAFile(response.data.rowId);
          this.setState({ aFile: null });

          Toast.show(response.data.msg, {
            duration: 3000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#7aa513' //dc3545 //7aa513
          });

          /* setTimeout( () => {
              this.props.navigation.navigate('Home');
          }, 3000); */

        } else {
          Toast.show(response.data.msg, {
            duration: 10000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#dc3545' //dc3545 //7aa513
          });
        }
      })
      .then((response) => response.json())
      .catch((error) => {
        //console.log(error.response.status, this.props.defaultSets.apiUrl+'register')

        //return console.log(error);
        this.setState({ isDisabled: false })

        if (error.response !== undefined && error.response.status === 422) {
          let errorTxt = '';
          let errorMessage = error.response.data.errors;

          Object.keys(errorMessage).map((key, i) => {
            errorTxt = errorTxt + ' ' + errorMessage[key][0] + "";
            //errorTxt = errorMessage[key][0];
          });
          Toast.hide(ldToast);

          Toast.show(<MyTxt txt={errorTxt} style={{ lineHeight: 28 }} />, {
            duration: 6000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#dc3545' //dc3545 //7aa513
          });
        }
      });

  }


  render() {


    const { isDisabled, aFile, languageCode, languages } = this.state;
    const lng = this.props.selectedLanguage;

    return (

      <Background>


<ScrollView style={{ flex: 1, width: '100%' }}>
          <Surface style={styles.surface}>

        <View style={cstmStyle.upldAFile}>
          <FontAwesome name='cloud-upload' style={cstmStyle.textOrange} size={28} />

          <Button onPress={this._pickImage}>
            {lng.prssHr2UpldFlsTxt}
          </Button>

        </View>

        <View style={cstmStyle.upldAFile}>

        <FontAwesome name='camera' style={cstmStyle.textOrange} size={28} />

          <Button onPress={this._takePhoto}>
            {lng.prssHr2CmraTxt}
          </Button>

        </View>
        {aFile &&
          <View style={[cstmStyle.upldAFile, { height: 110 }]}>

            <Image source={{ uri: aFile.uri }} style={{
              width: '90%',
              height: 100,
              resizeMode: 'contain'
            }} />
          </View>}

{languages &&
          <DropDownPicker
    items={languages}
    defaultIndex={0}
    containerStyle={{height: 60}}
    onChangeItem={(item) => this.onChangeValue('languageCode', item.value)}
    placeholder={lng.languageCodeTxt}

/>}

        <TextInput
          label={lng.titleTxt}
          returnKeyType="next"
          onChangeText={(val) => this.onChangeValue('title', val)}
        />

        <TextInput
          label={lng.priceTxt}
          returnKeyType="next"
          onChangeText={(val) => this.onChangeValue('price', val)}
          /* error={true} */
          /* errorText={'kk'} */
          maxLength={6}
          autoCapitalize="none"
          autoCompleteType="tel"
          textContentType="telephoneNumber"
          keyboardType="phone-pad"
        />

        <TextInput
          multiline={true}
          numberOfLines={6}
          label={lng.descriptionTxt}
          onChangeText={(val) => this.onChangeValue('description', val)}
        />

        <Button mode="contained"
          isDisabled={isDisabled}
          onPress={this.handleSubmit}>
          {lng.saveTxt}
        </Button>
        </Surface>

</ScrollView>
      </Background>

    )
  }
}


const styles = StyleSheet.create({
  surface: {
    padding: 28,
    margin: 15,
    elevation: 5,
  },
  row: {
    flexDirection: 'row',
    marginTop: 4,
    marginBottom: 15
  },


});




const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
    defaultSets: state.DfltSts,
    userInfo: state.User.info,
  }
}
export default connect(mapStateToProps, null)(Create);
