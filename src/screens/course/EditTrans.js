import React, { Component } from 'react';
import { View, ScrollView, StyleSheet } from "react-native";
import Button from '../../components/Button';
import TextInput from '../../components/TextInput';
import { connect } from 'react-redux';
import Toast from 'react-native-root-toast';
import getData from '../../helpers/getData';
import putData from '../../helpers/putData';
import { Surface } from 'react-native-paper';
import DropDownPicker from 'react-native-dropdown-picker';

import MyTxt from '../../components/MyTxt';
import HdrTitle from '../../components/HdrTitle';
import Background from '../../components/Background';
// end file upload.
//import Moment from 'moment';


class EditTrans extends Component {
  constructor(props) {
    super(props);
    this.state = {
      row: {},
      id: this.props.route.params.id,
      title: '',
      description: '',
      languageCode: 'EN',
      errorMessage: '',
      isDisabled: false,
      languages: [],
    };

  }

 

  fetchLanguagues() {
    getData(this.props.defaultSets.apiUrl + 'languages', this.props.userInfo.accessToken)
      //.then(response => response.json())
      .then(response => {

        let languages = [];

        response.data.rows.map((item, i) => {

          languages.push({ 
            label: item.title,
            value: item.languageCode
            //, selected: true 
          });
        });

        this.setState({ languages }); 
      })
      .catch(function (error) {
        console.log(error);
      })
    this.props.navigation.setOptions({ headerTitle: <HdrTitle txt={this.props.selectedLanguage.coursesTxt} /> });

  }

  onChangeValue = (key, val) => {

    this.setState({ [key]: val })
  }

  componentDidMount(){
    this.fetchLanguagues();
    
    this.fetchData(this.state.id);    
    this.props.navigation.setOptions({ headerTitle: <HdrTitle txt={this.props.route.params.updateTransTxt} /> });
  }


fetchData(elmntId) {
  getData(this.props.defaultSets.apiUrl+'courses-trans/'+elmntId+'/edit', this.props.userInfo.accessToken)

      .then(response => {

        this.setState({
          row: response.data,
          title: response.data.title,
          description: response.data.description,
          languageCode: response.data.languageCode,
        });
        //console.log(response.data.files[0].imgUrl)
      })
      .catch(function (error) {
          console.log(error);
      });
}

  handleSubmit = () => {

    this.setState({ isDisabled: true });

    let ldToast = Toast.show(this.props.selectedLanguage.loadingTxt, {
      duration: 10000, //Toast.durations.LONG,
      position: 0, //Toast.positions.TOP
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      backgroundColor: '#E0A800' //dc3545 //7aa513 //#23272B
    })

    //return console.log(this.state.countryInfo.phoneCode+this.state.phone)
    putData(this.props.defaultSets.apiUrl+'courses-trans/'+this.state.id, {
      id: this.state.id,            
      title: this.state.title,
      description: this.state.description,
      languageCode: this.state.languageCode,
      langCode: this.props.selectedLanguage.langCode,
    }, this.props.userInfo.accessToken)
      .then((response) => {

        if (response.data.success == true) {

            Toast.hide(ldToast);
     
            Toast.show(response.data.msg, {
              duration: 3000, //Toast.durations.LONG,
              position: 0, //Toast.positions.TOP
              shadow: true,
              animation: true,
              hideOnPress: true,
              delay: 0,
              backgroundColor: '#7aa513' //dc3545 //7aa513 //#E0A800 //#23272B
            });
            

        } else {
          Toast.hide(ldToast);

          Toast.show(response.data.msg, {
            duration: 10000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#dc3545' //dc3545 //7aa513 //#23272B
          });
        }
      })
      .then((response) => response.json())
      .catch((error) => {
        //console.log(error.response.status, this.props.defaultSets.apiUrl+'register')

        //return console.log(error.response);
        this.setState({ isDisabled: false })

        if (error.response !== undefined) {

          if( error.response.status === 422 ) {

          
          let errorTxt = '';
          let errorMessage = error.response.data.errors;

          Object.keys(errorMessage).map((key, i) => {
            errorTxt = errorTxt + ' '+ errorMessage[key][0] + "\n";
            //errorTxt = errorMessage[key][0];
          });
          Toast.hide(ldToast);

          Toast.show(<MyTxt txt={errorTxt} style={{ lineHeight: 28 }} />, {
            duration: 6000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#dc3545' //dc3545 //7aa513 //#23272B
          });
        
        } else if (error.response.status === 401) {
          Toast.show(<MyTxt txt={plsLgnFrst} style={{color: '#fff'}} />, {
            duration: 3000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#dc3545' //dc3545 //7aa513 //#23272B
          });
        }
        }
      });

  }


  render() {


    const { isDisabled, title, description, languageCode, languages } = this.state;
    const lng = this.props.selectedLanguage;

    return (

      <Background>


      <ScrollView style={{ flex: 1, width: '100%' }}>
        <Surface style={styles.surface}>
{languages && 
        <DropDownPicker
  items={languages}
  defaultIndex={0}
  containerStyle={{height: 60}}
  onChangeItem={(item) => this.onChangeValue('languageCode', item.value)}
  placeholder={lng.languageCodeTxt}

/>}
         
          <TextInput
          value={title}
            label={lng.titleTxt}
            returnKeyType="next"
            onChangeText={(val) => this.onChangeValue('title', val)}
          />

          <TextInput
          value={description}
            multiline={true}
            numberOfLines={6}
            label={lng.descriptionTxt}
            onChangeText={(val) => this.onChangeValue('description', val)}
          />

          <Button mode="contained"
            isDisabled={isDisabled}
            onPress={this.handleSubmit}>
            {lng.saveTxt}
          </Button>
        </Surface>

      </ScrollView>
    </Background>
    )
  }
}


const styles = StyleSheet.create({
 
  row: {
    flexDirection: 'row',
    marginTop: 4,
    marginBottom: 15
  },
 

});




const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
    defaultSets: state.DfltSts,
    userInfo: state.User.info,
  }
}
export default connect(mapStateToProps, null)(EditTrans);
