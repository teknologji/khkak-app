import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, Image } from "react-native";
import Background from '../../components/Background';
import Button from '../../components/Button';
import TextInput from '../../components/TextInput';
import { connect } from 'react-redux';
import Toast from 'react-native-root-toast';
import postData from '../../helpers/postData';
import getData from '../../helpers/getData';
import DropDownPicker from 'react-native-dropdown-picker';
import { Surface } from 'react-native-paper';

//file upload.

import MyTxt from '../../components/MyTxt';
import HdrTitle from '../../components/HdrTitle';
// end file upload.


class CreateTrans extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      description: '',
      languageCode: 'EN',
      errorMessage: '',
      isDisabled: false,
      languages: [],
    };

  }

  componentDidMount() {
    this.props.navigation.setOptions({ headerTitle: <HdrTitle txt={this.props.route.params.addTransTxt} /> });
    this.fetchLanguagues();

  }



  fetchLanguagues() {
    getData(this.props.defaultSets.apiUrl + 'languages', this.props.userInfo.accessToken)
      //.then(response => response.json())
      .then(response => {

        let languages = [];

        response.data.rows.map((item, i) => {
          languages.push({ 
            label: item.title,
            value: item.languageCode
            //, selected: true 
          });
        });

        this.setState({ languages }); 
      })
      .catch(function (error) {
        console.log(error);
      })
    this.props.navigation.setOptions({ headerTitle: <HdrTitle txt={this.props.selectedLanguage.coursesTxt} /> });

  }


  onChangeValue = (key, val) => {

    this.setState({ [key]: val })
  }

  handleSubmit = () => {
//return console.log(this.state.languageCode)

    this.setState({ isDisabled: true });

    let ldToast = Toast.show(this.props.selectedLanguage.loadingTxt, {
      duration: 10000, //Toast.durations.LONG,
      position: 0, //Toast.positions.TOP
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      backgroundColor: '#ffc107' //dc3545 //7aa513
    })


    //return console.log(this.state.countryInfo.phoneCode+this.state.phone)
    postData(this.props.defaultSets.apiUrl + 'courses-trans', {
      rowId: this.props.route.params.id,
      title: this.state.title,
      description: this.state.description,
      languageCode: this.state.languageCode,
      langCode: this.props.selectedLanguage.langCode,
    }, this.props.userInfo.accessToken)
      .then((response) => {
        Toast.hide(ldToast);

        if (response.data.success == true) {

          Toast.show(response.data.msg, {
            duration: 3000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#7aa513' //dc3545 //7aa513
          });

          /* setTimeout( () => {
              this.props.navigation.navigate('Home');
          }, 3000); */

        } else {
          Toast.show(response.data.msg, {
            duration: 10000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#dc3545' //dc3545 //7aa513
          });
        }
      })
      .then((response) => response.json())
      .catch((error) => {
        //console.log(error.response.status, this.props.defaultSets.apiUrl+'register')

        //return console.log(error);
        this.setState({ isDisabled: false })

        if (error.response !== undefined && error.response.status === 422) {
          let errorTxt = '';
          let errorMessage = error.response.data.errors;

          Object.keys(errorMessage).map((key, i) => {
            errorTxt = errorTxt + ' ' + errorMessage[key][0] + "";
            //errorTxt = errorMessage[key][0];
          });
          Toast.hide(ldToast);

          Toast.show(<MyTxt txt={errorTxt} style={{ lineHeight: 28 }} />, {
            duration: 6000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#dc3545' //dc3545 //7aa513
          });
        }
      });

  }


  render() {


    const { isDisabled, languages, languageCode } = this.state;
    const lng = this.props.selectedLanguage;

    return (

      <Background>


        <ScrollView style={{ flex: 1, width: '100%' }}>
          <Surface style={styles.surface}>

          <DropDownPicker
    items={languages}
    defaultIndex={0}
    containerStyle={{height: 60}}
    onChangeItem={(item) => this.onChangeValue('languageCode', item.value)}
    placeholder={lng.languageCodeTxt}

/>
           
            <TextInput
              label={lng.titleTxt}
              returnKeyType="next"
              onChangeText={(val) => this.onChangeValue('title', val)}
            />

            <TextInput
              multiline={true}
              numberOfLines={6}
              label={lng.descriptionTxt}
              onChangeText={(val) => this.onChangeValue('description', val)}
            />

            <Button mode="contained"
              isDisabled={isDisabled}
              onPress={this.handleSubmit}>
              {lng.saveTxt}
            </Button>
          </Surface>

        </ScrollView>
      </Background>

    )
  }
}


const styles = StyleSheet.create({
  surface: {
    padding: 28,
    margin: 15,
    elevation: 5,
  },
  row: {
    flexDirection: 'row',
    marginTop: 4,
    marginBottom: 15
  },


});




const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
    defaultSets: state.DfltSts,
    userInfo: state.User.info,
  }
}
export default connect(mapStateToProps, null)(CreateTrans);
