import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from "react-native";
import Background from '../../components/Background';
import Button from '../../components/Button';
import TextInput from '../../components/TextInput';
import Header from '../../components/Header';
import { connect } from 'react-redux';
import Toast from 'react-native-root-toast';
import getData from '../../helpers/getData';
import putData from '../../helpers/putData';
import { cstmStyle, theme } from '../../assets/theme';
import {
  TouchableRipple,
} from 'react-native-paper';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import Thumbnail from '../../components/Thumbnail';


//file upload.

import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import MyTxt from '../../components/MyTxt';
import HdrTitle from '../../components/HdrTitle';
// end file upload.
//import Moment from 'moment';


class Edit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      row: {},
      id: this.props.route.params.id,
      price: '',
      status: '',
      pgTitle: '',
      errorMessage: '',
      isDisabled: false,
      aFile: null,
    };

  }

  /* onChangeValue(e) {
      //console.log(e.target.value)
      this.setState({
          [e.target.name]: e.target.value, isDisabled: false
      });
  } */


  hndlChsPhoto = () => {
    const options = {
      noData: true,
    }
    ImagePicker.launchImageLibrary(options, response => {
      if (response.uri) {
        this.setState({ photo: response })
      }
    })
  }

  getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
  }



  _takePhoto = async () => {
    let result = await ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      //aspect: [4, 3],
      quality: 1
    });



    if (result.uri) {
      this.setState({ aFile: result });
    }
  };

  _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      //aspect: [4, 3],
      quality: 1
    });



    if (result.uri) {
      this.setState({ aFile: result });
    }
  };



  async upldAFile(rowId) {


    let attachedFile = this.state.aFile;
    if (attachedFile != null) {

      
    const data = new FormData();

    data.append('rowId', rowId);
    data.append('type', 'course');

      let filePath = attachedFile.uri;
      let fileExtention = filePath.substring(filePath.lastIndexOf(".") + 1, filePath.length);
      data.append('fileExtention', fileExtention);
      
      data.append('files[]', { // o Halid, these 3 paramters are neccessary for the object to be sent 
        uri: filePath,
        type: "image/jpeg", // it's very important to android
        name: 'khaled'
      }
      );

      await fetch(this.props.defaultSets.apiUrl + "upload-files", { // give something like https://xx.yy.zz/upload/whatever
        method: 'POST',
        body: data,
      })
        //.then(response => response.json())
        .then(response => {
         // console.log(response)  
         return true;      
        }
        ).catch(
          error => console.log('uploadImagek error:', error.response.data)
        );
    }

  

  }

  onChangeValue = (key, val) => {

    this.setState({ [key]: val })
  }

  componentDidMount(){
    this.setState({pgTitle: this.props.route.params.courseTitle});

    this.fetchData(this.state.id);    
    this.props.navigation.setOptions({ headerTitle: <HdrTitle txt={this.props.route.params.courseTitle} /> });
  }


fetchData(elmntId) {
  getData(this.props.defaultSets.apiUrl+'courses/'+elmntId+'/edit', this.props.userInfo.accessToken)

      .then(response => {

        this.setState({
          row: response.data,
          price: response.data.price,
          status: response.data.status,
        });
        //console.log(response.data.files[0].imgUrl)
      })
      .catch(function (error) {
          console.log(error);
      });
}

  handleSubmit = () => {

    this.setState({ isDisabled: true });

    let ldToast = Toast.show(this.props.selectedLanguage.loadingTxt, {
      duration: 10000, //Toast.durations.LONG,
      position: 0, //Toast.positions.TOP
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      backgroundColor: '#E0A800' //dc3545 //7aa513 //#23272B
    })

    //return console.log(this.state.countryInfo.phoneCode+this.state.phone)
    putData(this.props.defaultSets.apiUrl+'courses/'+this.state.id, {
      id: this.state.id,            
      price: this.state.price,
      status: this.state.status,
      langCode: this.props.selectedLanguage.langCode,
      fromMobApp: 1,
    }, this.props.userInfo.accessToken)
      .then((response) => {

        if (response.data.success == true) {

          //upload user files.
          this.upldAFile(response.data.rowId);  

            Toast.hide(ldToast);
     
            this.setState({ aFile: null });
            //this.fetchData(this.state.id)
  //console.log(response.data)
            Toast.show(response.data.msg, {
              duration: 3000, //Toast.durations.LONG,
              position: 0, //Toast.positions.TOP
              shadow: true,
              animation: true,
              hideOnPress: true,
              delay: 0,
              backgroundColor: '#7aa513' //dc3545 //7aa513 //#E0A800 //#23272B
            });
            

        } else {
          Toast.hide(ldToast);

          Toast.show(response.data.msg, {
            duration: 10000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#dc3545' //dc3545 //7aa513 //#23272B
          });
        }
      })
      .then((response) => response.json())
      .catch((error) => {
        //console.log(error.response.status, this.props.defaultSets.apiUrl+'register')

        //return console.log(error.response);
        this.setState({ isDisabled: false })

        if (error.response !== undefined) {

          if( error.response.status === 422 ) {

          
          let errorTxt = '';
          let errorMessage = error.response.data.errors;

          Object.keys(errorMessage).map((key, i) => {
            errorTxt = errorTxt + ' '+ errorMessage[key][0] + "\n";
            //errorTxt = errorMessage[key][0];
          });
          Toast.hide(ldToast);

          Toast.show(<MyTxt txt={errorTxt} style={{ lineHeight: 28 }} />, {
            duration: 6000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#dc3545' //dc3545 //7aa513 //#23272B
          });
        
        } else if (error.response.status === 401) {
          Toast.show(<MyTxt txt={plsLgnFrst} style={{color: '#fff'}} />, {
            duration: 3000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#dc3545' //dc3545 //7aa513 //#23272B
          });
        }
        }
      });

  }

  removeImg(id){
    getData(this.props.defaultSets.apiUrl+'remove-file/'+id+'/course', this.props.userInfo.accessToken)
      .then(res => {
        let currentRow = this.state.row;
          const filteredData = currentRow.files.filter(item => item.id !== id);
          currentRow.files = filteredData;
          this.setState({ row: currentRow });
              
      })
      .catch(err => {
          console.log(err);
      });
  }

  render() {


    const { isDisabled, aFile, row, price, status, pgTitle } = this.state;
    const lng = this.props.selectedLanguage;

    return (

      <Background>




        <View style={cstmStyle.upldAFile}> 
        <FontAwesome name='cloud-upload' style={cstmStyle.textOrange} size={28} />

          <Button onPress={this._pickImage}>
            {lng.prssHr2UpldFlsTxt}
          </Button>
          </View>

          <View style={cstmStyle.upldAFile}> 
          <FontAwesome name='camera' style={cstmStyle.textOrange} size={28} />

          <Button onPress={this._takePhoto}>
            {lng.prssHr2CmraTxt}
          </Button>
          
            
        </View>

        {aFile &&   
          <View style={[cstmStyle.upldAFile, {height: 110}]}> 
<Image source={{ uri: aFile.uri }} style={{ 
              width: '90%', 
              height: 100,
              resizeMode: 'contain' 
              }} />
</View>}

        <TextInput
          label={lng.priceTxt}
          value={String(price)}
          returnKeyType="next"
          onChangeText={(val) => this.onChangeValue('price', val)}
          /* error={true} */
          keyboardType="phone-pad"
          maxLength={6}
        />



<View style={[cstmStyle.upldAFile, {height: 120, marginTop: 20, padding: 10}]}> 

{row.files && row.files.map((file, i) => {
return(
  <View key={file.id} style={{flex: 1, justifyContent: 'space-between', flexDirection: 'row'}}>
    
    <View style={{flex: 0.5}}>
    <Thumbnail imgUrl={file.imgUrl} />
  </View>


  <View style={{flex: 0.5}}>
  <TouchableRipple onPress={() => this.removeImg(file.id)}>
  <FontAwesome name="window-close" size={28} color="#900" />
  </TouchableRipple>
  </View>
  </View>
)
  })
}

</View>

        <Button mode="contained"
          isDisabled={isDisabled}
          onPress={this.handleSubmit}>
          {lng.saveTxt}
        </Button>
     </Background>

    )
  }
}


const styles = StyleSheet.create({
 
  row: {
    flexDirection: 'row',
    marginTop: 4,
    marginBottom: 15
  },
 

});




const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
    defaultSets: state.DfltSts,
    userInfo: state.User.info,
  }
}
export default connect(mapStateToProps, null)(Edit);
