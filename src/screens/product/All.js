import React, { Component, Fragment } from 'react';
import { View, SafeAreaView, Text, FlatList } from "react-native";
import Background from '../../components/Background';
import { connect } from 'react-redux';
import getData from '../../helpers/getData';
import deleteData from '../../helpers/deleteData';
import HdrTitle from '../../components/HdrTitle';
import ProductBox from '../../components/ProductBox';
import { cstmStyle } from '../../assets/theme';
import MyTxt from '../../components/MyTxt';
import { Surface } from 'react-native-paper';
// end file upload.
//import Moment from 'moment';


class All extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categoryId: this.props.route.params.id,
      pgTitle: this.props.route.params.title,
      rows: '',
      loading: true,
      arrayholder: [],
    };
  }


  componentDidMount() {

    this.fetchRows();

    /* this.focusListener = this.props.navigation.addListener('focus', () => {
      this.fetchRows();
    }); */

  }


  fetchRows() {

    let categoryId = this.state.categoryId;
    console.log(this.state.categoryId, this.state.pgTitle)
    getData(this.props.defaultSets.apiUrl + 'prdcts-by-catId/' + categoryId)
      .then(response => {
        //let allPrdctsChsn = categoryId == 'all' ? true: false;
        //alert(allPrdctsChsn)
        this.setState({
          arrayholder: response.data.rows,
          rows: response.data.rows,
          loading: false,
          //allPrdctsChsn
        });
        //console.log(response.data)
      })
      .catch(function (error) {
        console.log(error);
      });
    this.props.navigation.setOptions({ headerTitle: <HdrTitle txt={this.state.pgTitle} /> });

  }



  onChangeSearch(query) {
    //passing the inserted text in textinput
    let oRows = this.state.arrayholder;
    let newRows = [];

    for (let i = 0; i < oRows.length; i++) {

      for (let k = 0; k < oRows[i].elment_trans.length; k++) {
        if (oRows[i].elment_trans[k].title.toString().indexOf(query) > -1) {
          newRows.push(oRows[i]);
        }
      }


    }

    this.setState({
      //setting the filtered newData on datasource
      rows: newRows
    });

  }

  render() {

    const lng = this.props.selectedLanguage;
    const { loading, rows, pgTitle } = this.state;

    return (

      <Background>
        <SafeAreaView style={{ flex: 1, width: '100%' }}>

          {rows ?
            <Surface style={[cstmStyle.surface, { padding: 10, flexDirection: 'row', justifyContent: 'space-between' }]}>
              <MyTxt fntWght='bold' txt={pgTitle} />
              <MyTxt style={{ fontSize: 12 }} txt={lng.productsCountTxt + ':' + rows.length} />
            </Surface> : null}

          {rows ?
            <FlatList
              refreshing={loading}
              data={rows}
              renderItem={({ item }) => <ProductBox item={item}
                navigation={this.props.navigation} />}
              keyExtractor={item => item.id.toString()}
              numColumns={2}
            /> : null}


        </SafeAreaView>
      </Background>

    )
  }
}



const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
    defaultSets: state.DfltSts,
    userInfo: state.User.info,
  }
}
export default connect(mapStateToProps, null)(All);
