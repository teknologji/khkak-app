import React, { Component } from 'react';
import { StyleSheet, Text, ScrollView, Share, ImageBackground } from "react-native";
import Background from '../../components/Background';

import { connect } from 'react-redux';
import Toast from 'react-native-root-toast';
import getData from '../../helpers/getData';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Button from '../../components/Button';
import Swiper from 'react-native-swiper'


import MyTxt from '../../components/MyTxt';

import HdrTitle from '../../components/HdrTitle';
// end file upload.
//import Moment from 'moment';

import { DataTable, Surface } from 'react-native-paper';
import { cstmStyle } from '../../assets/theme';

class Show extends Component {
  constructor(props) {
    super(props);
    this.state = {
      row: {},
      elment_trans: [],
      isDisabled: false,
    };

  }

  componentDidMount() {

    this.fetchData(this.props.route.params.id);
    //console.log('khaled', this.props.route.params.id)                
  }


  fetchData(elmntId) {
    getData(this.props.defaultSets.apiUrl + 'products/'+elmntId)

      .then(response => {

        this.setState({
          row: response.data,          
        });
        this.props.navigation.setOptions({ headerTitle: <HdrTitle txt={this.props.selectedLanguage.productDetailsTxt} /> });

      })
      .catch(function (error) {
        console.log(error);
      });
  }





  async onShare (title, url) {
  
    try {
      
      const result = await Share.share({
        message: (Platform.OS == 'ios')? title : title + ' \n ' + url,
        url: url
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  }

  render() {


    const { isDisabled, row } = this.state;
    const lng = this.props.selectedLanguage;
    return (

      <Background>

<ScrollView style={{ flex: 1, width: '100%' }}>
{row.files && 
<Swiper 
      style={styles.swiperContainer} 
      showsButtons={false}
      loop={true}
      dotColor="#ccc"
      activeDotColor="#dc3545"
      autoplay={true}
      key={row.files.length}
      >     
           
      {row.files.map( (item, i) =>  {
          return(
           
            <ImageBackground 
            style={{width: '100%', height: 200}}
            resizeMode= 'contain'            
            source={{ uri: item.imgUrl}}
            key={item.id}>     
            
          </ImageBackground> 
         
         );      
        
        })
       }


      </Swiper>

      }

{row &&
<>

            
           
<Surface style={cstmStyle.surface}> 
<Button 
           onPress={() => {
            this.props.addItemToCart(row); 
            //this.props.animateOnPress(photoUrl);        
            
            }}
          mode="contained" 
          style={[cstmStyle.bgGrey]}
          icon="cart"
          compact={false}
          contentStyle={{height: 40}}
          >          
            <MyTxt txt={this.props.selectedLanguage.addToCartTxt} />
          </Button>


          </Surface>
            <Surface style={cstmStyle.surface}>


<DataTable style={styles.dataTbl}>

<DataTable.Row>
      <DataTable.Title>
      <MyTxt txt={lng.titleTxt} />
      </DataTable.Title>
      <DataTable.Cell>
      {row && eval('row.title'+lng.langCode)}
        </DataTable.Cell>
    </DataTable.Row>

    <DataTable.Row>
      <DataTable.Title>
      <MyTxt txt={lng.priceTxt} />
      </DataTable.Title>
      <DataTable.Cell>
      {row.priceAfterDiscount == row.priceBeforeDiscount || row.priceAfterDiscount == 0 ?
row.priceBeforeDiscount
:row.priceAfterDiscount}
        </DataTable.Cell>
    </DataTable.Row>

    <DataTable.Row>
      <DataTable.Title>
      <MyTxt txt={lng.skuTxt} />
      </DataTable.Title>
      <DataTable.Cell>
      {row.sku}
        </DataTable.Cell>
    </DataTable.Row>

    <DataTable.Row>
      <DataTable.Title>
      <MyTxt txt={lng.categoryTxt} />
      </DataTable.Title>
      <DataTable.Cell>
      {row.category && eval('row.category.title'+lng.langCode)}
        </DataTable.Cell>
    </DataTable.Row>

    <DataTable.Row>
      <DataTable.Title>
      <MyTxt txt={lng.shareProductTxt} />
      </DataTable.Title>
      <DataTable.Cell onPress={() => this.onShare(row && eval('row.title'+lng.langCode), row.priceAfterDiscount)}>
      <FontAwesome 
                name="share-alt" 
                
                style={cstmStyle.textDanger}
                />
        </DataTable.Cell>
    </DataTable.Row>

    
    <DataTable.Pagination
      page={1}
      numberOfPages={1}
      onPageChange={page => {
        console.log(page);
      }}
      label=""
    />
  </DataTable>


</Surface>



<Surface style={cstmStyle.surface}>

  <Text style={cstmStyle.prgrfTitle}> 
  <MyTxt txt={lng.descriptionTxt} fntWght='bold'  />

  </Text>

<Text style={{padding: 20, alignSelf: 'flex-start'}}>
<MyTxt txt={row && eval('row.description'+lng.langCode) ? eval('row.description'+lng.langCode).replace(/(&nbsp;|<([^>]+)>)/ig, "") : null} /> 

</Text>

  </Surface>

</>
}


        {/* {row.files && row.files.map((file, i) => {
          return (
            <View key={file.id}>

              <EditThumb imgUrl={file.imgUrl} />
              <TouchableOpacity onPress={() => this.removeImg(file.id)}>
                <FontAwesome name="window-close" size={28} color="#900" />
              </TouchableOpacity>
            </View>
          )
        })
        } */}


</ScrollView>
      </Background>

    )
  }
}

const styles = StyleSheet.create({
  dataTbl: {
    justifyContent: 'center',
    backgroundColor: '#fff',
    padding: 8,
  },
 
  swiperContainer: {
    height: 200,
    marginBottom: 20
  },

});


const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
    defaultSets: state.DfltSts,
    userInfo: state.User.info,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      addItemToCart: (product) => dispatch({ type: 'ADD_TO_CART', payload: product }),
      addPrdctToFav: (prdctId) => dispatch({ type: 'ADD_TO_FAV', prdctId })
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Show);


