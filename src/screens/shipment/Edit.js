import React, { Component } from 'react';
import { View, Text, ScrollView, StyleSheet, TouchableOpacity, Image } from "react-native";
import Background from '../../components/Background';
import Button from '../../components/Button';
import { cstmStyle } from '../../assets/theme';
import TextInput from '../../components/TextInput';
import { connect } from 'react-redux';
import Toast from 'react-native-root-toast';
import putData from '../../helpers/putData';
import { Surface } from 'react-native-paper';

//file upload.
import MyTxt from '../../components/MyTxt';
import HdrTitle from '../../components/HdrTitle';
import getData from '../../helpers/getData';
// end file upload.


class Edit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.route.params.id,
      firstName: '',
      lastName: '',
      receiverMobile: '',
      receiverMobile2: '',
      countryId: 162,      
      countryCode: 'YE',
      cityId: '412810',
      adrs: '',

      errorMessage: '',
      isDisabled: false,
    };

  }


  onChangeValue = (key, val) => {

    this.setState({ [key]: val })
  }

  handleSubmit = () => {

   
    this.setState({ isDisabled: true });

    let ldToast = Toast.show(this.props.selectedLanguage.loadingTxt, {
      duration: 10000, //Toast.durations.LONG,
      position: 0, //Toast.positions.TOP
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      backgroundColor: '#ffc107' //dc3545 //7aa513
    })


    //return console.log(this.state.countryInfo.phoneCode+this.state.phone)
    putData(this.props.defaultSets.apiUrl+'shipmentadrs/'+this.state.id, {
      id: this.state.id,
      userId: this.props.userInfo.id,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      receiverMobile: this.state.receiverMobile,
      receiverMobile2: this.state.receiverMobile2,
      countryId: this.state.countryId,
      countryCode: this.state.countryCode,
      cityId: this.state.cityId,
      adrs: this.state.adrs,
      langCode: this.props.selectedLanguage.langCode
    }, this.props.userInfo.accessToken)
      .then((response) => {
        Toast.hide(ldToast);

        if (response.data.success == true) {

          if(this.props.route.params.screenToBack != undefined && this.props.route.params.screenToBack == 'ViewCart') {
            return this.props.navigation.navigate('ViewCart');
          }
          Toast.show(response.data.msg, {
            duration: 10000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#7aa513' //dc3545 //7aa513
          });

        } else {
          Toast.show(response.data.msg, {
            duration: 10000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#dc3545' //dc3545 //7aa513
          });
        }
      })
      .then((response) => response.json())
      .catch((error) => {
        //console.log(error.response.status, this.props.defaultSets.apiUrl+'register')

        //return console.log(error.response);
        this.setState({ isDisabled: false })

        if (error.response !== undefined && error.response.status === 422) {
          let errorTxt = '';
          let errorMessage = error.response.data.errors;

          Object.keys(errorMessage).map((key, i) => {
            errorTxt = errorTxt + ' ' + errorMessage[key][0] + "\n";
            //errorTxt = errorMessage[key][0];
          });
          Toast.hide(ldToast);

          Toast.show(<MyTxt txt={errorTxt} style={{ lineHeight: 28 }} />, {
            duration: 6000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#dc3545' //dc3545 //7aa513
          });
        }
      });

  }


  componentDidMount() {
    
    this.fetchData(this.state.id); 

    this.props.navigation.setOptions({ headerTitle: <HdrTitle txt={this.props.selectedLanguage.updateShpmntAdrsScreen} /> });

  }

  
fetchData(elmntId) {
  getData(this.props.defaultSets.apiUrl+'shipmentadrs/'+elmntId+'/edit', this.props.userInfo.accessToken)

      .then(response => {

        this.setState({
          firstName: response.data.firstName,
          lastName: response.data.lastName,
          receiverMobile: response.data.receiverMobile,
          receiverMobile2: response.data.receiverMobile2,
          adrs: response.data.adrs,
        });
        //console.log(response.data.files[0].imgUrl)
      })
      .catch(function (error) {
          console.log(error);
      });
}
  render() {


    const { isDisabled, firstName, lastName, receiverMobile, receiverMobile2, adrs} = this.state;
    const lng = this.props.selectedLanguage;

    return (

      <Background>

        <ScrollView style={{ flex: 1, width: '100%' }}>

          <Surface style={cstmStyle.surface}>

            <TextInput
              label={lng.firstNameTxt}
              value={firstName}
              returnKeyType="next"
              onChangeText={(val) => this.onChangeValue('firstName', val)}
            />

            <TextInput
              label={lng.lastNameTxt}
              value={lastName}
              returnKeyType="next"
              onChangeText={(val) => this.onChangeValue('lastName', val)}
            />

            <TextInput
              value={receiverMobile}
              label={lng.receiverMobileTxt}
              returnKeyType="next"
              onChangeText={(val) => this.onChangeValue('receiverMobile', val)}
              maxLength={12}
              autoCapitalize="none"
              autoCompleteType="tel"
              textContentType="telephoneNumber"
              keyboardType="phone-pad"
            />

<TextInput
              label={lng.receiverMobile2Txt}
              value={receiverMobile2}
              returnKeyType="next"
              onChangeText={(val) => this.onChangeValue('receiverMobile2', val)}
              autoCapitalize="none"
              autoCompleteType="tel"
              textContentType="telephoneNumber"
              keyboardType="phone-pad"
            />

<TextInput
              label={lng.adrsTxt}
              value={adrs}
              numberOfLines={6}              
              returnKeyType="done"
              onChangeText={(val) => this.onChangeValue('adrs', val)}
              
            />

          </Surface>

         
          <Surface style={cstmStyle.surface}>

            <Button mode="contained"
              isDisabled={isDisabled}
              onPress={this.handleSubmit}>
              {lng.saveTxt}
            </Button>
          
          </Surface>

         
        </ScrollView>
      </Background>

    )
  }
}

const styles = StyleSheet.create({


  row: {
    alignSelf: 'center',
    flexDirection: 'row',
    marginTop: 14,
    marginBottom: 15
  },

});



const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
    defaultSets: state.DfltSts,
    userInfo: state.User.info,
  }
}
export default connect(mapStateToProps, null)(Edit);
