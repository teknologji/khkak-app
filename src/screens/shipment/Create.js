import React, { Component } from 'react';
import { View, Text, ScrollView, StyleSheet, TouchableOpacity, Image } from "react-native";
import Background from '../../components/Background';
import Button from '../../components/Button';
import { cstmStyle } from '../../assets/theme';
import TextInput from '../../components/TextInput';
import { connect } from 'react-redux';
import Toast from 'react-native-root-toast';
import postData from '../../helpers/postData';
import { Surface } from 'react-native-paper';

//file upload.
import MyTxt from '../../components/MyTxt';
import HdrTitle from '../../components/HdrTitle';
// end file upload.

import CountryPicker from 'react-native-country-picker-modal';

class Create extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      receiverMobile: '966',
      receiverMobile2: '966',
      countryId: 163,      
      countryCode: 'SA',
      country: '',
      cityId: '7',
      adrs: '',
      errorMessage: '',
      isDisabled: false,
      loading: true,
    };

  }


  onChangeValue = (key, val) => {

    this.setState({ [key]: val })
  }
  handleSubmit = () => {

   
    this.setState({ isDisabled: true });

    let ldToast = Toast.show(this.props.selectedLanguage.loadingTxt, {
      duration: 10000, //Toast.durations.LONG,
      position: 0, //Toast.positions.TOP
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      backgroundColor: '#ffc107' //dc3545 //7aa513
    })


    //return console.log(this.state.countryInfo.phoneCode+this.state.phone)
    postData(this.props.defaultSets.apiUrl + 'shipmentadrs', {
      userId: this.props.userInfo.id,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      receiverMobile: this.state.receiverMobile,
      receiverMobile2: this.state.receiverMobile2,
      countryId: this.state.countryId,
      countryCode: this.state.countryCode,
      cityId: this.state.cityId,
      adrs: this.state.adrs,
      langCode: this.props.selectedLanguage.langCode
    }, this.props.userInfo.accessToken)
      .then((response) => {
        Toast.hide(ldToast);

        if (response.data.success == true) {

          if(this.props.route.params.screenToBack != undefined && this.props.route.params.screenToBack == 'ViewCart') {
            return this.props.navigation.navigate('ViewCart');
          }
          Toast.show(response.data.msg, {
            duration: 10000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#7aa513' //dc3545 //7aa513
          });

        } else {
          Toast.show(response.data.msg, {
            duration: 10000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#dc3545' //dc3545 //7aa513
          });
        }
      })
      .then((response) => response.json())
      .catch((error) => {
        //console.log(error.response.status, this.props.defaultSets.apiUrl+'register')

        //return console.log(error.response);
        this.setState({ isDisabled: false })

        if (error.response !== undefined && error.response.status === 422) {
          let errorTxt = '';
          let errorMessage = error.response.data.errors;

          Object.keys(errorMessage).map((key, i) => {
            errorTxt = errorTxt + ' ' + errorMessage[key][0] + "\n";
            //errorTxt = errorMessage[key][0];
          });
          Toast.hide(ldToast);

          Toast.show(<MyTxt txt={errorTxt} style={{ lineHeight: 28 }} />, {
            duration: 6000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#dc3545' //dc3545 //7aa513
          });
        }
      });

  }


  componentDidMount() {
    this.props.navigation.setOptions({ headerTitle: <HdrTitle txt={this.props.selectedLanguage.addShipmentAdrsTxt} /> });

    this.setState({ loading: false });
  }

  onSelect = (country) => {
    //console.log(country)
    this.setState({ 
      countryCode: country.cca2, 
      country: country, 
      receiverMobile: country.callingCode[0],
      receiverMobile2: country.callingCode[0]
    })
  }

  render() {


    const { isDisabled, loading, countryCode, receiverMobile, receiverMobile2} = this.state;
    const lng = this.props.selectedLanguage;

    return (

      <Background>

        <ScrollView style={{ flex: 1, width: '100%' }}>

          <Surface style={cstmStyle.surface}>

          {loading == false ?
            <CountryPicker
              withEmoji={true}
              withCloseButton={true}
              withCallingCode={true}
              withAlphaFilter={false}
              withFilter={true}
              withFlag={true}
              countryCode={countryCode}
              onSelect={this.onSelect}
              visible={false}
            /> : null}

            <TextInput
              label={lng.firstNameTxt}
              returnKeyType="next"
              onChangeText={(val) => this.onChangeValue('firstName', val)}
            />

            <TextInput
              label={lng.lastNameTxt}
              returnKeyType="next"
              onChangeText={(val) => this.onChangeValue('lastName', val)}
            />

            <TextInput
            value={receiverMobile}
              label={lng.receiverMobileTxt}
              returnKeyType="next"
              onChangeText={(val) => this.onChangeValue('receiverMobile', val)}
              maxLength={12}
              autoCapitalize="none"
              autoCompleteType="tel"
              textContentType="telephoneNumber"
              keyboardType="phone-pad"
            />

<TextInput
            value={receiverMobile2}
              label={lng.receiverMobile2Txt}
              returnKeyType="next"
              onChangeText={(val) => this.onChangeValue('receiverMobile2', val)}
              autoCapitalize="none"
              autoCompleteType="tel"
              textContentType="telephoneNumber"
              keyboardType="phone-pad"
            />

<TextInput
              label={lng.adrsTxt}
              placeHolder={lng.writeFullAdrsTxt}
              numberOfLines={6}              
              returnKeyType="done"
              onChangeText={(val) => this.onChangeValue('adrs', val)}
              
            />

          </Surface>

         
          <Surface style={cstmStyle.surface}>

            <Button mode="contained"
              isDisabled={isDisabled}
              onPress={this.handleSubmit}>
              {lng.saveTxt}
            </Button>
          
          </Surface>

         
        </ScrollView>
      </Background>

    )
  }
}

const styles = StyleSheet.create({


  row: {
    alignSelf: 'center',
    flexDirection: 'row',
    marginTop: 14,
    marginBottom: 15
  },

});



const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
    defaultSets: state.DfltSts,
    userInfo: state.User.info,
  }
}
export default connect(mapStateToProps, null)(Create);
