import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,

} from "react-native";
import MyTxt from '../../components/MyTxt';
import { connect } from 'react-redux'
import Background from "../../components/Background";
import { Surface } from 'react-native-paper';
import { cstmStyle } from "../../assets/theme";
import Button from "../../components/Button";
import ShipmentAdrsBox from "../../components/ShipmentAdrsBox";
import getData from '../../helpers/getData';
import deleteData from '../../helpers/deleteData';
import HdrTitle from "../../components/HdrTitle";

import AwesomeAlert from 'react-native-awesome-alerts';

class Index extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      rows: [],
      showAlert: false,
    };


  }



  componentDidMount() {
    this.fetchUsrShpmntAdrs();

    this.focusListener = this.props.navigation.addListener('focus', () => {
      this.fetchUsrShpmntAdrs();
    });
    this.props.navigation.setOptions({ headerTitle: <HdrTitle txt={this.props.selectedLanguage.shipmentAdrsesTxt} /> });

    /* this.focusListener = this.props.navigation.addListener('didFocus', async () => {

      this.setState({isReady: true})


  }); */

  }

  fetchUsrShpmntAdrs() {
    //
    getData(this.props.defaultSets.apiUrl + 'user-shpmnt-adrs/' + this.props.userInfo.id, this.props.userInfo.accessToken)

      .then((response) => {
        this.setState({ rows: response.data.rows });
        //console.log(response.data.rows)
      })
      .catch(err => {
        console.log(err);
      });
  }


  deleteRow() {
    let id = this.state.rowToBeDlt;
    console.log('dlt rowww', id)

    deleteData(this.props.defaultSets.apiUrl + 'shipmentadrs/' + id, this.props.userInfo.accessToken)
      .then(res => {
        const filteredData = this.state.rows.filter(item => item.id !== id);
        this.setState({ rows: filteredData });

      })
      .catch(err => {
        console.log(err);
      });

  }
  showAlert = (id) => {
    this.setState({
      rowToBeDlt: id,
      showAlert: true
    });
  };

  hideAlert = () => {
    this.setState({
      showAlert: false
    });
  };

  render() {

    let lng = this.props.selectedLanguage;
    let cartItems = this.props.cartItems;
    const { rows, showAlert, shipmentAdrsId } = this.state;
    return (

      <Background>

        <ScrollView style={{ flex: 1, width: '100%' }}>

          <Surface style={[cstmStyle.surface, { marginTop: 20, flexDirection: 'row', justifyContent: 'space-between' }]}>
            <MyTxt fntWght='bold' txt={lng.shipmentAdrsesTxt} />

            <Button mode="contained" onPress={() => this.props.navigation.navigate('CreateShipmentAdrs')}>
              <MyTxt txt={lng.addShipmentAdrsTxt} />
            </Button>

          </Surface>

          {rows.length > 0 ?
            rows.map((item, i) => {
              return (
                <ShipmentAdrsBox
                  showAlert={() => this.showAlert(item.id)}
                  shipmentAdrsId={shipmentAdrsId}
                  key={item.id}
                  item={item}
                  getShippingCharges={this.getShippingCharges}
                  navigation={this.props.navigation}
                />
              )
            })

            : null
          }



          <AwesomeAlert
            show={showAlert}
            showProgress={false}
            title={lng.confirmMsgTxt}
            message={lng.rUSureTxt}
            closeOnTouchOutside={true}
            closeOnHardwareBackPress={false}
            showCancelButton={true}
            showConfirmButton={true}
            cancelText={lng.noTxt}
            confirmText={lng.yesTxt}
            confirmButtonColor="#DD6B55"
            onCancelPressed={() => {
              this.hideAlert();
            }}
            onConfirmPressed={() => {

              this.deleteRow();
              this.hideAlert();
            }}
          />

        </ScrollView>
      </Background>

    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
    defaultSets: state.DfltSts,
    userInfo: state.User.info

  }
}




export default connect(mapStateToProps, null)(Index);

