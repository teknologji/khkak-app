import React from "react";
//import { View, Button, Text, StyleSheet } from "react-native";
import { SafeAreaView, View, FlatList, StyleSheet, Text, ScrollView } from 'react-native';

import Background from '../components/Background';
import Logo from '../components/Logo';
import getData from '../helpers/getData';

import { connect } from 'react-redux';
import { Surface } from 'react-native-paper';
import HdrTitle from "../components/HdrTitle";
import ProductBox from "../components/ProductBox";
import { cstmStyle } from "../assets/theme";
import MyTxt from "../components/MyTxt";
import CatBox from "../components/CatBox";

class HomeScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      lastAdded: [],
      loading: false
    }
  }  

  //return console.log(selectedLanguage)

  componentDidMount(){
    // Update the document title using the browser API
    this.props.navigation.setOptions({ headerTitle: <HdrTitle txt={this.props.selectedLanguage.appNmTxt} /> });
    this.fetchLastAdded();
    this.fetchCategories();
  }
  
  fetchLastAdded(){
  getData(this.props.defaultSets.apiUrl+'last-products')
  //.then(response => response.json())
  .then(response => {
      this.setState({ lastAdded: response.data, loading: false });
      //console.log(response.data)
  })
  .catch(function (error) {
      console.log(error);
  });
 }

 fetchCategories() {

  getData(this.props.defaultSets.apiUrl + 'categories/status/1')

      .then(response => {
        this.setState({ cats: response.data.rows, loading: false });
        //console.log(response.data.rows)
      })
      .catch(function (error) {
          console.log(error);
      });

}

  render() {
 
    
    const {lastAdded, loading, cats} = this.state;
    let lng = this.props.selectedLanguage;

  return (
    <Background>
      
    <ScrollView style={{ flex: 1, width: '100%' }}>


        
    <Surface style={[cstmStyle.surface, {height: 50, padding: 15}]}>
      <MyTxt fntWght='bold' txt={lng.lastAddedTxt} />
    </Surface>

      <FlatList 
      refreshing={loading}
        data={lastAdded}
        renderItem={ ({ item }) => <ProductBox item={item} navigation={this.props.navigation} />}
        keyExtractor={item => item.id.toString()}
        numColumns={2}
onRefresh={() => this.fetchLastAdded()}
      />


<Surface style={[cstmStyle.surface, {height: 50, padding: 15}]}>
      <MyTxt fntWght='bold' txt={lng.depsTxt} />
    </Surface>

    <FlatList 
      /* onRefresh={() => this.fetchCategories()} */
      refreshing={loading}
        data={cats}
        renderItem={ ({ item }) => <CatBox item={item} navigation={this.props.navigation} />}
        keyExtractor={item => item.id.toString()}
        numColumns={2}

      />

    </ScrollView>
  </Background>
  );
  }
};

const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
    defaultSets: state.DfltSts,
    userInfo: state.User.info,
  }
}
export default connect(mapStateToProps, null)(HomeScreen);