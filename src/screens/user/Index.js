import React, { Component } from 'react';
import { View, RefreshControl, ScrollView, TouchableOpacity, Image } from "react-native";
import Background from '../../components/Background';
import Button from '../../components/Button';
import Header from '../../components/Header';
import { connect } from 'react-redux';
import getData from '../../helpers/getData';
import deleteData from '../../helpers/deleteData';
import { cstmStyle } from '../../assets/theme';
import { DataTable } from 'react-native-paper';
import DropDownPicker from 'react-native-dropdown-picker';

import { FontAwesome5 } from '@expo/vector-icons';

import AwesomeAlert from 'react-native-awesome-alerts';
import HdrTitle from '../../components/HdrTitle';
import MyTxt from '../../components/MyTxt';
import TextInput from '../../components/TextInput';
import { Surface } from 'react-native-paper';

// end file upload.
//import Moment from 'moment';


class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rows: '',
      showAlert: false,
      rowToBeDlt: 0,
      loading: false,
      arrayholder: [],
      full_name: '',
      phone: '',
      minAge: '',
      maxAge: ''
    };
  }


  componentDidMount() {

    this.fetchRows();

    this.focusListener = this.props.navigation.addListener('focus', () => {
      this.fetchRows();
    });
  }


  fetchRows() {
    getData(this.props.defaultSets.apiUrl + 'users-by-group/2', this.props.userInfo.accessToken)
      //.then(response => response.json())
      .then(response => {
        this.setState({ arrayholder: response.data.rows, rows: response.data.rows, loading: false });
        //console.log(response.data.rows)
      })
      .catch(function (error) {
        console.log(error);
      })
    this.props.navigation.setOptions({ headerTitle: <HdrTitle txt={this.props.selectedLanguage.stdntsMngTxt} /> });

  }


  deleteRow() {

    let id = this.state.rowToBeDlt;

    deleteData(this.props.defaultSets.apiUrl + 'users/' + id, this.props.userInfo.accessToken)
      .then(res => {
        const filteredData = this.state.rows.filter(item => item.id !== id);
        this.setState({ rows: filteredData });

      })
      .catch(err => {
        console.log(err);
      });

  }

  showAlert = (rowToBeDlt) => {
    this.setState({
      showAlert: true, rowToBeDlt
    });
  };

  hideAlert = () => {
    this.setState({
      showAlert: false
    });
  };


  onChangeSearch(column, query) {
    //passing the inserted text in textinput
    //return console.log(column, query)

    let newRows = this.state.arrayholder;

    if (this.state.phone != '') {
      newRows = newRows.filter(row => row.phone.search(this.state.phone) !== -1);
    }

    if (this.state.full_name != '') {
      newRows = newRows.filter(row => row.full_name.search(this.state.full_name) !== -1);
    }

    if (this.state.minAge != '') {

      newRows = newRows.filter(row => {

        return (this.calculate_age(row.birthDate) >= this.state.minAge);
      });
    }

    if (this.state.maxAge != '') {

      newRows = newRows.filter(row => {
        return (this.calculate_age(row.birthDate) <= this.state.maxAge);
      });
    }

    this.setState({ rows: newRows });
  }

  calculate_age = (dob1) => {
    var today = new Date();
    var birthDate = new Date(dob1);  // create a date object directly from `dob1` argument
    var age_now = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age_now--;
    }
    //console.log(age_now);
    return age_now;
  }

  onChangeValue = (key, val) => {
    this.setState({ [key]: val }, () => {

      this.onChangeSearch(key, val)
    })
  }

  render() {

    const lng = this.props.selectedLanguage;
    const { rows, showAlert, loading, } = this.state;

    return (

      <Background>

        <ScrollView style={{ flex: 1, width: '100%' }}
          refreshControl={
            <RefreshControl refreshing={loading} onRefresh={() => this.fetchRows()} />
          }
        >
          <Button mode="contained" style={cstmStyle.bgOrange}
            onPress={() => this.props.navigation.navigate('Register')}
            icon="plus"
            compact={false}
            contentStyle={{ height: 30 }}
          >
            <MyTxt txt={lng.crtNewTxt} />
          </Button>



          <Surface style={cstmStyle.surface}>

            <View style={cstmStyle.fltrRow}>
              <TextInput
              style={{marginHorizontal: 2}}
                label={lng.srchByNmTxt}
                returnKeyType="next"
                onChangeText={(val) => this.onChangeValue('full_name', val)}
              />

              <TextInput
              style={{marginHorizontal: 2}}
                label={lng.srchByPhnTxt}
                returnKeyType="next"
                onChangeText={(val) => this.onChangeValue('phone', val)}
              />

            </View>

            <View style={cstmStyle.fltrRow}>
            <TextInput
              style={{marginHorizontal: 2}}
              label={lng.minAgeTxt}
              returnKeyType="next"
              onChangeText={(val) => this.onChangeValue('minAge', val)}
            />
            <TextInput
              style={{marginHorizontal: 2}}
              label={lng.maxAgeTxt}
              returnKeyType="next"
              onChangeText={(val) => this.onChangeValue('maxAge', val)}
            />
            </View>

            {/* <Searchbar
      placeholder={lng.srchByNmTxt}
      onChangeText={(val) => this.onChangeSearch(val)}
      style={{marginTop: 20}}
    /> */}


          </Surface>


          <DataTable style={cstmStyle.dataTbl}>
            <DataTable.Header>
              <DataTable.Title><MyTxt txt={lng.nameTxt} /></DataTable.Title>
              <DataTable.Title><MyTxt txt={lng.phoneTxt} /></DataTable.Title>
              <DataTable.Title>{/* <MyTxt txt={lng.actionTxt} /> */}</DataTable.Title>
            </DataTable.Header>


            {rows ?

              rows.map((item, index) => {

                return (

                  <DataTable.Row key={item.id}>
                    <DataTable.Cell><MyTxt txt={item.full_name} /></DataTable.Cell>
                    <DataTable.Cell>{item.phone}</DataTable.Cell>

                    <TouchableOpacity
                      style={{ width: 40, paddingTop: 15 }}
                      onPress={() => this.props.navigation.navigate('EditUser', { id: item.id })}>
                      <FontAwesome5 name="pencil-alt" size={20} style={cstmStyle.textGrey} />
                    </TouchableOpacity>

                    <TouchableOpacity
                      style={{ width: 40, paddingTop: 15 }}
                      onPress={() => this.showAlert(item.id)}>
                      <FontAwesome5 name="trash" size={20} style={cstmStyle.textDanger} />
                    </TouchableOpacity>


                  </DataTable.Row>

                )
              }) :
              <DataTable.Row>
                <DataTable.Cell>{lng.loadingTxt}</DataTable.Cell>
              </DataTable.Row>
            }
            <DataTable.Pagination
              page={1}
              numberOfPages={3}
              onPageChange={page => {
                console.log(page);
              }}
              label={lng.rowsCountTxt + ':' + rows && rows.length}
            />
          </DataTable>

          <AwesomeAlert
            show={showAlert}
            showProgress={false}
            title={lng.confirmMsgTxt}
            message={lng.rUSureTxt}
            closeOnTouchOutside={true}
            closeOnHardwareBackPress={false}
            showCancelButton={true}
            showConfirmButton={true}
            cancelText={lng.noTxt}
            confirmText={lng.yesTxt}
            confirmButtonColor="#DD6B55"
            onCancelPressed={() => {
              this.hideAlert();
            }}
            onConfirmPressed={() => {
              this.deleteRow();
              this.hideAlert();
            }}
          />
        </ScrollView>
      </Background>

    )
  }
}



const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
    defaultSets: state.DfltSts,
    userInfo: state.User.info,
  }
}
export default connect(mapStateToProps, null)(Index);
