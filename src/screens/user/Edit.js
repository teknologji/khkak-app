import React, { Component } from 'react';
import { View, ScrollView, RefreshControl, StyleSheet, TouchableOpacity, Image } from "react-native";
import Background from '../../components/Background';
import Button from '../../components/Button';
import { cstmStyle, theme } from '../../assets/theme';
import TextInput from '../../components/TextInput';
import getData from '../../helpers/getData';
import { connect } from 'react-redux';
import Toast from 'react-native-root-toast';
import { Surface, Text, Switch } from 'react-native-paper';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import DateTimePicker from '@react-native-community/datetimepicker';


//file upload.

import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import MyTxt from '../../components/MyTxt';
import putData from '../../helpers/putData';
import EditThumb from '../../components/EditThumb';
// end file upload.
import Moment from 'moment';


class Edit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      row: [],
      firstName: '',
      lastName: '',
      tcNr: '',
      phone: '',
      password: '',
      orphan: 0,

      nationality: 'SY',
      countryCode: 'TR',
      city: 'Istanbul',
      municipality: 'Esenyurt',
      neighborhood: 'Suleymaniye',
      street: '',
      building: '',
      apartment: '',
      birthDate: Moment(new Date()).format("YYYY-MM-DD"),
      groupId: 2, // 2:student.
      errorMessage: '',
      isDisabled: false,
      fromMobApp: 1,
      showDatePicker: false,
      aFile: null,
      loading: false
    };

  }

  /* onChangeValue(e) {
      //console.log(e.target.value)
      this.setState({
          [e.target.name]: e.target.value, isDisabled: false
      });
  } */


  handleChoosePhoto = () => {
    const options = {
      noData: true,
    }
    ImagePicker.launchImageLibrary(options, response => {
      if (response.uri) {
        this.setState({ photo: response })
      }
    })
  }

  getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
  }



  _takePhoto = async () => {
    let result = await ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      //aspect: [4, 3],
      quality: 1
    });



    if (result.uri) {
      this.setState({ aFile: result });
    }
  };

  _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      //aspect: [4, 3],
      quality: 1
    });



    if (result.uri) {
      this.setState({ aFile: result });
    }
  };



  async upldUsrFile(rowId) {

    const data = new FormData();

    data.append('rowId', rowId);
    data.append('type', 'user');

    let attachedFile = this.state.aFile;
    if (attachedFile != null) {

      let filePath = attachedFile.uri;
      let fileExtention = filePath.substring(filePath.lastIndexOf(".") + 1, filePath.length);
      data.append('fileExtention', fileExtention);

      data.append('files[]', { // o Halid, these 3 paramters are neccessary for the object to be sent 
        uri: filePath,
        type: "image/jpeg", // it's very important to android
        name: 'khaled'
      }
      );


    }

    fetch(this.props.defaultSets.apiUrl + "upload-files", { // give something like https://xx.yy.zz/upload/whatever
      method: 'POST',
      body: data,
    }
    )
      .then(response => response.json())
      .then(response => {
        //console.log(response)        
      }
      ).catch((error) => {
        error => console.log('uploadImage error:', error.response.data)
      });

  }



  onChangeValue = (key, val) => {
    this.setState({ [key]: val })
  }
  handleSubmit = () => {

    if (this.state.aFile == null) { // check user files.

      /* return Toast.show(this.props.selectedLanguage.plsUpldUsfFlTxt, {
        duration: 10000, //Toast.durations.LONG,
        position: 0, //Toast.positions.TOP
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 0,
        backgroundColor: '#dc3545' //dc3545 //7aa513
      }) */
    }

    this.setState({ isDisabled: true });

    let ldToast = Toast.show(this.props.selectedLanguage.loadingTxt, {
      duration: 10000, //Toast.durations.LONG,
      position: 0, //Toast.positions.TOP
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      backgroundColor: '#ffc107' //dc3545 //7aa513
    })


    //return console.log(this.state.countryInfo.phoneCode+this.state.phone)
    putData(this.props.defaultSets.apiUrl + 'users/' + this.props.route.params.id, {
      id: this.props.route.params.id,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      tcNr: this.state.tcNr,
      phone: this.state.phone,
      birthDate: this.state.birthDate,
      orphan: this.state.orphan == true ? 1 : 0,
      password: this.state.password,
      nationality: this.state.nationality,
      countryCode: this.state.countryCode,
      city: this.state.city,
      municipality: this.state.municipality,
      neighborhood: this.state.neighborhood,
      street: this.state.street,
      building: this.state.building,
      apartment: this.state.apartment,
      langCode: this.props.selectedLanguage.langCode
    }, this.props.userInfo.accessToken)
      .then((response) => {
        Toast.hide(ldToast);

        if (response.data.success == true) {

          // get added user.
          let theUser = response.data.user;

          //upload user files.
          this.upldUsrFile(theUser.id);
          this.setState({ aFile: null });

          // save user in redux.
          //theUser.accessToken = response.data.accessToken;
          //this.props.saveUserInfo(theUser);


          Toast.show(response.data.msg, {
            duration: 3000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#7aa513' //dc3545 //7aa513
          });

          /* setTimeout( () => {
              this.props.navigation.navigate('Home');
          }, 3000); */

        } else {
          Toast.show(response.data.msg, {
            duration: 10000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#ffc107' //dc3545 //7aa513
          });
        }
      })
      .then((response) => response.json())
      .catch((error) => {
        //console.log(error.response.status, this.props.defaultSets.apiUrl+'register')

        //console.log(error.response.data);
        this.setState({ isDisabled: false })
        let errorTxt = '';

        if (error.response !== undefined && error.response.status === 422) {
          let errorMessage = error.response.data.errors;

          Object.keys(errorMessage).map((key, i) => {
            errorTxt = errorTxt + ' ' + errorMessage[key][0] + "\n";
            //errorTxt = errorMessage[key][0];
          });
          Toast.hide(ldToast);

        } else if (error.response !== undefined && error.response.status === 401) {
          errorTxt = this.props.selectedLanguage.plsLgnFrst; //error.response.data.message;
        }

        if (errorTxt != '') {


          Toast.show(<MyTxt txt={errorTxt} style={{ lineHeight: 28 }} />, {
            duration: 6000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#dc3545' //dc3545 //7aa513
          });
        }

      });

  }


  componentDidMount() {
    this.props.navigation.setOptions({ headerTitle: this.props.selectedLanguage.profileTxt });

    this.fetchRows();
    //console.log(this.props.route.params.id)

  }


  fetchRows() {
    getData(this.props.defaultSets.apiUrl + 'users/' + this.props.route.params.id + '/edit', this.props.userInfo.accessToken)

      .then(response => {

        this.setState({
          row: response.data,
          firstName: response.data.firstName,
          lastName: response.data.lastName,
          tcNr: response.data.tcNr,
          phone: response.data.phone,
          birthDate: response.data.birthDate,
          orphan: response.data.orphan == 1 ? true : false,
          city: response.data.city,
          nationality: response.data.nationality,
          municipality: response.data.municipality,
          neighborhood: response.data.neighborhood,
          street: response.data.street,
          building: response.data.building,
          apartment: response.data.apartment,
          password: response.data.password,
          loading: false,
        });
        console.log(response.data.orphan)
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  removeImg(id) {
    getData(this.props.defaultSets.apiUrl + 'remove-file/' + id + '/user', this.props.userInfo.accessToken)
      .then(res => {
        let currentRow = this.state.row;
        const filteredData = currentRow.files.filter(item => item.id !== id);
        currentRow.files = filteredData;
        this.setState({ row: currentRow });

      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {


    const { isDisabled, aFile, row,
      firstName,
      lastName,
      tcNr,
      phone,
      birthDate,
      showDatePicker,
      orphan,
      city,
      nationality,
      municipality,
      neighborhood,
      street,
      building,
      apartment,
      loading
    } = this.state;
    const lng = this.props.selectedLanguage;

    return (

      <Background>

        <ScrollView style={{ flex: 1, width: '100%' }}
          refreshControl={
            <RefreshControl refreshing={loading} onRefresh={() => this.fetchRows()} />
          }
        >

          <View style={cstmStyle.upldAFile}>
              <FontAwesome name='cloud-upload' style={cstmStyle.textOrange} size={28} />
            <Button onPress={this._pickImage}>
              {lng.prssHr2UpldFlsTxt}
            </Button>
          </View>

          <View style={cstmStyle.upldAFile}>

          <FontAwesome name='camera' style={cstmStyle.textOrange} size={28} />
            <Button onPress={this._takePhoto}>
              {lng.prssHr2CmraTxt}
            </Button>

          </View>
          {aFile &&
            <View style={[cstmStyle.upldAFile, { height: 110 }]}>

              <Image source={{ uri: aFile.uri }} style={{
                width: '90%',
                height: 100,
                resizeMode: 'contain'
              }} />
            </View>}



          <Surface style={styles.surface}>

            <TextInput
              label={lng.firstNameTxt}
              value={firstName}
              returnKeyType="next"
              onChangeText={(val) => this.onChangeValue('firstName', val)}
            />

            <TextInput
              label={lng.lastNameTxt}
              value={lastName}
              returnKeyType="next"
              onChangeText={(val) => this.onChangeValue('lastName', val)}
            />

            <TextInput
              label={lng.tcNrTxt}
              value={tcNr}
              returnKeyType="next"
              onChangeText={(val) => this.onChangeValue('tcNr', val)}
              /* error={true} */
              /* errorText={'kk'} */
              maxLength={12}
              autoCapitalize="none"
              autoCompleteType="tel"
              textContentType="telephoneNumber"
              keyboardType="phone-pad"
            />

            <TextInput
              label={lng.phone}
              value={phone}
              returnKeyType="next"
              onChangeText={(val) => this.onChangeValue('phone', val)}
              maxLength={12}
              autoCapitalize="none"
              autoCompleteType="tel"
              textContentType="telephoneNumber"
              keyboardType="phone-pad"
            />

{birthDate &&
            <TextInput
              value={birthDate}
              label={lng.birthDateTxt}
              returnKeyType="next"
              onFocus={() => this.setState({ showDatePicker: true })}
            />}

          {showDatePicker &&
            <DateTimePicker

              testID="dateTimePicker"
              value={new Date(1598051730000)}
              mode={'date'}
              is24Hour={true}
              display="default"
              onChange={(event, selectedDate) => {
                this.setState({
                  birthDate: Moment(selectedDate).format("YYYY-MM-DD"),
                  showDatePicker: false
                });d
              }}
            />}

          </Surface>



          <Surface style={styles.surface}>

            <View style={styles.radiosBox}>
              <MyTxt txt={lng.orphanTxt} />
              <Switch
                color='#D81500'
                value={orphan}
                onValueChange={(val) => this.onChangeValue('orphan', val)}
              />


            </View>

          </Surface>

          <Surface style={styles.surface}>

            <TextInput
              label={lng.cityTxt}
              value={city}
              returnKeyType="next"
              onChangeText={(val) => this.onChangeValue('city', val)}
            />
            <TextInput
              label={lng.nationalityTxt}
              value={nationality}
              returnKeyType="next"
              onChangeText={(val) => this.onChangeValue('nationality', val)}
            />
            <TextInput
              label={lng.municipalityTxt}
              value={municipality}
              returnKeyType="next"
              onChangeText={(val) => this.onChangeValue('municipality', val)}
            />


            <TextInput
              label={lng.neighborhoodTxt}
              value={neighborhood}
              returnKeyType="next"
              onChangeText={(val) => this.onChangeValue('neighborhood', val)}
            />

            <TextInput
              label={lng.streetTxt}
              value={street}
              returnKeyType="next"
              onChangeText={(val) => this.onChangeValue('street', val)}
            />

            <TextInput
              label={lng.buildingTxt}
              value={building}
              returnKeyType="next"
              onChangeText={(val) => this.onChangeValue('building', val)}
            />

            <TextInput
              label={lng.apartmentTxt}
              value={apartment}
              returnKeyType="next"
              onChangeText={(val) => this.onChangeValue('apartment', val)}
              keyboardType="phone-pad"
            />

            <TextInput
              label={lng.passwordTxt}
              returnKeyType="done"
              onChangeText={(val) => this.onChangeValue('password', val)}
              /* error={false}
              errorText={'pass err'} */
              secureTextEntry
              autoCompleteType="tel"    
              textContentType="telephoneNumber"
              keyboardType="phone-pad"
            />
            
          </Surface>

          <Surface style={styles.surface}>

            <View style={[cstmStyle.upldAFile, { height: 120, marginTop: 20, padding: 10 }]}>

              {row.files && row.files.map((file, i) => {
                return (
                  <View key={file.id} style={{ flex: 1, justifyContent: 'space-between', flexDirection: 'row' }}>

                    <View style={{ flex: 0.5 }}>
                      <EditThumb imgUrl={file.imgUrl} />
                    </View>


                    <View style={{ flex: 0.5 }}>
                      <TouchableOpacity onPress={() => this.removeImg(file.id)}>
                        <FontAwesome name="window-close" size={28} color="#900" />
                      </TouchableOpacity>
                    </View>
                  </View>
                )
              })
              }

            </View>




            <Button mode="contained"
              isDisabled={isDisabled}
              onPress={this.handleSubmit}>
              {lng.saveTxt}
            </Button>
          </Surface>

        </ScrollView>
      </Background>

    )
  }
}


const styles = StyleSheet.create({
  surface: {
    padding: 8,
    margin: 15,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 5,
  },
  row: {
    flexDirection: 'row',
    marginTop: 4,
    marginBottom: 15
  },
  radiosBox: { flexDirection: 'row', justifyContent: 'space-between', margin: 20 },

});


const mapDispatchToProps = (dispatch) => {
  return {
    saveUserInfo: (userInfo) => dispatch({ type: 'SAVE_USER', payload: userInfo }),

  }
};

const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
    defaultSets: state.DfltSts,
    userInfo: state.User.info,
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Edit);
