import React, { Component } from 'react';
import { View, ScrollView } from "react-native";
import Background from '../../components/Background';
import Button from '../../components/Button';
import TextInput from '../../components/TextInput';
import Toast from 'react-native-root-toast';
import postData from '../../helpers/postData';
import { connect } from 'react-redux';
import { Surface } from 'react-native-paper';

import FontAwesome from 'react-native-vector-icons/FontAwesome';


//file upload.

import MyTxt from '../../components/MyTxt';
import HdrTitle from '../../components/HdrTitle';
import { cstmStyle } from '../../assets/theme';
// end file upload.
//import Moment from 'moment';


class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      phone: '',
      msg: '',
      fromMobApp: 1,
    };

  }


  onChangeValue = (key, val) => {

    this.setState({ [key]: val, isDisabled: true });

  }

  handleSubmit = () => {

  
    this.setState({ isDisabled: true });

    let ldToast = Toast.show(this.props.selectedLanguage.loadingTxt, {
      duration: 10000, //Toast.durations.LONG,
      position: 0, //Toast.positions.TOP
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      backgroundColor: '#212529' //dc3545 //7aa513
    })


    //return console.log(this.state.countryInfo.phoneCode+this.state.phone)
    postData(this.props.defaultSets.apiUrl + 'contact', {

      name: this.state.name,
      phone: this.state.phone,
      msg: this.state.msg,
      langCode: this.props.selectedLanguage.langCode
    })
      .then((response) => {
        Toast.hide(ldToast);

        if (response.data.success == true) {

          Toast.show(response.data.msg, {
            duration: 3000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#7aa513' //dc3545 //7aa513
          });

        } else {
          Toast.show(response.data.msg, {
            duration: 10000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#dc3545' //dc3545 //7aa513
          });
        }
      })
      .then((response) => response.json())
      .catch((error) => {

        this.setState({ isDisabled: false })

        if (error.response !== undefined && error.response.status === 422) {
          let errorTxt = '';
          let errorMessage = error.response.data.errors;

          Object.keys(errorMessage).map((key, i) => {
            errorTxt = errorTxt + ' ' + errorMessage[key][0] + "\n";
            //errorTxt = errorMessage[key][0];
          });
          Toast.hide(ldToast);

          Toast.show(<MyTxt txt={errorTxt} style={{ lineHeight: 28 }} />, {
            duration: 6000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#dc3545' //dc3545 //7aa513
          });
        }
      });

  }


  componentDidMount() {
    this.props.navigation.setOptions({ headerTitle: <HdrTitle txt={this.props.selectedLanguage.contactTxt} /> });

  }
  render() {


    const { isDisabled } = this.state;
    let lng = this.props.selectedLanguage;

    return (

      <Background>


<ScrollView style={{ flex: 1, width: '100%' }}>
        <Surface style={cstmStyle.surface}>

        <TextInput
          label={lng.nameTxt}
          returnKeyType="next"
          onChangeText={(val) => this.onChangeValue('name', val)}
        />

       
        <TextInput
          label={lng.phone}
          returnKeyType="next"
          onChangeText={(val) => this.onChangeValue('phone', val)}
          maxLength={12}
          autoCapitalize="none"
          autoCompleteType="tel"
          textContentType="telephoneNumber"
          keyboardType="phone-pad"
        />

<TextInput
          multiline={true}
          numberOfLines={6}
          label={lng.msgTxt}
          onChangeText={(val) => this.onChangeValue('msg', val)}
        />

        <Button mode="contained"
          isDisabled={isDisabled}
          onPress={this.handleSubmit}>
          {lng.sendTxt}
        </Button>
 </Surface>
        </ScrollView>
      </Background>

    )
  }
}


const mapStateToProps = (state) => {
  return {

    userInfo: state.User.info,
    selectedLanguage: state.LanguageReducer.slctdLng,
    defaultSets: state.DfltSts,
  }
}

export default connect(mapStateToProps, null)(Contact);