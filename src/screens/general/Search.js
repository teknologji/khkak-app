import React, { Component, Fragment } from 'react';
import { View, SafeAreaView, Text, FlatList } from "react-native";
import Background from '../../components/Background';
import { connect } from 'react-redux';
import getData from '../../helpers/getData';
import TextInput from '../../components/TextInput';
import HdrTitle from '../../components/HdrTitle';
import ProductBox from '../../components/ProductBox';
import { cstmStyle } from '../../assets/theme';
import MyTxt from '../../components/MyTxt';
import { Surface } from 'react-native-paper';
import Toast from 'react-native-root-toast';
// end file upload.
//import Moment from 'moment';


class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      rows: '',
      loading: true,
      arrayholder: [],
    };
  }


  componentDidMount() {

 

  }


  fetchRows(search) {

    let ldToast = Toast.show(this.props.selectedLanguage.loadingTxt, {
      duration: 10000, //Toast.durations.LONG,
      position: 0, //Toast.positions.TOP
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      backgroundColor: '#ffc107' //dc3545 //7aa513
    })

    //let search = this.state.search;
    console.log(search)
    getData(this.props.defaultSets.apiUrl + 'search-products/' + search)
      .then(response => {
        //let allPrdctsChsn = categoryId == 'all' ? true: false;
        //alert(allPrdctsChsn)
        Toast.hide(ldToast);

        this.setState({
          arrayholder: response.data.rows,
          rows: response.data.rows,
          loading: false,
          //allPrdctsChsn
        });
        //console.log(response.data)
      })
      .catch(function (error) {
        console.log(error);
      });
    this.props.navigation.setOptions({ headerTitle: <HdrTitle txt={this.props.selectedLanguage.searchTxt} /> });

  }

 
  render() {

    const lng = this.props.selectedLanguage;
    const { loading, rows, pgTitle } = this.state;

    return (

      <Background>
        <SafeAreaView style={{flex: 1, width: '100%'}}>


          <TextInput
          label={lng.searchTxt}
          returnKeyType="next"
          onChangeText={(val) => this.fetchRows(val)}
        />
      
            {rows ?
              <Surface style={[cstmStyle.surface, { padding: 15}]}>
                <MyTxt style={{ fontSize: 12 }} txt={lng.productsCountTxt + ':' + rows.length} />
              </Surface> : null}

            {rows ?
              <FlatList
                refreshing={loading}
                onRefresh={() => this.fetchRows()}
                data={rows}
                renderItem={({ item }) => <ProductBox item={item}
                  navigation={this.props.navigation} />}
                keyExtractor={item => item.id.toString()}
                numColumns={2}
              /> : null}

        </SafeAreaView>
      </Background>

    )
  }
}



const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
    defaultSets: state.DfltSts,
    userInfo: state.User.info,
  }
}
export default connect(mapStateToProps, null)(Search);
