import React from "react";
import { View, StyleSheet, RefreshControl, ScrollView, SafeAreaView } from "react-native";
import HdrTitle from "../../components/HdrTitle";
import { connect } from 'react-redux';
import getData from '../../helpers/getData';
import Accordion from 'react-native-collapsible/Accordion';
import MyTxt from "../../components/MyTxt";
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { cstmStyle } from "../../assets/theme";
import BackgroundScroll from "../../components/BackgroundScroll";
import Background from "../../components/Background";

/* const SECTIONS = [
  {
    kkk: 'First',
    content: 'Lorem ipsum...',
  },
  {
    kkk: 'Second',
    content: 'Lorem ipsum...',
  },
]; */


class Faq extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      activeSections: [],
      loading: false,
      rows: []
    }
  }

/*   _renderSectionTitle = section => {
    return (
      <View style={styles.title}>
        <Text>{section.content}</Text>
      </View>
    );
  }; */
  _renderHeader = item => {
    /* style={{color: 'black', fontSize: 18}} */
    return (
      <View style={styles.title}>
        <MyTxt 
        style={{lineHeight: 26, fontSize: 16}}
        txt={eval("item.q"+this.props.selectedLanguage.langCode) && eval("item.q"+this.props.selectedLanguage.langCode).replace(/(&nbsp;|<([^>]+)>)/ig, "")} />
      </View>
    );
  };



  _renderContent = item => {


    return (
      <View style={styles.content} >
        <MyTxt style={{fontSize: 16, lineHeight: 26}} 
        txt={eval("item.a"+this.props.selectedLanguage.langCode) && eval("item.a"+this.props.selectedLanguage.langCode).replace(/(&nbsp;|<([^>]+)>)/ig, "")} />
      </View>
    );
  };

  

  _updateSections = activeSections => {
    this.setState({ activeSections });
  };

  componentDidMount() {
    // Update the document title using the browser API
    this.props.navigation.setOptions({ headerTitle: <HdrTitle txt={this.props.selectedLanguage.faqsTxt} /> });
    this.fetchRows();
  }

  fetchRows() {
    getData(this.props.defaultSets.apiUrl + 'faqs')
      //.then(response => response.json())
      .then(response => {
        this.setState({rows: response.data, loading: false });
        //console.log(response.data.rows);
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  render() {


    const {  rows, loading } = this.state;
    let lng = this.props.selectedLanguage;
    //let elmntTrns = row.elment_trans && row.elment_trans.find(item => item.languageCode == this.props.selectedLanguage.langCode);

    return (

      <Background>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={loading} onRefresh={() => this.fetchRows()} />
        }
      >
        {rows && <Accordion
        sections={rows}
        activeSections={this.state.activeSections}
        //renderSectionTitle={this._renderSectionTitle}
        renderHeader={this._renderHeader}
        renderContent={this._renderContent}
        onChange={this._updateSections}
        
      />}
      </ScrollView>
      </Background>


      
    );
  };
}


const styles = StyleSheet.create({
  
  content: {
    width: 350,
    padding: 20,
    backgroundColor: '#fff'
  },
 title: {
   backgroundColor: '#f4f4f4',
   borderColor: '#fff',
   borderWidth: 1,
   width: 350,
   padding: 20,
 }
})
const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
    defaultSets: state.DfltSts,
    userInfo: state.User.info,
  }
}
export default connect(mapStateToProps, null)(Faq);