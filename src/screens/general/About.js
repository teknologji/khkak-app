import React from "react";
import { View, StyleSheet, ScrollView, RefreshControl, Linking } from "react-native";
import HdrTitle from "../../components/HdrTitle";
import { connect } from 'react-redux';
import getData from '../../helpers/getData';
import { DataTable } from 'react-native-paper';
import MyTxt from "../../components/MyTxt";
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { cstmStyle } from "../../assets/theme";

class About extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      row: [],
      loading: false
    }
  }

  componentDidMount() {
    // Update the document title using the browser API
    this.props.navigation.setOptions({ headerTitle: <HdrTitle txt={this.props.selectedLanguage.aboutUsTxt} /> });
    this.fetchRow();
  }

  fetchRow() {
    getData(this.props.defaultSets.apiUrl + 'settings')
      //.then(response => response.json())
      .then(response => {
        this.setState({ row: response.data, loading: false });
        //console.log(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  render() {


    const { row, loading } = this.state;
    let lng = this.props.selectedLanguage;
    //const elmntTrns = row.elment_trans && row.elment_trans.find(item => item.languageCode == lng.langCode);

    return (
      <ScrollView
      refreshControl={
        <RefreshControl refreshing={loading} onRefresh={() => this.fetchRow()} />
      }
    >
        <DataTable style={styles.dataTbl}>
        
          <DataTable.Row>
            <DataTable.Title>
              <MyTxt txt={lng.name} />
            </DataTable.Title>
            <DataTable.Cell>{lng.langCode == 'AR' ? row.titleAR : row.titleEN}</DataTable.Cell>
          </DataTable.Row>


          {/* <DataTable.Row>
            <DataTable.Title>
              <MyTxt txt={lng.adrsTxt} />
            </DataTable.Title>
            <DataTable.Cell numberOfLines={2} onPress={() => Linking.openURL(row.mapLink)}>
                 {row.adrsAR} 
            </DataTable.Cell>
          </DataTable.Row> */}


          {/* <DataTable.Row>
            <DataTable.Title>
              <MyTxt txt={lng.mapSiteTxt} />
            </DataTable.Title>
            <DataTable.Cell numberOfLines={2} onPress={() => Linking.openURL(row.mapLink)}>
                         
              <MaterialCommunityIcons style={cstmStyle.textGreen}
                name="map-marker-radius"                
                size={22}
              />
              
            </DataTable.Cell>
          </DataTable.Row> */}

          <DataTable.Row>
            <DataTable.Title>
              <MyTxt txt={lng.phoneTxt} />
            </DataTable.Title>
            <DataTable.Cell>
            {row.phone1+ ' '}            
            <MaterialCommunityIcons style={cstmStyle.textGreen}
                name="phone-classic"                
                size={18}
              />
              
            </DataTable.Cell>
          </DataTable.Row>

          <DataTable.Row>
            <DataTable.Title>
              <MyTxt txt={lng.emailTxt} />
            </DataTable.Title>
            <DataTable.Cell>
            {row.email1 + ' '} 
            <MaterialCommunityIcons style={cstmStyle.textGreen}
                name="email"                
                size={18}
              /> 
              </DataTable.Cell>
          </DataTable.Row>

          <DataTable.Row>
            <DataTable.Title>
              <MyTxt txt={lng.adrsTxt} />
            </DataTable.Title>
            <DataTable.Cell>
           {lng.langCode == 'AR' ? row.adrsAR : row.adrsEN}
            </DataTable.Cell>

            
          </DataTable.Row>

        </DataTable>

      </ScrollView>
    );
  };
}


const styles = StyleSheet.create({
  dataTbl: {
    justifyContent: 'center',
    backgroundColor: '#fff',
    padding: 8,
    marginTop: 20
  },
});

const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
    defaultSets: state.DfltSts,
    userInfo: state.User.info,
  }
}
export default connect(mapStateToProps, null)(About);