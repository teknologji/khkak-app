import React from "react";
import { View, StyleSheet, ScrollView, RefreshControl, Linking } from "react-native";
import HdrTitle from "../../components/HdrTitle";
import { connect } from 'react-redux';
import getData from '../../helpers/getData';
import { DataTable } from 'react-native-paper';
import MyTxt from "../../components/MyTxt";
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { cstmStyle } from "../../assets/theme";

class Cpage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      row: [],
      loading: false
    }
  }

  componentDidMount() {
    // Update the document title using the browser API
    this.fetchRow();
  }

  componentDidUpdate(prevProps, prevState) {
    if(this.props.route.params.id != prevProps.route.params.id) {
      
    this.fetchRow();
    }
  }

  fetchRow() {
    getData(this.props.defaultSets.apiUrl + 'cpages/'+this.props.route.params.id)
      //.then(response => response.json())
      .then(response => {
        this.setState({ row: response.data, loading: false });
        this.props.navigation.setOptions({ headerTitle: <HdrTitle txt={this.props.selectedLanguage.langCode == 'AR' ? response.data.titleAR: response.data.titleEN} /> });

        //console.log(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  render() {


    const { row, loading } = this.state;
    let lng = this.props.selectedLanguage;
    //const elmntTrns = row.elment_trans && row.elment_trans.find(item => item.languageCode == lng.langCode);
    let description = lng.langCode == 'AR'  ? 
    row.descriptionAR && row.descriptionAR.replace(/(&nbsp;|<([^>]+)>)/ig, "") 
    : 
    row.descriptionEN && row.descriptionEN.replace(/(&nbsp;|<([^>]+)>)/ig, "");

    let title = lng.langCode == 'AR'  ? 
    row.titleAR && row.titleAR.replace(/(&nbsp;|<([^>]+)>)/ig, "") 
    : 
    row.titleEN && row.titleEN.replace(/(&nbsp;|<([^>]+)>)/ig, "");

    return (
      <ScrollView
      refreshControl={
        <RefreshControl refreshing={loading} onRefresh={() => this.fetchRow()} />
      }
    >
      
      <MyTxt txt={title} fntWght='bold' style={{fontSize: 18, padding: 15}} />
      <MyTxt txt={description} style={{fontSize: 16, lineHeight: 28, padding: 15}} />
      </ScrollView>
    );
  };
}


const styles = StyleSheet.create({
  dataTbl: {
    justifyContent: 'center',
    backgroundColor: '#fff',
    padding: 8,
    marginTop: 20
  },
});

const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
    defaultSets: state.DfltSts,
    userInfo: state.User.info,
  }
}
export default connect(mapStateToProps, null)(Cpage);