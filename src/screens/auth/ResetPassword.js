import React, {Component} from 'react';
import { View, Text  } from "react-native";
import Background from '../../components/Background';
import Button from '../../components/Button';
import Logo from '../../components/Logo';
import TextInput from '../../components/TextInput';
import Header from '../../components/Header';
import { connect } from 'react-redux';
import Toast from 'react-native-root-toast';
import postData from '../../helpers/postData';
import MyTxt from '../../components/MyTxt';
import HdrTitle from '../../components/HdrTitle';

//import Moment from 'moment'; 

class Login extends Component {
    constructor(props){
        super(props);
        this.state = {
            phone: '',
            errorMessage: '',
            isDisabled: false,
        };

    }

    
  componentDidMount() {
    this.props.navigation.setOptions({ headerTitle: <HdrTitle txt={this.props.selectedLanguage.resetPasswordTxt} /> });
  }
    /* onChangeValue(e) {
        //console.log(e.target.value)
        this.setState({
            [e.target.name]: e.target.value, isDisabled: false
        });
    } */
    onChangeValue = (key, val) => {
    
      this.setState({ [key]: val })
    }
    handleSubmit = () => {


        this.setState({isDisabled: true});

        let ldToast = Toast.show(this.props.selectedLanguage.loadingTxt, {
          duration: 10000, //Toast.durations.LONG,
          position: 0, //Toast.positions.TOP
          shadow: true,
          animation: true,
          hideOnPress: true,
          delay: 0,
          backgroundColor: '#ffc107' //dc3545 //7aa513
            })
            
            
//return console.log(this.state.countryInfo.tcNrCode+this.state.tcNr)
        postData(this.props.defaultSets.apiUrl+'reset-password', {
          phone: this.state.phone,
          langCode: this.props.selectedLanguage.langCode
        }) 
            .then( (response) => {
              Toast.hide(ldToast);

                if(response.data.success == true) {
                    
                  Toast.show(this.props.selectedLanguage.nwPswrdSnt, {
                    duration: 3000, //Toast.durations.LONG,
                    position: 0, //Toast.positions.TOP
                    shadow: true,
                    animation: true,
                    hideOnPress: true,
                    delay: 0,
                    backgroundColor: '#7aa513'
                
                });
        
                  setTimeout(() => {
                    this.props.navigation.navigate('Login');
                  }, 3000);

                  
                } else {
                  Toast.show(response.data.msg, {
                    duration: 10000, //Toast.durations.LONG,
                    position: 0, //Toast.positions.TOP
                    shadow: true,
                    animation: true,
                    hideOnPress: true,
                    delay: 0,
                    backgroundColor: '#dc3545' //dc3545 //7aa513
                      });
                }
            })
            .then( (response) => response.json() )
            .catch( (error) => {
               //console.log(error.response.status, this.props.defaultSets.apiUrl+'register')

                console.log(error.response);
                this.setState({  isDisabled: false})

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                      errorTxt = errorTxt + ' '+ errorMessage[key][0];
                      });

                      Toast.hide(ldToast);

                      Toast.show(<MyTxt txt={errorTxt} style={{color: '#fff'}} />, {
                        duration: 6000, //Toast.durations.LONG,
                          position: 0, //Toast.positions.TOP
                          shadow: true,
                          animation: true,
                          hideOnPress: true,
                          delay: 0,
                          backgroundColor: '#dc3545' //dc3545 //7aa513
                            }); 
                            //alert(errorTxt)
                            //console.log(errorTxt)
                }
            });

    }

  
    render() {


        const {isDisabled} = this.state;
        const lng = this.props.selectedLanguage;
        
        return (

      <Background>
{/* 
      <Logo />
 
      <Header>{lng.resetPasswordTxt}</Header>*/}

      <TextInput
        label={lng.phoneTxt}
        returnKeyType="next"
        onChangeText={(val) => this.onChangeValue('phone', val)}
        maxLength={12}
        autoCapitalize="none"
        autoCompleteType="tel"
        textContentType="telephoneNumber"
        keyboardType="phone-pad"
      />

      <Button mode="contained"
      style={{marginTop: 40}}
      isDisabled={isDisabled} 
      onPress={this.handleSubmit}>
        {lng.resetPasswordTxt}
      </Button>

    </Background>
   
        )
    }
}

 
const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info, 
    }
}
export default connect(mapStateToProps, null)(Login);
