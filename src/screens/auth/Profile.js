import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, TouchableOpacity, Image } from "react-native";
import Background from '../../components/Background';
import Button from '../../components/Button';
import { cstmStyle, theme } from '../../assets/theme';
import TextInput from '../../components/TextInput';
import getData from '../../helpers/getData';
import { connect } from 'react-redux';
import Toast from 'react-native-root-toast';
import { Surface } from 'react-native-paper';


//file upload.

import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import MyTxt from '../../components/MyTxt';
import putData from '../../helpers/putData';
import EditThumb from '../../components/Thumbnail';
// end file upload.
import Moment from 'moment';


class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      row: [],
      firstName: '',
      lastName: '',
      email: '',
      phone: '',
      password: '',

      errorMessage: '',
      isDisabled: false,

    };

  }



  onChangeValue = (key, val) => {

    this.setState({ [key]: val })
  }
  handleSubmit = () => {


    this.setState({ isDisabled: true });

    let ldToast = Toast.show(this.props.selectedLanguage.loadingTxt, {
      duration: 10000, //Toast.durations.LONG,
      position: 0, //Toast.positions.TOP
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      backgroundColor: '#ffc107' //dc3545 //7aa513
    })


    //return console.log(this.state.countryInfo.phoneCode+this.state.phone)
    putData(this.props.defaultSets.apiUrl+'users/'+this.props.userInfo.id, {
      id: this.props.userInfo.id, //this.props.userInfo.id,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      email: this.state.email,
      phone: this.state.phone,
      password: this.state.password,
      langCode: this.props.selectedLanguage.langCode
    }, this.props.userInfo.accessToken)
      .then((response) => {
        Toast.hide(ldToast);

        if (response.data.success == true) {

          // get added user.
          let theUser = response.data.user;

          // save user in redux.
          theUser.accessToken = response.data.accessToken;
          this.props.saveUserInfo(theUser);


          Toast.show(response.data.msg, {
            duration: 3000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#7aa513' //dc3545 //7aa513
          });

          /* setTimeout( () => {
              this.props.navigation.navigate('Home');
          }, 3000); */

        } else {
          Toast.show(response.data.msg, {
            duration: 10000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#ffc107' //dc3545 //7aa513
          });
        }
      })
      .then((response) => response.json())
      .catch((error) => {
        //console.log(error.response.status, this.props.defaultSets.apiUrl+'register')

        //console.log(error.response.data);
        this.setState({ isDisabled: false })
        let errorTxt = '';

        if (error.response !== undefined && error.response.status === 422) {
          let errorMessage = error.response.data.errors;

          Object.keys(errorMessage).map((key, i) => {
            errorTxt = errorTxt + ' ' + errorMessage[key][0] + "\n";
            //errorTxt = errorMessage[key][0];
          });
          Toast.hide(ldToast);       

        } else if (error.response !== undefined && error.response.status === 401) {
          errorTxt = this.props.selectedLanguage.plsLgnFrst; //error.response.data.message;
        }

        if(errorTxt != '') {

        
          Toast.show(<MyTxt txt={errorTxt} style={{ lineHeight: 28 }} />, {
            duration: 6000, //Toast.durations.LONG,
          position: 0, //Toast.positions.TOP
          shadow: true,
          animation: true,
          hideOnPress: true,
          delay: 0,
          backgroundColor: '#dc3545' //dc3545 //7aa513
        });
      }
        
      });

  }


  componentDidMount() {
    this.props.navigation.setOptions({ headerTitle: this.props.selectedLanguage.profileTxt });
    console.log(this.props.userInfo.accessToken)

    this.fetchData(this.props.userInfo.id);    
  }
 
  
fetchData(elmntId) {
  getData(this.props.defaultSets.apiUrl+'users/'+elmntId+'/edit', this.props.userInfo.accessToken)

      .then(response => {

        this.setState({
          row: response.data,
          firstName: response.data.firstName,
          lastName: response.data.lastName,
          email: response.data.email,
          phone: response.data.phone,
          password: response.data.password,
        });
        //console.log(response.data)
      })
      .catch(function (error) {
          console.log(error);
      });
}

  render() {


    const { isDisabled, aFile, row, 
      firstName,
      lastName,
      email,
      phone,
      
      } = this.state;
    const lng = this.props.selectedLanguage;

    return (

      <Background>

<ScrollView style={{ flex: 1, width: '100%' }}>

<Surface style={cstmStyle.surface}>


        <TextInput
          label={lng.firstNameTxt}
          value={firstName}
          returnKeyType="next"
          onChangeText={(val) => this.onChangeValue('firstName', val)}
        />

        <TextInput
          label={lng.lastNameTxt}
          value={lastName}
          returnKeyType="next"
          onChangeText={(val) => this.onChangeValue('lastName', val)}
        />


        <TextInput
          label={lng.phone}
          value={phone}
          returnKeyType="next"
          onChangeText={(val) => this.onChangeValue('phone', val)}
          maxLength={12}
          autoCapitalize="none"
          autoCompleteType="tel"
          textContentType="telephoneNumber"
          keyboardType="phone-pad"
        />

<TextInput
          label={lng.emailTxt}
          value={email}
          returnKeyType="next"
          onChangeText={(val) => this.onChangeValue('email', val)}         
          autoCapitalize="none"
        />

<TextInput
          label={lng.passwordTxt}
          returnKeyType="done"
          onChangeText={(val) => this.onChangeValue('password', val)}
          /* error={false}
          errorText={'pass err'} */
          secureTextEntry
          autoCompleteType="tel"    
          textContentType="telephoneNumber"
          keyboardType="phone-pad"
        />

</Surface>

<Button mode="contained"
              isDisabled={isDisabled}
              onPress={this.handleSubmit}>
              {lng.saveTxt}
            </Button>
          
          
        </ScrollView>
      </Background>

    )
  }
}


const styles = StyleSheet.create({

  row: {
    flexDirection: 'row',
    marginTop: 4,
    marginBottom: 15
  },


});


const mapDispatchToProps = (dispatch) => {
  return {
    saveUserInfo: (userInfo) => dispatch({ type: 'SAVE_USER', payload: userInfo }),

  }
};

const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
    defaultSets: state.DfltSts,
    userInfo: state.User.info,
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Profile);
