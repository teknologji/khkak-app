import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, TouchableOpacity } from "react-native";
import Background from '../../components/Background';
import Button from '../../components/Button';
import TextInput from '../../components/TextInput';
import { connect } from 'react-redux';
import Toast from 'react-native-root-toast';
import postData from '../../helpers/postData';
import MyTxt from '../../components/MyTxt';
import HdrTitle from '../../components/HdrTitle';
import { Surface } from 'react-native-paper';
import { cstmStyle } from '../../assets/theme';

//import Moment from 'moment';


class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      errorMessage: '',
      isDisabled: false,
    };

  }

  /* onChangeValue(e) {
      //console.log(e.target.value)
      this.setState({
          [e.target.name]: e.target.value, isDisabled: false
      });
  } */
  onChangeValue = (key, val) => {

    this.setState({ [key]: val })
  }
  handleSubmit = () => {


    this.setState({ isDisabled: true });

    let ldToast = Toast.show(this.props.selectedLanguage.loadingTxt, {
      duration: 10000, //Toast.durations.LONG,
      position: 0, //Toast.positions.TOP
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      backgroundColor: '#ffc107' //dc3545 //7aa513
    })


    //return console.log(this.state.countryInfo.tcNrCode+this.state.tcNr)
    postData(this.props.defaultSets.apiUrl + 'login', {

      phone: this.state.phone,
      password: this.state.password,
      langCode: this.props.selectedLanguage.langCode
    })
      .then((response) => {
        Toast.hide(ldToast);

        if (response.data.success == true) {

          let theUser = response.data.user;
          theUser.accessToken = response.data.accessToken;
          this.props.saveUserInfo(response.data.user);

          if(this.props.route.params.screenToBack != undefined && this.props.route.params.screenToBack == 'ViewCart') {
            return this.props.navigation.navigate('ViewCart');
          }
          this.props.navigation.navigate('homePage');

          //console.log(response.data.accessToken)
          /* setTimeout( () => {
            this.props.navigation.navigate('homePage');
          }, 2000); */ 

        } else {
          Toast.show(response.data.msg, {
            duration: 10000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#dc3545' //dc3545 //7aa513
          });
        }
      })
      .then((response) => response.json())
      .catch((error) => {
        //console.log(error.response.status, this.props.defaultSets.apiUrl+'register')

        console.log(error);
        this.setState({ isDisabled: false })

        if (error.response !== undefined && error.response.status === 422) {
          let errorTxt = '';
          let errorMessage = error.response.data.errors;

          Object.keys(errorMessage).map((key, i) => {
            //errorTxt = (<Text>{errorTxt} {errorMessage[key][0]} {"\n"}</Text>);
            errorTxt = errorTxt + ' ' + errorMessage[key][0] + "\n";

          });
          Toast.hide(ldToast);

          Toast.show(<MyTxt txt={errorTxt} style={{ lineHeight: 28 }} />, {
            duration: 6000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#dc3545' //dc3545 //7aa513
          });
          //alert(errorTxt)
          //console.log(errorTxt)
        }
      });

  }
  componentDidMount() {
    this.props.navigation.setOptions({ headerTitle: <HdrTitle txt={this.props.selectedLanguage.loginTxt} /> });

  }

  render() {


    const { isDisabled } = this.state;
    const lng = this.props.selectedLanguage;

    return (

      <Background>

        {/* <Logo /> */}


        <ScrollView style={{ flex: 1, width: '100%' }}>
        <Surface style={cstmStyle.surface}>

        <TextInput
          label={lng.phoneTxt}
          returnKeyType="next"
          onChangeText={(val) => this.onChangeValue('phone', val)}
          maxLength={12}
          autoCapitalize="none"
          autoCompleteType="tel"
          textContentType="telephoneNumber"
          keyboardType="phone-pad"
        />


        <TextInput
          label={lng.passwordTxt}
          returnKeyType="done"
          onChangeText={(val) => this.onChangeValue('password', val)}
          /* error={false}
          errorText={'pass err'} */
          secureTextEntry      
        />

        <View style={styles.forgotPassword}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('ResetPassword')}
          >
            <MyTxt txt={lng.forgotPasswordTxt} />
          </TouchableOpacity>
        </View>


        <Button mode="contained"
          isDisabled={isDisabled}
          onPress={this.handleSubmit}>
          {lng.loginTxt}
        </Button>

  
        
        </Surface>
        <View style={styles.row}>
          <MyTxt txt={lng.dontHvAccnt} />
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Register')}>

            <MyTxt style={styles.link} txt={lng.crtNewAccntTxt} />
          </TouchableOpacity>
        </View>
        </ScrollView>
      </Background>

    )
  }
}


const styles = StyleSheet.create({
 
  forgotPassword: {
    width: '100%',
    alignItems: 'flex-end',
    marginBottom: 24,
  },
  row: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginTop: 44,
  },

});


const mapDispatchToProps = (dispatch) => {
  return {
    saveUserInfo: (userInfo) => dispatch({ type: 'SAVE_USER', payload: userInfo }),

  }
};

const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
    defaultSets: state.DfltSts,
    userInfo: state.User.info,
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Login);
