import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, Animated } from "react-native";
import Background from '../../components/Background';
import Button from '../../components/Button';
import TextInput from '../../components/TextInput';
import { connect } from 'react-redux';
import Toast from 'react-native-root-toast';
import postData from '../../helpers/postData';
import MyTxt from '../../components/MyTxt';
import HdrTitle from '../../components/HdrTitle';
import { Surface } from 'react-native-paper';
import { cstmStyle } from '../../assets/theme';
import { CountdownCircleTimer } from 'react-native-countdown-circle-timer'

//import Moment from 'moment';


class Activation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      code: '', 
      errorMessage: '', 
      isDisabled: false, 
      resendDisabled: true,
      showCounter: true,
      akey: 1,
      isPlaying: true
    };

  }


  resendCode() {

    let ldToast = Toast.show(this.props.selectedLanguage.loadingTxt, {
      duration: 10000, //Toast.durations.LONG,
      position: 0, //Toast.positions.TOP
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      backgroundColor: '#ffc107' //dc3545 //7aa513
    })
    
    this.setState({resendDisabled: true, isPlaying: true, showCounter: true});

    postData(this.props.defaultSets.apiUrl+'resend-activation', {
        userId: 58, // this.props.userInfo.id,
        langCode: this.props.selectedLanguage.langCode
    })
        .then((response) => {


          Toast.show(response.data.msg, {
            duration: 3000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#7aa513' //dc3545 //7aa513
          });

         

        })
        .catch((error) => {
            console.log(error);
        });
}

  onChangeValue = (key, val) => {

    this.setState({ [key]: val })
  }





  handleSubmit = () => {

    let ldToast = Toast.show(this.props.selectedLanguage.loadingTxt, {
      duration: 10000, //Toast.durations.LONG,
      position: 0, //Toast.positions.TOP
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      backgroundColor: '#ffc107' //dc3545 //7aa513
    })
    
    //this.setState({isPlaying: false, showCounter: false})

    postData(this.props.defaultSets.apiUrl+'activation', {
        code: this.state.code,
        langCode: this.props.selectedLanguage.langCode
    })
        .then((response) => {
            //console.log(response.data);
            Toast.hide(ldToast);

            if (response.data.success == true) {
    
              let theUser = response.data.user;
              theUser.accessToken = response.data.accessToken;
              this.props.saveUserInfo(response.data.user);
    
              setTimeout( () => {
                this.props.navigation.navigate('homePage');
              }, 2000);
              
              return;
            } 
            
              return Toast.show(response.data.msg, {
                duration: 10000, //Toast.durations.LONG,
                position: 0, //Toast.positions.TOP
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
                backgroundColor: '#dc3545' //dc3545 //7aa513
              });
            

            //
        })
        .catch((error) => {
            //console.log(error);

            if (error.response !== undefined && error.response.status === 422) {
              let errorTxt = '';
              let errorMessage = error.response.data.errors;
    
              Object.keys(errorMessage).map((key, i) => {
                //errorTxt = (<Text>{errorTxt} {errorMessage[key][0]} {"\n"}</Text>);
                errorTxt = errorTxt + ' ' + errorMessage[key][0] + "\n";
    
              });
              Toast.hide(ldToast);
    
              Toast.show(<MyTxt txt={errorTxt} style={{ lineHeight: 28 }} />, {
                duration: 6000, //Toast.durations.LONG,
                position: 0, //Toast.positions.TOP
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
                backgroundColor: '#dc3545' //dc3545 //7aa513
              });
              //alert(errorTxt)
              //console.log(errorTxt)
            }
        });

}



  componentDidMount() {
    this.props.navigation.setOptions({ headerTitle: <HdrTitle txt={this.props.selectedLanguage.verificationCodeTxt} /> });

  }

  render() {


    const { isDisabled, showCounter, isPlaying, resendDisabled } = this.state;
    const lng = this.props.selectedLanguage;

    return (

      <Background>

        {/* <Logo /> */}

        <ScrollView style={{ flex: 1, width: '100%' }}>
          
        <Surface style={cstmStyle.surface}>


        <MyTxt txt={lng.writeActvtnCdTxt} />

        <TextInput
          label={lng.verificationCodeTxt}
          returnKeyType="next"
          onChangeText={(val) => this.onChangeValue('code', val)}
          maxLength={4}
          autoCapitalize="none"
          autoCompleteType="tel"
          textContentType="telephoneNumber"
          keyboardType="phone-pad"
        />



        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
        <Button mode="contained"
          isDisabled={isDisabled}
          onPress={this.handleSubmit}>
          {lng.verifyTxt}
        </Button>

{!resendDisabled && 
  <Button mode="contained"
          isDisabled={resendDisabled}
          onPress={() =>{ this.resendCode(); } }>
          {lng.resendTxt}
        </Button>
}
        
        </View>
        
        </Surface>
        <View style={styles.row}>
          
        {showCounter && 
        <CountdownCircleTimer
        onComplete={() => {
          // do your stuff here
          this.setState({resendDisabled: false})
          return [resendDisabled, 1500] // repeat animation in 1.5 seconds
        }}
        isPlaying
        duration={6}

        colors={[
          ['#004777', 0.4],
          ['#F7B801', 0.4],
          ['#A30000', 0.2],
        ]}
      >
        {({ remainingTime, animatedColor }) => (
          <Animated.Text style={{ color: animatedColor }}>
            {remainingTime}
          </Animated.Text>
        )}
      </CountdownCircleTimer>
      }
  
        </View>
        </ScrollView>
      </Background>

    )
  }
}


const styles = StyleSheet.create({
 
  forgotPassword: {
    width: '100%',
    alignItems: 'flex-end',
    marginBottom: 24,
  },
  row: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginTop: 44,
  },

});


const mapDispatchToProps = (dispatch) => {
  return {
    saveUserInfo: (userInfo) => dispatch({ type: 'SAVE_USER', payload: userInfo }),

  }
};

const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
    defaultSets: state.DfltSts,
    userInfo: state.User.info,
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Activation);
