import React, { Component } from 'react';
import { View, Text, ScrollView, StyleSheet, TouchableOpacity, Image } from "react-native";
import Background from '../../components/Background';
import Button from '../../components/Button';
import { cstmStyle, theme } from '../../assets/theme';
import TextInput from '../../components/TextInput';
import { connect } from 'react-redux';
import Toast from 'react-native-root-toast';
import postData from '../../helpers/postData';
import { Surface, Switch, List } from 'react-native-paper';

//file upload.
import MyTxt from '../../components/MyTxt';
import HdrTitle from '../../components/HdrTitle';
// end file upload.
import Moment from 'moment';


class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      phone: '',
      password: '',

      groupId: 2, // 2:client.
      errorMessage: '',
      isDisabled: false,
      fromMobApp: 1
    };

  }


  onChangeValue = (key, val) => {

    this.setState({ [key]: val })
  }
  handleSubmit = () => {

   
    this.setState({ isDisabled: true });

    let ldToast = Toast.show(this.props.selectedLanguage.loadingTxt, {
      duration: 10000, //Toast.durations.LONG,
      position: 0, //Toast.positions.TOP
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      backgroundColor: '#ffc107' //dc3545 //7aa513
    })


    //return console.log(this.state.countryInfo.phoneCode+this.state.phone)
    postData(this.props.defaultSets.apiUrl + 'register', {

      firstName: this.state.firstName,
      lastName: this.state.lastName,
      email: this.state.email,
      phone: this.state.phone,
      password: this.state.password,
      groupId: this.state.groupId,
      fromMobApp: this.state.fromMobApp,
      langCode: this.props.selectedLanguage.langCode
    })
      .then((response) => {
        Toast.hide(ldToast);

        if (response.data.success == true) {

          // get added user.
          let theUser = response.data.user;

          // save user in redux. // these two lines should be moved to activation screen.
          theUser.accessToken = null; // to prevent him from admin panel till to be activated.
          this.props.saveUserInfo(theUser);

          this.props.navigation.navigate('Activation');

        } else {
          Toast.show(response.data.msg, {
            duration: 10000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#dc3545' //dc3545 //7aa513
          });
        }
      })
      .then((response) => response.json())
      .catch((error) => {
        //console.log(error.response.status, this.props.defaultSets.apiUrl+'register')

        //return console.log(error.response);
        this.setState({ isDisabled: false })

        if (error.response !== undefined && error.response.status === 422) {
          let errorTxt = '';
          let errorMessage = error.response.data.errors;

          Object.keys(errorMessage).map((key, i) => {
            errorTxt = errorTxt + ' ' + errorMessage[key][0] + "\n";
            //errorTxt = errorMessage[key][0];
          });
          Toast.hide(ldToast);

          Toast.show(<MyTxt txt={errorTxt} style={{ lineHeight: 28 }} />, {
            duration: 6000, //Toast.durations.LONG,
            position: 0, //Toast.positions.TOP
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: '#dc3545' //dc3545 //7aa513
          });
        }
      });

  }


  componentDidMount() {
    this.props.navigation.setOptions({ headerTitle: <HdrTitle txt={this.props.selectedLanguage.registerTxt} /> });

  }
  render() {


    const { isDisabled} = this.state;
    const lng = this.props.selectedLanguage;

    return (

      <Background>

        <ScrollView style={{ flex: 1, width: '100%' }}>

          <Surface style={cstmStyle.surface}>

            <TextInput
              label={lng.firstNameTxt}
              returnKeyType="next"
              onChangeText={(val) => this.onChangeValue('firstName', val)}
            />

            <TextInput
              label={lng.lastNameTxt}
              returnKeyType="next"
              onChangeText={(val) => this.onChangeValue('lastName', val)}
            />

            <TextInput
              label={lng.phone}
              returnKeyType="next"
              onChangeText={(val) => this.onChangeValue('phone', val)}
              maxLength={12}
              autoCapitalize="none"
              autoCompleteType="tel"
              textContentType="telephoneNumber"
              keyboardType="phone-pad"
            />

<TextInput
              label={lng.emailTxt}
              returnKeyType="next"
              onChangeText={(val) => this.onChangeValue('email', val)}
              autoCapitalize="none"
            />

<TextInput
              label={lng.passwordTxt}
              returnKeyType="done"
              onChangeText={(val) => this.onChangeValue('password', val)}
              /* error={false}
              errorText={'pass err'} */
              secureTextEntry
              autoCompleteType="tel"    
              textContentType="telephoneNumber"
              keyboardType="phone-pad"
            />

          </Surface>

         
          <Surface style={cstmStyle.surface}>

            <Button mode="contained"
              isDisabled={isDisabled}
              onPress={this.handleSubmit}>
              {lng.saveTxt}
            </Button>
          
          </Surface>

          <View style={styles.row}>
            <MyTxt txt={lng.alreadyHaveAcct} />

            <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
              <MyTxt txt={lng.loginTxt} />
            </TouchableOpacity>
          </View>

        </ScrollView>
      </Background>

    )
  }
}

const styles = StyleSheet.create({


  row: {
    alignSelf: 'center',
    flexDirection: 'row',
    marginTop: 14,
    marginBottom: 15
  },
  radiosBox: { flexDirection: 'row', justifyContent: 'space-between', margin: 20 },

});



const mapDispatchToProps = (dispatch) => {
  return {
    saveUserInfo: (userInfo) => dispatch({ type: 'SAVE_USER', payload: userInfo }),

  }
};

const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
    defaultSets: state.DfltSts,
    userInfo: state.User.info,
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Register);
