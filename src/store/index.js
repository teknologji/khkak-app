
import { combineReducers, createStore } from 'redux';
import cartItems from './reducers/cartItems';
import LanguageReducer from './reducers/LanguageReducer';
//import Curr from '../reducers/Curr';
import DfltSts from './reducers/DfltSts';
import User from './reducers/User';

const store = createStore(combineReducers({
    cartItems,
    LanguageReducer,
    //Curr,
    DfltSts,
    User
  })
  )
//console.log(store.getState())

export default store 