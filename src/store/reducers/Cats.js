
const INITIAL_STATE = {rows: []};

export const Cats  = (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case 'SAVE_CATS':
            return {
                rows: action.payload
            };
        default:
            return state; 
    }
};

export default Cats;
