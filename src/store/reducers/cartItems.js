
const initialState = {
  cart: [],
  total: 0,
  //subTotalPrice : 0,
  totalPrice: 0,
  totalQuantity: 0,
  /* totalWeight:0,
  isOffer:0 */
}

const cartItems = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_TO_CART':
      //console.log(action.payload)
      if (action.qty === undefined) {
        action.qty = 1;
      }

      let addedItem = state.cart.find(item => item.id == action.payload.id);

      if (addedItem) {
        state.cart = state.cart.filter(item => item.id != action.payload.id);

        addedItem.quantity = addedItem.quantity + parseInt(action.qty);
        state.cart.push(addedItem);

      } else {

        action.payload.quantity = parseInt(action.qty);
        //state.cart.push(addedItem);
        state.cart = [action.payload, ...state.cart];
      }


      //totalPriceOfAllCartItems = state.total + action.payload.subTotalPrice;
      state.totalQuantity = 0;
      //state.totalWeight = 0;
      //state.isOffer = 0;
      state.totalPrice = 0;

      state.cart.map((itemk) => {
        state.totalQuantity += itemk.quantity;
        //state.totalWeight += itemk.weight;
        //state.isOffer = (itemk.isOffer == 1 ) ? 1 : 0;
        state.totalPrice += itemk.quantity * itemk.priceAfterDiscount;

      });
      state.cart.sort(function (a, b) {
        return a.id - b.id;
      });
      //console.log(state);
      return {
        ...state,
        //cart: [action.payload, ...state.cart],
        //total:totalPriceOfAllCartItems,

        totalQuantity: state.totalQuantity,
        //totalWeight: state.totalWeight,
        //isOffer: state.isOffer,
        totalPrice: state.totalPrice

      }
    case 'REMOVE_FROM_CART':
      //let itemToRemove= state.cart.find(item=> action.payload.id === item.id)
      let new_items = state.cart.filter(item => action.payload.id !== item.id)


      state.totalQuantity = 0;
      //state.totalWeight = 0;
      //state.isOffer = 0;
      state.totalPrice = 0;


      new_items.map((itemk) => {
        state.totalQuantity += itemk.quantity;
        //state.totalWeight += itemk.weight;
        //state.isOffer = (itemk.isOffer == 1 ) ? 1 : 0;
        state.totalPrice += itemk.quantity * itemk.priceAfterDiscount;

      });

      return {
        ...state,
        cart: new_items,
        totalQuantity: state.totalQuantity,
        //totalWeight: state.totalWeight,
        //isOffer: state.isOffer,
        totalPrice: state.totalPrice

      }

    case 'UPDATE_CART_TOTAL':
      return Object.assign({}, state, {
        total: state.total + action.newSubTotalValue
      });

    case 'ChANGE_PRODUCT_QUANTITY':
      let item = state.cart.find(item => item.id == action.productId);

      let newCart = state.cart.filter(item => item.id != action.productId);

      item.quantity = parseInt(action.newQuantity);


      newCart.push(item);

      newCart.sort(function (a, b) {
        return a.id - b.id;
      });

      state.totalQuantity = 0;
      //state.totalWeight = 0;
      //state.isOffer = 0;
      state.totalPrice = 0;


      newCart.map((itemk) => {
        state.totalQuantity += parseInt(itemk.quantity);
        //state.totalWeight += itemk.weight * itemk.quantity;
        //state.isOffer = (itemk.isOffer == 1 ) ? 1 : 0;
        state.totalPrice += itemk.quantity * itemk.priceAfterDiscount;

      });

      state.cart = newCart;
      state.totalQuantity = state.totalQuantity;
      state.totalPrice = state.totalPrice;

      return {
        ...state,
        //cart: newCart,
        //totalQuantity: state.totalQuantity,
        //totalPrice:state.totalPrice
        //totalWeight: state.totalWeight,
        //isOffer: state.isOffer,
      };
    case 'EMPTIFY_CART':
      state.cart = [];
      state.totalQuantity = 0;
      state.totalPrice = 0;
      return {
        ...state
        /* cart: [],
        total: 0,
        //subTotalPrice : 0,
        totalPrice: 0, */
      };

    case 'logout':
      return {
        cart: [],
        total: 0,
        //subTotalPrice : 0,
        totalPrice: 0,
      };
  }

  return state
}


export default cartItems;



