import { combineReducers } from 'redux'

import LanguageReducer from './LanguageReducer';
import cartItems from './cartItems';
import User from './User';
import DfltSts from './DfltSts';
import Cats from './Cats';
import Favourites from './Favourites';

export default combineReducers({
    cartItems,
    LanguageReducer,
    User,
    DfltSts,
    Cats,
    Favourites
});