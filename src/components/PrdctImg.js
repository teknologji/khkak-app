import React, { memo } from 'react';
import { Image, StyleSheet } from 'react-native';

const PrdctImg = ({imgUrl}) => (
  <Image source={{uri: imgUrl}} style={styles.image} />
);

const styles = StyleSheet.create({
  image:{ 
    alignSelf: 'center', 
    width: 120, 
    height: 90, 
    borderRadius: 4 
    }
});

export default memo(PrdctImg);