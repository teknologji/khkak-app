import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import MyTxt from './MyTxt';

//import striptags from 'striptags';
import { connect } from 'react-redux';

//import theme from  '../assets/theme';
import { cstmStyle, theme } from '../assets/theme';
import Toast from 'react-native-root-toast';
import { FontAwesome5, SimpleLineIcons, MaterialCommunityIcons } from '@expo/vector-icons';
import PrdctImg from './PrdctImg';
import Button from '../components/Button';


class CourseBox extends React.Component {

  constructor(props) {
    super(props);

  }


/*   toCourseDetails(id){


    this.props.navigation.navigate({
      routeName: 'EditCourse',
      params: { id: id },
      //key: 'k'+Math.random()
    });

  } */

  addToFav() {

    //this.setState({ isReady: false});  


    if (this.props.userInfo.info.id == 0) {

      return Toast.show(this.props.selectedLanguage.plsLgnFrst, {
        duration: Toast.durations.LONG,
        position: 0, //Toast.positions.TOP,
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 0,
        backgroundColor: '#FF9800'
      });

    }

    let loadingToast = Toast.show(this.props.selectedLanguage.loadingTxt, {
      duration: Toast.durations.LONG,
      position: 0, //Toast.positions.TOP
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      backgroundColor: '#FF9800'

    });

    fetch(this.props.defaultSets.baseUrl + 'add-to-favourite/' + this.props.userInfo.info.id + '/' + this.props.article.id)
      .then((response) => response.json())
      .then((response) => {

        this.props.addPrdctToFav(this.props.article.id);
        //return console.log(response)
        // alert(response.msg)
        Toast.hide(loadingToast);

        Toast.show(response.msg, {
          duration: Toast.durations.LONG,
          position: 0, //Toast.positions.TOP,
          shadow: true,
          animation: true,
          hideOnPress: true,
          delay: 0,
          backgroundColor: '#FF9800'
        });

      }).done();

  }

  componentDidMount() {
    //console.log(cstmStyle)
  }
  render() {
    let item = this.props.item;
    let lng = this.props.selectedLanguage;

    //const {langRow, langAlign} = this.props.selectedLanguage;
    const elmntTrns = item && item.elment_trans.find(row => row.languageCode == lng.langCode);

    //const {currency_code, exchangeRate} = this.props.selectedCurr;
    //const time = (created_at || '');
    /*  const defaultImg =
       '../../assets/img/notfound.png'; */

    return (

      <View style={cstmStyle.boxStyle}>

        {elmntTrns != 888 ?
          <View style={cstmStyle.dsntBx}>


            <MyTxt txt={lng.discountTxt + " 0%"}
              style={[cstmStyle.textWhite, { fontSize: 12 }]} />

          </View>
          : <View style={{ height: 0 }}></View>

        }


        {/* <Icon 
                name="heart" 
                
                onPress={() => {this.addToFav();} }
                //style={[ this.props.fvrdPrdcts.indexOf(id) > -1 || this.state.fvrdPrdctsOfState.indexOf(id) > -1 ? cstmStyle.textDanger : cstmStyle.textLightGrey,{
                  style={[ this.props.userInfo.fvrdPrdcts.indexOf(id) > -1 ? cstmStyle.textDanger : cstmStyle.textLightGrey,{
                  position: "absolute", 
                  right:13, top: 13,
                  fontSize: 22      
  
                  }]}
                /> */}



        <View style={{
          marginTop: 40, marginBottom: 5,
          justifyContent: 'center',
          flex: 1
        }}>

          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('EditCourse', {id: item.id, courseTitle: elmntTrns.title})}>
            <PrdctImg
              imgUrl={item.photo} />
          </TouchableOpacity>


          <TouchableOpacity
            style={{ marginTop: 20 }}
            onPress={() => this.props.navigation.navigate('CoursesIndex')}>

            <MyTxt txt={elmntTrns && elmntTrns.title.substring(0, 30)}
              style={{ textAlign: 'center', lineHeight: 20 }} />


          </TouchableOpacity>
        </View>

        <View style={{ paddingRight: 2, marginBottom: 10, marginTop: 10 }}>

          <View style={{ flexDirection: 'row', justifyContent: 'center'}}>

            <Text style={[cstmStyle.textGreen, {marginRight: 7, fontSize: 18}]}>
            {item.price}
            </Text>
            <Text style={cstmStyle.txtStriked}>
              {item.price}
            </Text>
          </View>


        </View>

        <View>
          <Button mode="contained" style={cstmStyle.bgOrange}
          icon="cart"
          compact={false}
          contentStyle={{height: 30}}
          >          
            <MyTxt txt={this.props.selectedLanguage.addToCartTxt} />
          </Button>
        </View>

      </View>

    );



  }
}

const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
    //selectedCurr: state.Curr.slctdCurr,
    defaultSets: state.DfltSts,
    userInfo: state.User,
  }
}


export default connect(mapStateToProps, null)(CourseBox);
