import React, { memo } from 'react';
import { Image, StyleSheet } from 'react-native';

const Thumbnail = ({imgUrl}) => (
  <Image source={{uri: imgUrl}} style={styles.image} />
);

const styles = StyleSheet.create({
  image: {
    width: 140,
    height: 100,
    marginBottom: 12,
    resizeMode: 'contain'
  },
});

export default memo(Thumbnail);