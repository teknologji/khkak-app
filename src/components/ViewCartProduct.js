import React, { Fragment } from 'react';
import { View, Text } from 'react-native';
import MyTxt from './MyTxt';
import { connect } from 'react-redux';

//import striptags from 'striptags';
import NumericInput from 'react-native-numeric-input'

import { TouchableOpacity } from 'react-native-gesture-handler';

import { Surface } from 'react-native-paper';
import Thumbnail from './Thumbnail';
import { cstmStyle } from '../assets/theme';
import { FontAwesome5 } from '@expo/vector-icons';

class ViewCartProduct extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      //eachProductTotal:0,
      total: 0,
      newSubTotal: 0,
      itemQty:0
    }

  }

  render() {

    let lng = this.props.selectedLanguage;
    let item = this.props.item;
    let {itemQty} = this.state;
    return (
      <Fragment>
        {item != null ? 
        <Surface style={[{ flexDirection: 'row', marginVertical: 5, paddingVertical: 15 }]}>
          <View style={{ flex: 0.3 }}>
            <Thumbnail
              imgUrl={item.imgUrl}
              onPress={() => this.props.navigation.navigate('ShowProduct', {id: item.id})}

            />
          </View>


          <View style={{ flex: 0.7, flexDirection: 'column' }}>



            <View style={{ marginHorizontal: 20, flexDirection: 'row' }}>

              <View style={{ flex: 0.8 }}>

                <TouchableOpacity onPress=  {() => this.props.navigation.navigate('ShowProduct', {id: item.id})}
>
                  <MyTxt txt={eval("item.title" + lng.langCode)}
                    style={[cstmStyle.textDanger]} />

                </TouchableOpacity>

                <MyTxt txt={lng.langCode == 'AR' && item.category ? item.category.titleAR : item.category.titleEN}
                  style={{ marginTop: 5, marginBottom: 5 }}
                />

              </View>

              <View style={{ flex: 0.2 }}>
                <FontAwesome5
                  onPress={() => this.props.removeItem(this.props.item)}
                  name="trash" size={20}
                  style={cstmStyle.textDanger} />


              </View>

            </View>




            <View style={{ marginTop: 10, flexDirection: 'row' }}>

              <View style={{ flex: 0.5, padding: 15 }}>

                {item.priceAfterDiscount != 0 && item.priceBeforeDiscount != item.priceAfterDiscount ?
                  <View style={{ justifyContent: 'space-around' }}>

                    <MyTxt txt={item.priceAfterDiscount + lng.sarTxt} />
                    <MyTxt txt={item.priceBeforeDiscount + lng.sarTxt} />


                  </View>
                  :
                  <View>

                    <MyTxt txt={item.priceBeforeDiscount + lng.sarTxt} />

                  </View>

                }
              </View>

              <View style={{ flex: 0.5 }}>

                <NumericInput
                initValue={item.quantity}
                  value={itemQty}
                  onChange={newQty => {
                    this.props.updateQuantity(item.id, newQty);

                  }}
                  /*onLimitReached={(isMax,msg) => console.log(isMax,msg)}*/
                  totalWidth={100}
                  totalHeight={40}
                  iconSize={25}
                  step={1}
                  minValue={1}
                  valueType='real'
                  rounded
                  textColor='black' /*B0228C*/
                  iconStyle={{ color: 'black' }}   /*#7aa513*/
                  leftButtonBackgroundColor='#fff'
                  rightButtonBackgroundColor='#fff' />

              </View>

            </View>



          </View>

        </Surface>
: null}
      </Fragment>



    );





  }
}


const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
  }
}

export default connect(mapStateToProps, null)(ViewCartProduct);