import React from 'react';
import { Text, View, TouchableOpacity, StyleSheet  } from 'react-native';
import MyTxt from './MyTxt';

//import striptags from 'striptags';
import { connect } from 'react-redux';

//import theme from  '../assets/theme';
import { cstmStyle, theme } from '../assets/theme';
import Toast from 'react-native-root-toast';
import { FontAwesome5, SimpleLineIcons, MaterialCommunityIcons } from '@expo/vector-icons';
import PrdctImg from './PrdctImg';
import Button from './Button';
import {
Surface
} from 'react-native-paper';

class CatBox extends React.Component {

  constructor(props) {
    super(props);

  }


  componentDidMount() {
  }
  render() {
    let item = this.props.item;
    let lng = this.props.selectedLanguage;


    return (

      <Surface style={styles.surface}>
     
     
     <View style={{
          justifyContent: 'center',
         
        }}>

          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('AllProducts', {id: item.id, title: eval("item.title"+lng.langCode)})}>

            <PrdctImg
              imgUrl={item.imgUrl} />
          </TouchableOpacity>
      

          <TouchableOpacity
            style={{ marginTop: 15 }}
            onPress={() => this.props.navigation.navigate('AllProducts', {id: item.id, title: eval("item.title"+lng.langCode)})}>

            <MyTxt txt={eval("item.title"+lng.langCode)}
              style={{ textAlign: 'center', fontSize: 16 }} />


          </TouchableOpacity>
        </View>

      
          <View style={{ justifyContent: 'center', marginBottom: 10, marginTop: 10}}>


            <MyTxt txt={lng.productsTxt + ' : ' + item.products.length} style={[cstmStyle.textDanger, {fontSize: 14}]} />
            
            {/* <Text style={cstmStyle.txtStriked}>
              {item.price}
            </Text> */}
          </View>


        {/* <View>
          <Button 
          mode="contained" 
          style={cstmStyle.bgGrey}
          icon="eye"
          compact={false}
          contentStyle={{height: 30}}
          >          
            <MyTxt txt={this.props.selectedLanguage.addToCartTxt} />
          </Button>
        </View> */}

  </Surface>

    );

  }
}

const styles = StyleSheet.create({
  surface: {
    padding: 8,
    margin: 10,
    height: 280,
    width: 175,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 5,
  },
});

const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
    //selectedCurr: state.Curr.slctdCurr,
    defaultSets: state.DfltSts,
    userInfo: state.User,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      addItemToCart: (product) => dispatch({ type: 'ADD_TO_CART', payload: product }),
      addPrdctToFav: (prdctId) => dispatch({ type: 'ADD_TO_FAV', prdctId })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CatBox);


