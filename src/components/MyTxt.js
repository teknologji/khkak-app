import React from 'react';
import { Text, StyleSheet } from 'react-native';
import * as Font from 'expo-font';
import { theme } from '../assets/theme';

 class MyTxt extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      fontLoaded: false,

    }
  }  

  async componentDidMount() {
    
    await Font.loadAsync({
      'Tajawal-bold': require('../assets/fonts/Tajawal-Bold.ttf'),
      'Tajawal-regular': require('../assets/fonts/Tajawal-Regular.ttf'),
      //'frutiger': require('../../assets/fonts/frutiger.ttf')
      });
    this.setState({ fontLoaded: true });

  }
   
  render() {
    const {txt, fntWght, style} = this.props;
        return (
          this.state.fontLoaded ? (
            <Text style={[
              style ? style : styles.text, 
            {fontFamily: fntWght == 'bold' ? 'Tajawal-bold' : 'Tajawal-regular'}
            ]}>
          {txt}
         </Text>
          ) : null
         
          
        );
      

    
  }
}
const styles = StyleSheet.create({
  text: {
    fontSize: 16,
    color: theme.textWhite,
  },
});
export default MyTxt;
