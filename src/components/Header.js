import React, { memo } from 'react';
import { StyleSheet, Text } from 'react-native';
import { theme } from '../assets/theme';
import MyTxt from './MyTxt';

const Header = ({ children }) => <MyTxt style={styles.header} txt={children} />;

const styles = StyleSheet.create({
  header: {
    fontSize: 22,
    color: theme.colors.primary,
    //paddingVertical: 14,
    alignSelf: 'center'
 },
});

export default (Header);