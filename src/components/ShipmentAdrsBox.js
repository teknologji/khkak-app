import React, { Fragment } from 'react';
import { View, Text } from 'react-native';
import MyTxt from './MyTxt';
import { connect } from 'react-redux';

//import striptags from 'striptags';
import NumericInput from 'react-native-numeric-input'

import { TouchableOpacity } from 'react-native-gesture-handler';

import { Surface } from 'react-native-paper';
import Thumbnail from './Thumbnail';
import { cstmStyle } from '../assets/theme';
import { FontAwesome5 } from '@expo/vector-icons';

class ShipmentAdrsBox extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      //eachProductTotal:0,
      total: 0,
      newSubTotal: 0,
    }

  }




  render() {
    let item = this.props.item;
    let {screenToBack} = this.props;
    let lng = this.props.selectedLanguage;

    return (
      <Fragment>
        <Surface style={[{
          flexDirection: 'row',
          marginVertical: 5,
          paddingVertical: 15,
          backgroundColor: (this.props.shipmentAdrsId == item.id) ? '#ccc' : 'white'
        }]}>



          <View style={{ flex: 1, flexDirection: 'column' }}>



            <View style={{ marginHorizontal: 10, flexDirection: 'row' }}>

              <View style={{ flex: 0.8 }}>

                <TouchableOpacity 
                onPress={() => { screenToBack != undefined && this.props.getShippingCharges(item.id) }}
                >
                  <MyTxt txt={item.firstName + ' ' + item.lastName}
                    style={[cstmStyle.textDanger]} />


                  <MyTxt txt={eval("item.country.title" + lng.langCode) + '-' +
                    /* eval("item.city.title" + lng.langCode) + '-' + */
                    item.adrs
                  }
                    style={{ lineHeight: 26 }}
                  />

                  <MyTxt txt={lng.phoneTxt + '-' + item.receiverMobile + '-' + item.receiverMobile2} />

                </TouchableOpacity>
              </View>

              <View style={{ flex: 0.2, justifyContent: 'space-between', flexDirection: 'column' }}>
                <FontAwesome5
                  //onPress={() => this.props.removeItem(this.props.item)}
                  onPress={() => {this.props.showAlert() }}
                  name="trash" size={20}
                  style={cstmStyle.textDanger} />


                <FontAwesome5
                  onPress={() => this.props.navigation.navigate('EditShipmentAdrs', { id: item.id, screenToBack: this.props.screenToBack })}

                  //onPress={() => this.props.removeItem(this.props.item)}
                  name="pen" size={20}
                  style={cstmStyle.textGreen} />
              </View>

            </View>


          </View>

        </Surface>

      </Fragment>



    );





  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    removeItem: (product) => dispatch({ type: 'REMOVE_FROM_CART', payload: product }),
    //updateCartTotal: (newSubTotal) => dispatch({ type: 'UPDATE_CART_TOTAL', newSubTotalValue: newSubTotal }),
    updateQuantity: (productId, quantity) => dispatch({ type: 'ChANGE_PRODUCT_QUANTITY', productId: productId, newQuantity: quantity }),
    //addItemToCart: (product) => dispatch({ type: 'ADD_TO_CART', payload: product })

  }
}

const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ShipmentAdrsBox);