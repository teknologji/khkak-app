import React from 'react';
import { Text, View, TouchableOpacity, StyleSheet  } from 'react-native';
import MyTxt from './MyTxt';

//import striptags from 'striptags';
import { connect } from 'react-redux';

//import theme from  '../assets/theme';
import { cstmStyle, theme } from '../assets/theme';
import Toast from 'react-native-root-toast';
import { FontAwesome5, SimpleLineIcons, MaterialCommunityIcons } from '@expo/vector-icons';
import PrdctImg from './PrdctImg';
import Button from './Button';
import {
  Surface
} from 'react-native-paper';

class ProductBox extends React.Component {

  constructor(props) {
    super(props);

  }


/*   toProductDetails(id){


    this.props.navigation.navigate({
      routeName: 'EditProduct',
      params: { id: id },
      //key: 'k'+Math.random()
    });

  } */

  /* addToFav() {

    //this.setState({ isReady: false});  


    if (this.props.userInfo.info.id == 0) {

      return Toast.show(this.props.selectedLanguage.plsLgnFrst, {
        duration: Toast.durations.LONG,
        position: 0, //Toast.positions.TOP,
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 0,
        backgroundColor: '#FF9800'
      });

    }

    let loadingToast = Toast.show(this.props.selectedLanguage.loadingTxt, {
      duration: Toast.durations.LONG,
      position: 0, //Toast.positions.TOP
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      backgroundColor: '#FF9800'

    });

    fetch(this.props.defaultSets.baseUrl + 'add-to-favourite/' + this.props.userInfo.info.id + '/' + this.props.article.id)
      .then((response) => response.json())
      .then((response) => {

        this.props.addPrdctToFav(this.props.article.id);
        //return console.log(response)
        // alert(response.msg)
        Toast.hide(loadingToast);

        Toast.show(response.msg, {
          duration: Toast.durations.LONG,
          position: 0, //Toast.positions.TOP,
          shadow: true,
          animation: true,
          hideOnPress: true,
          delay: 0,
          backgroundColor: '#FF9800'
        });

      }).done();

  } */

  componentDidMount() {
  }
  render() {
    let item = this.props.item;
    let lng = this.props.selectedLanguage;


    return (

      <Surface style={styles.surface}>
     
     
     <View style={{
          marginTop: 40, marginBottom: 5,
          justifyContent: 'center',
          flex: 1
        }}>

          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('ShowProduct', {id: item.id})}>
            <PrdctImg imgUrl={item.imgUrl} />
          </TouchableOpacity>
      

          <TouchableOpacity
            style={{ marginTop: 15 }}
            onPress={() => this.props.navigation.navigate('ShowProduct', {id: item.id})}>

            <MyTxt txt={eval("item.title"+lng.langCode)}
              style={[{ textAlign: 'center', fontSize: 16 }, cstmStyle.textDanger]} />


          </TouchableOpacity>
        </View>

        <View style={{ paddingRight: 2, marginBottom: 10, marginTop: 5 }}>

          <View style={{ flexDirection: 'row', justifyContent: 'center'}}>


            <Text style={[{fontSize: 18}]}>
            {item.priceBeforeDiscount} {lng.sarTxt}
            </Text>
            {/* <Text style={cstmStyle.txtStriked}>
              {item.priceBeforeDiscount}
            </Text> */}
          </View>


        </View>

        <View>
          <Button 
           onPress={() => {
            this.props.addItemToCart(item, this.props.selectedLanguage);            
            }}
          mode="contained" 
          style={cstmStyle.bgGrey}
          icon="cart"
          compact={false}
          contentStyle={{height: 30}}
          >          
            <MyTxt txt={this.props.selectedLanguage.addToCartTxt} />
          </Button>
        </View>

  </Surface>

    


     



   

    );



  }
}

const styles = StyleSheet.create({
  surface: {
    padding: 8,
    margin: 10,
    height: 280,
    width: 175,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 5,
  },
});

const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
    //selectedCurr: state.Curr.slctdCurr,
    defaultSets: state.DfltSts,
    userInfo: state.User,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      addItemToCart: (product, lng) => {

        dispatch({ type: 'ADD_TO_CART', payload: product });

        Toast.show(lng.addedToCartTxt, {
          duration: Toast.durations.SHORT,
          position: 0, //Toast.positions.TOP,
          shadow: true,
          animation: true,
          hideOnPress: true,
          delay: 0,
          backgroundColor: '#7aa513'
        });
      },
      addPrdctToFav: (prdctId) => dispatch({ type: 'ADD_TO_FAV', prdctId })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductBox);


