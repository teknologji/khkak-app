import React, { memo } from 'react';
import {
  ImageBackground,
  StyleSheet,
  KeyboardAvoidingView,
  StatusBar,
  Platform,
  SafeAreaView,
  View
} from 'react-native';
//import Constants from 'expo-constants';


const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[{ backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

const Background = ({ children }) => (
  
    
  <ImageBackground
    source={require('../assets/background_dot.png')}
    resizeMode="repeat"
    style={styles.background}
  >
    
    <MyStatusBar backgroundColor="#8B0000" barStyle="light-content" />

    
    <KeyboardAvoidingView style={styles.container}
      behavior={Platform.OS == "ios" ? "padding" : "height"}>
        {children}
    </KeyboardAvoidingView>
  </ImageBackground>

);



const styles = StyleSheet.create({
  background: {
    flex: 1,
    paddingBottom: 20,
    /* width: '100%', */
    /* backgroundColor: 'blue' */
  },
  container: {
    flex: 1,
    paddingTop: 20,
    paddingRight: 10,
    paddingLeft: 10,
    width: '100%',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    /* backgroundColor: 'red' */
  },
});

export default memo(Background);