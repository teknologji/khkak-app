import React, { memo } from 'react';
import { StyleSheet } from 'react-native';
import { Button as PaperButton } from 'react-native-paper';
import { theme } from '../assets/theme';
import MyTxt from './MyTxt';
import { FontAwesome5 } from '@expo/vector-icons';

const Button = ({ mode, style, children, ...props }) => (
  <PaperButton
    style={[
      styles.button,
      mode === 'outlined' && { backgroundColor: theme.colors.surface },
      style,
    ]}
    //labelStyle={styles.text}
    mode={mode}
    {...props}
  >
    <MyTxt txt={children} style={[styles.text, style]} />
    
  </PaperButton>
);

const styles = StyleSheet.create({
  button: {/* 
    width: '100%',
    marginVertical: 10, */
    backgroundColor: '#5b5b5b'
  },
  text: {
    fontSize: 18,/* 
    lineHeight: 26, */
    marginRight: 10,
    marginLeft: 10
  },
});

export default memo(Button);