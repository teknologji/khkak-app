import React, { memo } from 'react';
import {
  ImageBackground,
  StyleSheet,
  KeyboardAvoidingView,
  ScrollView,
  Platform,
  SafeAreaView
} from 'react-native';
//import Constants from 'expo-constants';

const BackgroundScroll = ({ children }) => (
  <ImageBackground
    source={require('../assets/background_dot.png')}
    resizeMode="repeat"
    style={styles.background}
  >
    
    <KeyboardAvoidingView style={styles.container}>
        {children}
    </KeyboardAvoidingView>
    
  </ImageBackground>

);

const styles = StyleSheet.create({
  background: {
    flex: 1,
    paddingBottom: 0,
    paddingTop: Platform.OS === 'android' ? 20 : 0
  },
  container: {
    paddingTop: Platform.OS === 'android' ? 20 : 0,
    flex: 1,
    paddingRight: 20,
    paddingLeft: 20,
    width: '100%',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    /* backgroundColor: 'red' */
  },
});

export default memo(BackgroundScroll);