import React from 'react';
import { Text, View, TouchableOpacity, StyleSheet  } from 'react-native';
import MyTxt from './MyTxt';

//import striptags from 'striptags';
import { connect } from 'react-redux';

//import theme from  '../assets/theme';
import { cstmStyle, theme } from '../assets/theme';
import Toast from 'react-native-root-toast';
import { FontAwesome5, SimpleLineIcons, MaterialCommunityIcons } from '@expo/vector-icons';
import PrdctImg from './PrdctImg';
import Button from './Button';
import {
Surface
} from 'react-native-paper';

class CourseBox extends React.Component {

  constructor(props) {
    super(props);

  }


/*   toCourseDetails(id){


    this.props.navigation.navigate({
      routeName: 'EditCourse',
      params: { id: id },
      //key: 'k'+Math.random()
    });

  } */

  /* addToFav() {

    //this.setState({ isReady: false});  


    if (this.props.userInfo.info.id == 0) {

      return Toast.show(this.props.selectedLanguage.plsLgnFrst, {
        duration: Toast.durations.LONG,
        position: 0, //Toast.positions.TOP,
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 0,
        backgroundColor: '#FF9800'
      });

    }

    let loadingToast = Toast.show(this.props.selectedLanguage.loadingTxt, {
      duration: Toast.durations.LONG,
      position: 0, //Toast.positions.TOP
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      backgroundColor: '#FF9800'

    });

    fetch(this.props.defaultSets.baseUrl + 'add-to-favourite/' + this.props.userInfo.info.id + '/' + this.props.article.id)
      .then((response) => response.json())
      .then((response) => {

        this.props.addPrdctToFav(this.props.article.id);
        //return console.log(response)
        // alert(response.msg)
        Toast.hide(loadingToast);

        Toast.show(response.msg, {
          duration: Toast.durations.LONG,
          position: 0, //Toast.positions.TOP,
          shadow: true,
          animation: true,
          hideOnPress: true,
          delay: 0,
          backgroundColor: '#FF9800'
        });

      }).done();

  } */

  componentDidMount() {
    //console.log(cstmStyle)
  }
  render() {
    let item = this.props.item;
    let lng = this.props.selectedLanguage;

    //const {langRow, langAlign} = this.props.selectedLanguage;
    const elmntTrns = item && item.elment_trans.find(row => row.languageCode == lng.langCode);

    //const {currency_code, exchangeRate} = this.props.selectedCurr;
    //const time = (created_at || '');
    /*  const defaultImg =
       '../../assets/img/notfound.png'; */

    return (

      <Surface style={styles.surface}>
      {elmntTrns != 888 ?
          <View style={cstmStyle.dsntBx}>


            <MyTxt txt={lng.avlblTxt}
              style={[cstmStyle.textWhite, { fontSize: 14 }]} />

          </View>
          : <View style={{ height: 0 }}></View>

        }
     
     <View style={{
          marginTop: 40, marginBottom: 5,
          justifyContent: 'center',
          flex: 1
        }}>

{item && item.files[0] ? 
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('ShowCourse', {id: item.id, courseTitle: elmntTrns.title})}>
            <PrdctImg
              imgUrl={item.files[0].imgUrl} />
          </TouchableOpacity>
        : null}

          <TouchableOpacity
            style={{ marginTop: 15 }}
            onPress={() => this.props.navigation.navigate('ShowCourse', {id: item.id, courseTitle: elmntTrns.title})}>

            <MyTxt txt={elmntTrns && elmntTrns.title.substring(0, 30)}
              style={{ textAlign: 'center', fontSize: 18 }} />


          </TouchableOpacity>
        </View>

        <View style={{ paddingRight: 2, marginBottom: 10, marginTop: 5 }}>

          <View style={{ flexDirection: 'row', justifyContent: 'center'}}>


            <Text style={[cstmStyle.textGreen, {fontSize: 18}]}>
            {item.price}
            </Text>
            {/* <Text style={cstmStyle.txtStriked}>
              {item.price}
            </Text> */}
          </View>


        </View>

        <View>
          <Button 
          onPress={() => this.props.navigation.navigate('ShowCourse', {id: item.id, courseTitle: elmntTrns.title})}
          mode="contained" 
          style={cstmStyle.bgOrange}
          icon="library"
          compact={false}
          contentStyle={{height: 30}}
          >          
            <MyTxt txt={this.props.selectedLanguage.crsDtlsTxt} />
          </Button>
        </View>

  </Surface>

    


     



   

    );



  }
}

const styles = StyleSheet.create({
  surface: {
    padding: 8,
    margin: 10,
    height: 280,
    width: 175,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 5,
  },
});

const mapStateToProps = (state) => {
  return {
    selectedLanguage: state.LanguageReducer.slctdLng,
    //selectedCurr: state.Curr.slctdCurr,
    defaultSets: state.DfltSts,
    userInfo: state.User,
  }
}


export default connect(mapStateToProps, null)(CourseBox);
